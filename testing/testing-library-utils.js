import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom/extend-expect'

import '../src/index.css';

const AllTheProviders = ({ children }) => {
  return (
    children
  );
};

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options })

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }

export { userEvent }

export function mockWindowSize(width, height) {
  jest.spyOn(document.body, 'getBoundingClientRect').mockImplementation(() => ({ width, height }));
  fireEvent(window, new Event('resize'));
}
