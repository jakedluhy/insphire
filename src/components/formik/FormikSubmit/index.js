import React from 'react';

import { useFormikContext } from 'formik';

import Button from '../../Button';

function FormikSubmit({ className, children, ...rest }) {
  const { isSubmitting } = useFormikContext();

  return (
    <Button
      className={className}
      type='submit'
      disabled={isSubmitting}
      variant='primary'
      color='primary'
      {...rest}
    >
      {children || 'Submit'}
    </Button>
  );
}

export default React.memo(FormikSubmit);
