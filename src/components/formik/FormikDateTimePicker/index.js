import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import _ from 'lodash';

import FormLabel from '../../FormLabel';
import DateTimePicker from '../../DateTimePicker';

function FormikDateTimePicker(props) {
  const { className, label, hasLabel, name, helperText, isRequired, ...rest } = props;
  const [field, meta, helpers] = useField(name);

  const isErrored = !!meta.touched && !!meta.error;
  const resolvedLabel = label || _.startCase(name);

  return (
    <FormLabel
      className={className}
      label={hasLabel ? resolvedLabel : null}
      error={isErrored ? meta.error : null}
      helperText={helperText}
      isRequired={isRequired}
    >
      <DateTimePicker
        {...rest}
        selected={field.value}
        onChange={helpers.setValue}
      />
    </FormLabel>
  );
}

FormikDateTimePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  hasLabel: PropTypes.bool,
  helperText: PropTypes.string,
  isRequired: PropTypes.bool,
};

FormikDateTimePicker.defaultProps = {
  hasLabel: true,
};

export default FormikDateTimePicker;
