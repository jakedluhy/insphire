import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import _ from 'lodash';

import FormLabel from '../../FormLabel';
import FileUpload from '../../FileUpload';

function FormikFileUpload(props) {
  const { className, label, hasLabel, name, helperText, isRequired, ...rest } = props;
  const [field, meta, helpers] = useField(name);

  const isErrored = !!meta.touched && !!meta.error;
  const resolvedLabel = label || _.startCase(name);

  return (
    <FormLabel
      className={className}
      label={hasLabel ? resolvedLabel : null}
      error={isErrored ? meta.error : null}
      helperText={helperText}
      isRequired={isRequired}
    >
      <FileUpload
        {...rest}
        value={field.value}
        onChange={helpers.setValue}
      />
    </FormLabel>
  );
}

FormikFileUpload.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  hasLabel: PropTypes.bool,
  helperText: PropTypes.string,
  isRequired: PropTypes.bool,
};

FormikFileUpload.defaultProps = {
  hasLabel: true,
};

export default FormikFileUpload;
