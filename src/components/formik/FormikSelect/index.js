import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import _ from 'lodash';

import FormLabel from '../../FormLabel';
import Select from '../../Select';

function FormikSelect(props) {
  const { className, label, hasLabel, name, helperText, isRequired, onChange, ...rest } = props;
  const [field, meta, helpers] = useField(name);

  const isErrored = !!meta.touched && !!meta.error;
  const resolvedLabel = label || _.startCase(name);

  return (
    <FormLabel
      className={className}
      label={hasLabel ? resolvedLabel : null}
      error={isErrored ? meta.error : null}
      helperText={helperText}
      isRequired={isRequired}
    >
      <Select
        {...rest}
        selectedItemValue={field.value}
        onChange={val => {
          helpers.setValue(val);
          if (onChange) onChange(val);
        }}
      />
    </FormLabel>
  );
}

FormikSelect.propTypes = {
  ...Select.propTypes,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  hasLabel: PropTypes.bool,
  helperText: PropTypes.string,
  isRequired: PropTypes.bool,
};

FormikSelect.defaultProps = {
  hasLabel: true,
};

export default FormikSelect;
