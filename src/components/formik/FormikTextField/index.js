import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import _ from 'lodash';

import FormLabel from '../../FormLabel';
import TextField from '../../TextField';

function FormikTextField(props) {
  const { className, label, hasLabel, name, helperText, isRequired, ...rest } = props;
  const [field, meta] = useField({ name, ...rest });

  const isErrored = !!meta.touched && !!meta.error;
  const resolvedLabel = label || _.startCase(name);

  return (
    <FormLabel
      className={className}
      label={resolvedLabel}
      hasLabel={hasLabel}
      error={isErrored ? meta.error : null}
      helperText={helperText}
      isRequired={isRequired}
    >
      <TextField {...field} {...rest} isErrored={isErrored} />
    </FormLabel>
  );
}

FormikTextField.propTypes = {
  ...TextField.propTypes,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  hasLabel: PropTypes.bool,
  helperText: PropTypes.string,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func,
  value: PropTypes.string,
};

FormikTextField.defaultProps = {
  hasLabel: true,
};

export default React.memo(FormikTextField);
