import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import _ from 'lodash';

import TextField from '../TextField';
import Search from '../../icons/Search';

function Menu({
  className,
  items,
  selectedItemValue,
  onChange,
  getItemProps,
  isSearchable,
  searchRef,
  ...rest
}) {
  const [searchValue, setSearchValue] = useState('');

  return (
    <div className={cx(className, 'bg-white p-sm rounded-md shadow-sm')} {...rest}>
      {isSearchable ? (
        <TextField
          ref={searchRef}
          className='mb-sm '
          icon={<Search size={16} className='text-asphalt-grey' />}
          value={searchValue}
          onChange={e => {
            e.stopPropagation();
            setSearchValue(e.target.value)
          }}
          placeholder='Search for an Item'
        />
      ) : null}

      {_.isEmpty(items) ? (
        <div style={{ height: 40 }} />
      ) : (
        <ul
          style={{ maxHeight: 180, overflowY: 'auto' }}
        >
          {items.map((item, index) => (
            <li
              key={item.value || item.label}
              className={cx('p-sm rounded-sm cursor-pointer hover:bg-primary hover:text-white', {
                'bg-primary-highlight text-primary': item.value === selectedItemValue,
                'hidden': searchValue && !_.toLower(item.label).includes(_.toLower(searchValue))
              })}
              {...getItemProps({ item, index })}
            >
              {item.label}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

Menu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  })),
  selectedItemValue: PropTypes.string,
  onChange: PropTypes.func,
  getItemProps: PropTypes.func,
  isSearchable: PropTypes.bool,
  searchRef: PropTypes.oneOfType([
    PropTypes.func, 
    PropTypes.shape({ current: PropTypes.instanceOf(Element) })
  ]),
};

Menu.defaultProps = {
  items: [],
  selectedItemValue: null,
  onChange: () => {},
  getItemProps: () => {},
  isSearchable: false,
  searchRef: () => {},
};

export default Menu;
