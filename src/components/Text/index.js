import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const TYPE_TO_CLASS_MAP = {
  'main': 'text-32px font-medium leading-none',
  'title': 'text-2xl font-bold leading-none',
  'secondary-title': 'text-2xl font-light leading-none',
  'subtitle': 'text-lg font-medium leading-26px',
  'paragraph': 'leading-normal',
  'medium-text': 'text-sm font-medium leading-22px',
};

function Text({
  className,
  children,
  label,
  type,
}) {
  const resolvedClassName = cx('block text-dark-grey', TYPE_TO_CLASS_MAP[type], className);

  return (
    <span className={resolvedClassName}>
      {label || children}
    </span>
  );
}

Text.propTypes = {
  children: PropTypes.node,
  label: PropTypes.string,
  type: PropTypes.oneOf([
    'main',
    'title',
    'secondary-title',
    'subtitle',
    'paragraph',
    'medium-text',
  ]),
};

Text.defaultProps = {
  type: 'paragraph',
};

export default Text;
