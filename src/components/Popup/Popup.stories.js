import React from 'react';

import Popup from './index';

export default {
  title: 'Components/Popup',
  component: Popup,
  argTypes: {
    onConfirm: { action: 'confirmed' },
    onRequestClose: { action: 'closed' },
  },
  args: {
    isOpen: true,
    title: 'My Popup',
    confirmText: 'Confirm',
  },
};

const Template = (args) => <Popup {...args} />;

export const Default = Template.bind({});

export const WithBody = Template.bind({});
WithBody.args = {
  body: `I'm baby everyday carry squid vegan tumblr palo santo swag. Pop-up venmo church-key vape master cleanse, tattooed ennui umami taiyaki. Next level ramps before they sold out, cred single-origin coffee deep v drinking vinegar literally farm-to-table. Vinyl chambray gochujang celiac.`,
}
