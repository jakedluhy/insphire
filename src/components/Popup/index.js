import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from '../Button';
import Modal from '../Modal';
import Text from '../Text';

function Popup({
  className,
  title,
  body,
  cancelText,
  cancelProps,
  confirmText,
  confirmProps,
  onConfirm,
  onRequestClose,
  ...rest
}) {
  return (
    <Modal
      className={cx(className, 'min-w-sm')}
      onRequestClose={onRequestClose}
      {...rest}
    >
      <div className='pb-md'>
        <Text type='main' label={title} />
      </div>

      {body ? <Text label={body} /> : null}

      <div className='pt-lg flex justify-end'>
        <div className='flex'>
          <Button
            label={cancelText}
            variant='tertiary'
            {...cancelProps}
            className={cx('mr-md', cancelProps.className)}
            onClick={(e) => {
              if (cancelProps.onClick) cancelProps.onClick(e);
              setTimeout(onRequestClose, 200);
            }}
          />

          <Button
            label={confirmText}
            variant='primary'
            {...confirmProps}
            onClick={e => setTimeout(() => onConfirm(e), 200)}
          />
        </div>
      </div>
    </Modal>
  );
}

Popup.propTypes = {
  ...Modal.propTypes,
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  cancelText: PropTypes.string,
  cancelProps: PropTypes.object,
  confirmText: PropTypes.string.isRequired,
  confirmProps: PropTypes.object,
  onConfirm: PropTypes.func.isRequired,
};

Popup.defaultProps = {
  cancelText: 'Cancel',
  cancelProps: {},
  confirmProps: {},
};

export default Popup;
