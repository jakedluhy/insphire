import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useSelect } from 'downshift';

import RelativePortal from 'react-relative-portal';
import Button from '../Button';
import Menu from '../Menu';

function ButtonMenu({
  children,
  className,
  style,
  menuPlacement,
  items,
  selectedItemValue,
  onChange,
}) {
  const {
    isOpen,
    getToggleButtonProps,
    getMenuProps,
    getItemProps,
  } = useSelect({
    items,
    itemToString: i => i.label,
    onSelectedItemChange: ({ selectedItem }) => {
      selectedItem.onClick();
    },
  });

  const portalProps = menuPlacement === 'left' ? { left: 0 }: { right: 0 };

  return (
    <div className={cx(className, 'relative')} style={style}>
      <Button
        className='flex items-center'
        color='plain'
        variant='tertiary'
        {...getToggleButtonProps()}
      >
        {children}
      </Button>

      <RelativePortal
        component='div'
        top={1}
        {...portalProps}
      >
        <div {...getMenuProps()} style={{ outline: 0 }}>
          {isOpen ? (
            <Menu
              items={items}
              selectedItemValue={selectedItemValue}
              onChange={onChange}
              getItemProps={getItemProps}
            />
          ) : null}
        </div>
      </RelativePortal>
    </div>
  );
}

ButtonMenu.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  menuPlacement: PropTypes.oneOf(['left', 'right']),
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    onClick: PropTypes.func.isRequired,
  })),
};

ButtonMenu.defaultProps = {
  menuPlacement: 'left',
  items: [],
};

export default ButtonMenu;
