import React from 'react';
import PropTypes from 'prop-types';
import getYear from 'date-fns/getYear';

import ReactDatepicker from 'react-datepicker';
import ChevronLeft from 'react-feather/dist/icons/chevron-left';
import ChevronRight from 'react-feather/dist/icons/chevron-right';
import Button from '../Button';
import Text from '../Text';
import TextField from '../TextField';

import 'react-datepicker/dist/react-datepicker.css';
import './date-time-picker.css';

const TextFieldForTime = React.forwardRef((props, ref) => {
  const { onChange = () => {}, ...rest } = props;

  return <TextField {...rest} ref={ref} onChange={e => onChange(e.target.value)} />;
});

const MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

function DateTimePicker({
  size,
  isErrored,
  isSuccess,
  showTimeInput,
  ...rest
}) {
  return (
    <ReactDatepicker
      {...rest}
      customInput={(
        <TextField
          size={size}
          isErrored={isErrored}
          isSuccess={isSuccess}
        />
      )}
      withPortal
      timeInputLabel=''
      dateFormat={showTimeInput ? 'MM/dd/yyyy h:mm aa' : 'MM/dd/yyyy'}
      showTimeInput={showTimeInput}
      showPopperArrow={false}
      renderCustomHeader={({
        date,
        decreaseMonth,
        increaseMonth,
        prevMonthButtonDisabled,
        nextMonthButtonDisabled
      }) => {
        const dateToDisplay = date || new Date();

        return (
          <div
            className='px-sm flex justify-between items-center'
            style={{ minWidth: 264 }}
          >
            <Button
              size='small'
              variant='tertiary'
              color='plain'
              className='py-2 mr-md'
              onClick={decreaseMonth}
            >
              <ChevronLeft />
            </Button>

            <Text type='subtitle'>
              {MONTHS[dateToDisplay.getMonth()]} {getYear(dateToDisplay)}
            </Text>

            <Button
              size='small'
              variant='tertiary'
              color='plain'
              className='py-2 ml-md'
              onClick={increaseMonth}
            >
              <ChevronRight />
            </Button>
          </div>
        );
      }}
      customTimeInput={<TextFieldForTime type='time' size='small' />}
    />
  )
}

DateTimePicker.propTypes = {
  showTimeInput: PropTypes.bool,
};

DateTimePicker.defaultProps = {
  showTimeInput: true,
};

export default DateTimePicker;
