import React from 'react';

import { Tooltip as TippyTooltip } from 'react-tippy';

import 'react-tippy/dist/tippy.css'

function Tooltip({
  ...rest
}) {
  return (
    <TippyTooltip
      arrow={true}
      {...rest}
    />
  );
}

export default Tooltip;
