import React from 'react';
import { render } from 'testing-library-utils';

import Button from './index';

describe('Button', () => {
  it('renders', () => {
    const { asFragment } = render(
      <Button label='Test' />
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
