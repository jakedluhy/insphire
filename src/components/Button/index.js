import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './button.css';

const DISABLED_PRIMARY = 'bg-ui-grey text-asphalt-grey';
const DISABLED_SECONDARY = 'text-asphalt-grey border-ui-grey';
const DISABLED_TERTIARY = 'text-asphalt-grey';

const VARIANT_COLOR_CLASSNAME_MAP = {
  'primary-primary-false': 'bg-primary hover:bg-primary-hover active:bg-primary-active text-white',
  'primary-negative-false': 'bg-negative hover:bg-negative-hover text-white',
  'primary-positive-false': 'bg-positive hover:bg-positive-hover text-white',
  'primary-negative-inverse-false': 'bg-white hover:bg-riverstone-grey text-negative',
  'primary-positive-inverse-false': 'bg-white hover:bg-riverstone-grey text-positive',
  'primary-plain-false': '',
  'secondary-primary-false': 'text-dark border border-wolf-grey hover:bg-ui-grey active:text-primary active:border-primary active:bg-primary-highlight',
  'secondary-negative-false': 'text-negative border border-negative hover:bg-negative-highlight',
  'secondary-positive-false': 'text-positive border border-positive hover:bg-positive-highlight',
  'secondary-negative-inverse-false': 'text-white border border-white',
  'secondary-positive-inverse-false': 'text-white border border-white',
  'secondary-plain-false': 'text-dark border border-wolf-grey hover:bg-ui-grey',
  'tertiary-primary-false': 'text-dark hover:bg-ui-grey active:text-primary active:bg-primary-highlight',
  'tertiary-negative-false': 'text-negative hover:bg-negative-highlight',
  'tertiary-positive-false': 'text-positive hover:bg-positive-highlight',
  'tertiary-negative-inverse-false': 'text-white hover:bg-negative-highlight',
  'tertiary-positive-inverse-false': 'text-white hover:bg-positive-highlight',
  'tertiary-plain-false': 'text-dark hover:bg-ui-grey',
  'primary-primary-true': DISABLED_PRIMARY,
  'primary-negative-true': DISABLED_PRIMARY,
  'primary-positive-true': DISABLED_PRIMARY,
  'primary-negative-inverse-true': DISABLED_PRIMARY,
  'primary-positive-inverse-true': DISABLED_PRIMARY,
  'primary-plain-true': DISABLED_PRIMARY,
  'secondary-primary-true': DISABLED_SECONDARY,
  'secondary-negative-true': DISABLED_SECONDARY,
  'secondary-positive-true': DISABLED_SECONDARY,
  'secondary-negative-inverse-true': DISABLED_SECONDARY,
  'secondary-positive-inverse-true': DISABLED_SECONDARY,
  'secondary-plain-true': DISABLED_SECONDARY,
  'tertiary-primary-true': DISABLED_TERTIARY,
  'tertiary-negative-true': DISABLED_TERTIARY,
  'tertiary-positive-true': DISABLED_TERTIARY,
  'tertiary-negative-inverse-true': DISABLED_TERTIARY,
  'tertiary-positive-inverse-true': DISABLED_TERTIARY,
  'tertiary-plain-true': DISABLED_TERTIARY,
};

const SIZE_CLASSNAME_MAP = {
  'small-false': 'py-1 px-2',
  'medium-false': 'py-2 px-4',
  'large-false': 'py-3 px-6',
  'small-true': 'py-1 px-6',
  'medium-true': 'py-2 px-8',
  'large-true': 'py-3 px-12',
};

const Button = React.forwardRef((props, ref) => {
  const {
    type,
    className,
    style,
    children,
    label,
    variant,
    size,
    color,
    disabled,
    isPill,
    ...rest
  } = props;

  const resolvedClassName = cx(
    className,
    'button leading-normal',
    VARIANT_COLOR_CLASSNAME_MAP[`${variant}-${color}-${disabled}`],
    SIZE_CLASSNAME_MAP[`${size}-${isPill}`],
    {
      'rounded-sm': !isPill,
      'rounded-full': isPill,
    }
  ) ;

  return (
    <button
      ref={ref}
      type={type}
      className={resolvedClassName}
      style={{
        outline: 0,
        ...style
      }}
      {...rest}
    >
      {label || children}
    </button>
  );
});

Button.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(['button', 'submit']),
  style: PropTypes.object,
  children: PropTypes.node,
  label: PropTypes.string,
  variant: PropTypes.oneOf(['primary', 'secondary', 'tertiary']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  color: PropTypes.oneOf(['primary', 'negative', 'positive', 'negative-inverse', 'positive-inverse', 'plain']),
  disabled: PropTypes.bool,
  isPill: PropTypes.bool,
};

Button.defaultProps = {
  type: 'button',
  className: '',
  style: {},
  variant: 'secondary',
  size: 'medium',
  color: 'primary',
  disabled: false,
  isPill: false,
};

export default Button;
