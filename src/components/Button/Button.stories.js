import React from 'react';
import _ from 'lodash';

import Button from './index';

export default {
  title: 'Components/Button',
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
  label: 'Default',
};

export const Variants = (args) => (
  <>
    <Button className='mr-4' variant='primary' {..._.omit(args, 'variant')}>Primary</Button>
    <Button className='mr-4' variant='secondary' {..._.omit(args, 'variant')}>Secondary</Button>
    <Button className='mr-4' variant='tertiary' {..._.omit(args, 'variant')}>Tertiary</Button>
  </>
);
Variants.args = {
  size: 'medium',
  color: 'primary',
  disabled: false,
};

export const Sizes = (args) => (
  <>
    <Button className='mr-4' size='small' {..._.omit(args, 'size')}>Small</Button>
    <Button className='mr-4' size='medium' {..._.omit(args, 'size')}>Medium</Button>
    <Button className='mr-4' size='large' {..._.omit(args, 'size')}>Large</Button>
  </>
);
Sizes.args = {
  variant: 'secondary',
  color: 'primary',
  disabled: false,
};

export const Colors = (args) => (
  <>
    <Button className='mr-4' color='primary' {..._.omit(args, 'color')}>Primary</Button>
    <Button className='mr-4' color='negative' {..._.omit(args, 'color')}>Negative</Button>
    <Button className='mr-4' color='positive' {..._.omit(args, 'color')}>Positive</Button>
    <div className='mr-4 p-4 bg-negative inline-block'>
      <Button color='negative-inverse' {..._.omit(args, 'color')}>Negative Inverse</Button>
    </div>
    <div className='mr-4 p-4 bg-positive inline-block'>
      <Button color='positive-inverse' {..._.omit(args, 'color')}>Positive Inverse</Button>
    </div>
  </>
);
Colors.args = {
  variant: 'secondary',
  size: 'medium',
  disabled: false,
};
