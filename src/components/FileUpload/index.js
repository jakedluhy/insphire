import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import _ from 'lodash';
import { useDropzone } from 'react-dropzone';

import AlertTriangle from 'react-feather/dist/icons/alert-triangle';
import FilePlus from 'react-feather/dist/icons/file-plus';
import Upload from 'react-feather/dist/icons/upload';

function FileUpload({
  className,
  value,
  onChange,
  placeholder,
  disabled,
}) {
  const [errorMessage, setErrorMessage] = useState(null);

  const onDrop = useCallback(files => {
    const numFiles = _.size(files);

    if (numFiles === 0) {
      setErrorMessage('Must add a single file');
    } else if (numFiles > 1) {
      setErrorMessage('Submit a single file');
    } else {
      onChange(files[0]);
      setErrorMessage(null);
    }
  }, [setErrorMessage, onChange]);

  const {
    getRootProps,
    getInputProps,
    isDragActive
  } = useDropzone({
    onDrop,
    multiple: false,
    disabled,
  });

  return (
    <div
      {...getRootProps({
        className: cx(className, 'p-sm rounded-sm flex items-center justify-center', {
          'cursor-pointer bg-white border': !disabled,
          'bg-ui-grey': disabled,
        }),
        style: { outline: 0, height: 40 },
        onClick: e => e.stopPropagation(),
      })}
    >
      <input {...getInputProps({ disabled })} />

      {isDragActive ? (
        <span className='text-asphalt-grey flex items-center'>
          <span>Drop to upload</span>
          <FilePlus size={16} className='ml-xs' />
        </span>
      ) : errorMessage ? (
        <>
          <span>{errorMessage}</span>
          <AlertTriangle size={16} className='ml-xs' />
        </>
      ) : value ? (
        <span>{value.name}</span>
      ) : (
        <span className='text-asphalt-grey flex items-center'>
          <span>{placeholder}</span>
          <Upload size={16} className='ml-xs' />
        </span>
      )}
    </div>
  )
}

FileUpload.propTypes = {
  className: PropTypes.string,
  value: PropTypes.instanceOf(File),
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
};

FileUpload.defaultProps = {
  value: null,
  placeholder: 'Click or drop files',
};

export default FileUpload;
