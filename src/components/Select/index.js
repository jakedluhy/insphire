import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import _ from 'lodash';
import { useSelect } from 'downshift';

import ChevronDown from 'react-feather/dist/icons/chevron-down';
import RelativePortal from 'react-relative-portal';
import Menu from '../Menu';

const ALLOWED_MENU_KEYS = {
  ArrowDown: true,
  ArrowUp: true,
  End: true,
  Home: true,
  Enter: true,
  Escape: true,
};

function Select({
  className,
  style,
  items,
  selectedItemValue,
  onChange,
  placeholder,
  isSearchable,
}) {
  const searchRef = useRef(null);
  const isFocusingSearchRef = useRef(false);

  const selectedItem = _.find(items, { value: selectedItemValue });

  const {
    isOpen,
    getToggleButtonProps,
    getMenuProps,
    getItemProps,
  } = useSelect({
    items,
    selectedItem: selectedItem || {},
    itemToString: i => i.value,
    onSelectedItemChange: ({ selectedItem }) => onChange(selectedItem.value),
    onIsOpenChange: (changes => {
      if (changes.isOpen && searchRef.current) {
        isFocusingSearchRef.current = true;
        setTimeout(() => {
          searchRef.current.focus();
        }, 50);
      }
    }),
    stateReducer: (state, { changes, type }) => {
      switch (type) {
        case useSelect.stateChangeTypes.MenuBlur:
          if (isFocusingSearchRef.current) {
            isFocusingSearchRef.current = false;
            return { ...changes, isOpen: true };
          } else {
            return changes;
          }
        default:
          return changes;
      }
    }
  });

  return (
    <div className={cx(className, 'relative')} style={style}>
      <button
        type='button'
        className={cx(
          'w-full bg-white border border-wolf-grey rounded-sm px-sm',
          'cursor-pointer flex items-center justify-between',
          { 'text-asphalt-grey': !selectedItem }
        )}
        style={{ outline: 0, paddingTop: 7, paddingBottom: 7 }}
        {...getToggleButtonProps()}
      >
        <span className='truncate mr-sm'>
          {selectedItem ? (
            selectedItem.label
          ) : (
            placeholder || 'Select An Item'
          )}
        </span>

        <ChevronDown size={16} />
      </button>

      <RelativePortal
        component='div'
        fullWidth
        left={0}
        right={0}
        top={1}
      >
        <div
          {...getMenuProps({
            onKeyDown: e => {
              if (isSearchable && !ALLOWED_MENU_KEYS[e.key]) {
                e.nativeEvent.preventDownshiftDefault = true;
              }
            },
          })}
          style={{ outline: 0 }}
        >
          {isOpen ? (
            <Menu
              items={items}
              selectedItemValue={selectedItemValue}
              onChange={onChange}
              getItemProps={getItemProps}
              isSearchable={isSearchable}
              searchRef={searchRef}
            />
          ) : null}
        </div>
      </RelativePortal>
    </div>
  );
}

Select.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  })),
  selectedItemValue: PropTypes.string,
  onChange: PropTypes.func,
  isSearchable: PropTypes.bool,
};

Select.defaultProps = {
  items: [],
  selectedItemValue: null,
  onChange: () => {},
  isSearchable: false,
};

export default Select;
