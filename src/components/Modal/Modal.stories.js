import React, { useState } from 'react';

import Modal from './index';
import Button from '../Button';

export default {
  title: 'Components/Modal',
  component: Modal,
};

const Template = (args) => <Modal {...args} />;

export const Default = Template.bind({});
Default.args = {
  isOpen: true,
};

export const Controllable = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button label='Open' onClick={() => setIsOpen(!isOpen)} />

      <Modal isOpen={isOpen} onRequestClose={() => setIsOpen(false)}>
        Test Content
      </Modal>
    </>
  )
};
