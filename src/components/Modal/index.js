import React from 'react';
import cx from 'classnames';

import ReactModal from 'react-modal';
import Button from '../Button';
import X from '../../icons/X';

ReactModal.setAppElement('#root');

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(41,47,76,0.7)',
    transition: 'opacity 0.1s ease-in-out',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content : {
    maxHeight: '90vh',
    maxWidth: '40rem',
    transition: 'all 0.3s ease 0s',
    outline: 'none',
    boxShadow: 'rgba(0,0,0,0.7) 0 25px 50px -15px',
    paddingBottom: '1px',
    overflowY: 'hidden',
    position: 'relative',
  }
};

function Modal({
  className,
  style,
  children,
  isOpen,
  onRequestClose,
  ...rest
}) {
  const resolvedClassName = cx(
    className,
    'bg-white rounded-lg border-0',
  );

  return (
    <ReactModal
      style={{ overlay: customStyles.overlay, content: { ...customStyles.content, ...style } }}
      className={resolvedClassName}
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      {...rest}
    >
      <Button
        size='small'
        variant='tertiary'
        color='plain'
        className='py-2'
        style={{ position: 'absolute', top: 16, right: 16 }}
        onClick={() => setTimeout(onRequestClose, 200)}
      >
        <X height={12} width={12} />
      </Button>

      <div className='p-xl overflow-y-auto' style={{ maxHeight: 'calc(90vh - 1px)' }}>
        {children}
      </div>
    </ReactModal>
  );
}

Modal.propTypes = {
  ...ReactModal.propTypes,
};

Modal.defaultProps = {

};

export default Modal;
