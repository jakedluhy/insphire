import React from 'react';
import PropTypes from 'prop-types';

function FormLabel({
  className,
  label,
  hasLabel,
  hasErrorLabel,
  error,
  helperText,
  isRequired,
  children,
}) {
  return (
    <div className={className}>
      
      <label className='flex flex-col'>
        {hasLabel ? (
          <span className='flex justify-between items-center'>
            <span className='text-sm'>{label}</span>

            {isRequired ? <span className='text-helper-grey text-13px'>Required*</span> : null}
          </span>
        ) : null}

        {children}
      </label>

      {error ? (
        <div className='text-sm text-negative'>
          {hasErrorLabel ? `${label} ` : ''}{error}
        </div>
      ) : helperText ? (
        <div className='text-sm text-helper-grey'>
          {helperText}
        </div>
      ) : null}
    </div>
  );
}

FormLabel.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  hasLabel: PropTypes.bool,
  hasErrorLabel: PropTypes.bool,
  error: PropTypes.string,
  helperText: PropTypes.string,
  isRequired: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

FormLabel.defaultProps = {
  className: null,
  label: null,
  hasLabel: true,
  hasErrorLabel: true,
  error: null,
  helperText: null,
  isRequired: false,
};

export default FormLabel;
