import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Check from 'react-feather/dist/icons/check';
import X from '../../icons/X';

import './text-field.css';

const SIZE_HEIGHT_MAP = {
  small: 32,
  medium: 40,
  large: 48
};

const SIZE_TEXT_CNAME_MAP = {
  small: 'text-sm',
  medium: 'text-md',
  large: 'text-md'
};

const TextField = React.forwardRef((props, ref) => {
  const {
    type,
    size,
    className,
    style,
    isErrored,
    isSuccess,
    icon,
    ...rest
  } = props;

  const wrapperClassName = cx(
    'bg-white border border-wolf-grey rounded-sm p-sm flex items-center justify-between',
    className,
    {
      'border-negative': isErrored,
      'border-positive': isSuccess,
    }
  );

  return type === 'textarea' ? (
    <textarea
      ref={ref}
      className={cx(wrapperClassName, SIZE_TEXT_CNAME_MAP[size])}
      style={{ outline: 0, minHeight: SIZE_HEIGHT_MAP[size], ...style }}
      {...rest}
    />
  ) : (
    <div className={wrapperClassName} style={{ height: SIZE_HEIGHT_MAP[size], ...style }}>
      <input
        ref={ref}
        type={type}
        className={cx('flex-1', SIZE_TEXT_CNAME_MAP[size])}
        style={{ outline: 0 }}
        {...rest}
      />

      {isErrored ? (
        <X className='text-negative mr-sm' size={8} />
      ) : isSuccess ? (
        <Check className='text-primary mr-sm' size={12} />
      ) : icon ? (
        icon
      ) : null}
    </div>
  );
});


TextField.propTypes = {
  type: PropTypes.oneOf(['input', 'textarea', 'email', 'number', 'time']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  isErrored: PropTypes.bool,
  isSuccess: PropTypes.bool,
};

TextField.defaultProps = {
  type: 'input',
  size: 'medium',
  isErrored: false,
  isSuccess: false,
};

export default TextField;
