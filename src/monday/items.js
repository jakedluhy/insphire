import _ from 'lodash';

import monday from './sdk';
import { MONDAY_API_URL } from './constants';

export function createItem(boardId, groupId, name, columnValues = {}) {
  return monday.api(`
    mutation createItem($groupId: String, $columnValues: JSON) {
      create_item(
        board_id: ${boardId},
        item_name: "${name}",
        group_id: $groupId,
        column_values: $columnValues
      ) {
        id
        name
        column_values {
          id
          text
          title
          value
          additional_info
        }
        group {
          id
          title
          color
        }
      }
    }
  `, {
    variables: { groupId, columnValues: JSON.stringify(columnValues) },
  })
  .then(res => res.data.create_item);
}

export function createSubitem(parentItemId, name, columnValues = {}) {
  return monday.api(`
    mutation createSubitem($columnValues: JSON) {
      create_subitem(
        parent_item_id: ${parentItemId},
        item_name: "${name}",
        column_values: $columnValues
      ) {
        id
        name
        column_values {
          id
          text
          title
          value
          additional_info
        }
      }
    }
  `, {
    variables: { columnValues: JSON.stringify(columnValues) },
  })
  .then(res => res.data.create_subitem);
}

export function addFileToColumn(itemId, columnId, file, apiToken) {
  const formData = new FormData();
  formData.append('variables[file]', file, file.name);
  formData.append('query', `
    mutation addFileToColumn($file: File!) {
      add_file_to_column(
        item_id: ${itemId},
        column_id: "${columnId}",
        file: $file
      ) {
        id
        url
        name
      }
    }
  `);

  return fetch(MONDAY_API_URL, {
    method: 'POST',
    body: formData,
    headers: { Authorization: apiToken },
  })
  .then(res => res.json())
  .then(res => _.get(res, 'data.add_file_to_column'));
}

export function updateItem(boardId, itemId, columnValues = {}) {
  return monday.api(`
    mutation updateItem($columnValues: JSON!) {
      change_multiple_column_values(
        board_id: ${boardId},
        item_id: ${itemId},
        column_values: $columnValues
      ) {
        id
        name
        column_values {
          id
          text
          title
          value
          additional_info
        }
        group {
          id
          title
          color
        }
      }
    }
  `, {
    variables: { columnValues: JSON.stringify(columnValues) },
  })
  .then(res => res.data.change_multiple_column_values);
}

export function deleteItem(itemId) {
  return monday.api(`
    mutation deleteItem {
      delete_item(item_id: ${itemId}) {
        id
      }
    }
  `)
  .then(res => res.data.delete_item);
}
