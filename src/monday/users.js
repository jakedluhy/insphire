import _ from 'lodash';

import monday from './sdk';
import { getUserRoleKey } from './constants';

function getCurrentUserFromApi() {
  return monday.api(`
    query {
      me {
        id
        name
        email
        is_guest
        account {
          slug
        }
      }
    }
  `)
  .then(res => {
    return _.get(res, 'data.me', {});
  });
}

export function getUserRoleFromStorage(userId) {
  return monday.storage.instance.getItem(getUserRoleKey(userId))
  .then(res => _.get(res, 'data.value'));
}

export function setUserRole(userId, role) {
  return monday.storage.instance.setItem(getUserRoleKey(userId), role);
}

export async function getCurrentUser() {
  const currentUser = await getCurrentUserFromApi();
  if (!currentUser.id) return {};

  const role = await getUserRoleFromStorage(currentUser.id);

  return { ...currentUser, role };
}

export function getAllUsers() {
  return monday.api(`
    query {
      users {
        id
        name
        phone
        mobile_phone
        email
        title
        url
        is_guest
        is_pending
      }
    }
  `)
  .then(res => {
    return _.get(res, 'data.users', []);
  });
}
