import _ from 'lodash';

import monday from './sdk';
import {
  BOARD_IDS_STORAGE_KEY,
  CANDIDATE_PIPELINE_BOARD_NAME,
  HIRING_POSITIONS_BOARD_NAME,
  EMAIL_TEMPLATES_BOARD_NAME,
} from './constants';

function getBoardIdsFromStorage() {
  return monday.storage.instance.getItem(BOARD_IDS_STORAGE_KEY)
  .then(res => _.get(res, 'data.value'));
}

function getBoardIdsFromApi() {
  return monday.get('context').then(res => {
    return monday.api(`
      query {
        boards(ids: [${res.data.boardIds.join(', ')}]) {
          id
          name
        }
      }
    `)
  })
  .then((res) => {
    const boardIds = {
      candidatePipelineBoardId: _.find(res.data.boards, { name: CANDIDATE_PIPELINE_BOARD_NAME }).id,
      hiringPositionsBoardId: _.find(res.data.boards, { name: HIRING_POSITIONS_BOARD_NAME }).id,
      emailTemplatesBoardId: _.find(res.data.boards, { name: EMAIL_TEMPLATES_BOARD_NAME }).id,
    };

    return boardIds;
  });
}

function setBoardIdsToStorage(boardIds) {
  return monday.storage.instance.setItem(BOARD_IDS_STORAGE_KEY, boardIds);
}

export async function getAndSetBoardIds() {
  const storageBoardIds = await getBoardIdsFromStorage();
  if (storageBoardIds) return storageBoardIds;

  const apiBoardIds = await getBoardIdsFromApi();
  setBoardIdsToStorage(apiBoardIds);

  return apiBoardIds;
}

