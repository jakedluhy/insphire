import { useEffect } from 'react';
import _ from 'lodash';
import monday from './sdk';

import { useMondayData } from '../contexts/monday-data';
import { CANDIDATE_COLUMN_IDS_TO_KEYS, CANDIDATE_KEYS_TO_COLUMN_IDS } from './constants';
import { createItem, addFileToColumn, updateItem } from './items';
import { createColumnsMap, extractOptionsFromSelectColumn } from '../helpers/monday';
import useLocalStorage from '../hooks/use-local-storage';

export function useCandidateColumnOptions() {
  const { data: { candidatePipelineBoardId } } = useMondayData();
  const [candidateColumnOptions, setCandidateColumnOptions] = useLocalStorage('candidate-column-options', {});

  useEffect(() => {
    monday.api(`
      query {
        boards(ids: [${candidatePipelineBoardId}]) {
          columns {
            id
            settings_str
          }
        }
      }
    `)
    .then(res => {
      const columns = _.get(res, 'data.boards[0].columns', []);
      const underrepresentedGroupsColumn = _.find(columns, { id: CANDIDATE_KEYS_TO_COLUMN_IDS.fromUnderrepresentedGroup });
      const sourceColumn = _.find(columns, { id: CANDIDATE_KEYS_TO_COLUMN_IDS.source });

      setCandidateColumnOptions(oldOptionsObj => {
        const clonedObj = _.cloneDeep(oldOptionsObj);

        if (underrepresentedGroupsColumn) {
          clonedObj.underrepresentedGroupOptions = extractOptionsFromSelectColumn(underrepresentedGroupsColumn);
        }

        if (underrepresentedGroupsColumn) {
          clonedObj.sourceOptions = extractOptionsFromSelectColumn(sourceColumn);
        }

        return clonedObj;
      });
    });
  }, [candidatePipelineBoardId, setCandidateColumnOptions]);

  return { candidateColumnOptions };
}

export function fetchCandidates(boardId, groupIds) {
  const resolvedGroupIds = Array.isArray(groupIds) ? groupIds : [groupIds];

  return monday.api(`
    query {
      boards(ids: [${boardId}]) {
        groups(ids: [${resolvedGroupIds.map(gid => `"${gid}"`).join(', ')}]) {
          id
          title
          color
          items {
            id
            name
            updated_at
            created_at
            column_values {
              id
              title
              type
              text
              value
              additional_info
            }
          }
        }
      }
    }
  `)
  .then(res => {
    const groups = _.get(res, 'data.boards[0].groups', []);

    const data = [];
    groups.forEach(group => {
      group.items.forEach(item => {
        data.push({
          id: item.id,
          name: item.name,
          updatedAt: item.updated_at ? new Date(item.updated_at) : null,
          createdAt: item.created_at ? new Date(item.created_at) : null,
          columns: createColumnsMap(CANDIDATE_COLUMN_IDS_TO_KEYS, item.column_values),
          group: _.pick(group, ['id', 'title', 'color']),
        });
      });
    });

    return data;
  })
  .catch(() => {
    monday.execute('notice', { message: 'Failed to fetch candidates', type: 'error' });
    return [];
  });
}

export function createCandidate(boardId, groupId, name, columnValues = {}, apiToken) {
  const resumeColumnId = CANDIDATE_KEYS_TO_COLUMN_IDS.resume;
  const sanitizedColumnValues = _.omitBy(columnValues, (columnValue, columnKey) => {
    return columnValue === null || _.isEmpty(columnValue) || columnKey === resumeColumnId;
  });

  return createItem(boardId, groupId, name, sanitizedColumnValues)
  .then((item) => {
    const file = columnValues[resumeColumnId];
    const candidateObj = {
      id: item.id,
      name: item.name,
      updatedAt: item.updated_at ? new Date(item.updated_at) : null,
      columns: createColumnsMap(CANDIDATE_COLUMN_IDS_TO_KEYS, item.column_values),
      group: _.pick(item.group, ['id', 'title', 'color']),
    };

    if (!file || !apiToken) return candidateObj;

    return addFileToColumn(item.id, resumeColumnId, file, apiToken)
    .then((asset) => {
      candidateObj.columns.resume = {
        id: resumeColumnId,
        title: 'Resume',
        type: 'file',
        text: asset.url,
        value: {
          files: [{
            filetype: 'ASSET',
            assetId: asset.id,
            name: asset.name,
            isImage: false,
          }],
        },
        additionalInfo: null,
      };

      return candidateObj;
    });
  });
}

export function createCandidateUpdate(candidateId, updateBody) {
  return monday.api(`
    mutation($body: String!, $itemId: Int) {
      create_update(body: $body, item_id: $itemId) {
        id
        body
        text_body
      }
    }
  `, {
    variables: { body: updateBody, itemId: _.toNumber(candidateId) }
  })
  .then(res => _.get(res, 'data.create_update'));
}

export function updateCandidateGroup(candidateId, groupId) {
  return monday.api(`
    mutation {
      move_item_to_group(item_id: ${candidateId}, group_id: "${groupId}") {
        id
      }
    }
  `);
}

export function updateCandidate(boardId, candidateId, columnValues = {}) {
  const resumeColumnId = CANDIDATE_KEYS_TO_COLUMN_IDS.resume;
  const sanitizedColumnValues = _.omitBy(columnValues, (columnValue, columnKey) => {
    return columnValue === null || _.isEmpty(columnValue) || columnKey === resumeColumnId;
  });

  return updateItem(boardId, candidateId, sanitizedColumnValues)
  .then((item) => {
    const candidateObj = {
      id: item.id,
      name: item.name,
      updatedAt: item.updated_at ? new Date(item.updated_at) : null,
      columns: createColumnsMap(CANDIDATE_COLUMN_IDS_TO_KEYS, item.column_values),
      group: _.pick(item.group, ['id', 'title', 'color']),
    };

    return candidateObj;
  });
}
