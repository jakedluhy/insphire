import { useState, useEffect } from 'react';
import _ from 'lodash';

import { useMondayData } from '../contexts/monday-data';
import { EMAIL_TEMPLATES_COLUMN_IDS_TO_KEYS, EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS } from './constants';
import { createColumnsMap, extractOptionsFromSelectColumn } from '../helpers/monday';

import monday from './sdk';

export function useEmailTemplateTypes() {
  const { data: { emailTemplatesBoardId } } = useMondayData();
  const [emailTemplateTypes, setEmailTemplateTypes] = useState([]);

  useEffect(() => {
    monday.api(`
      query {
        boards(ids: [${emailTemplatesBoardId}]) {
          columns {
            id
            settings_str
          }
        }
      }
    `)
    .then(res => {
      const columns = _.get(res, 'data.boards[0].columns', []);
      const typeColumn = _.find(columns, { id: EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.type });

      if (typeColumn) {
        setEmailTemplateTypes(extractOptionsFromSelectColumn(typeColumn));
      } else {
        // TODO: Log Error
      }
    });
  }, [emailTemplatesBoardId, setEmailTemplateTypes]);

  return { emailTemplateTypes };
}

export function getAllEmailTemplates(boardId) {
  return monday.api(`
    query {
      boards(ids: [${boardId}]) {
        items {
          id
          name
          updated_at
          column_values {
            id
            title
            type
            text
            value
            additional_info
          }
        }
      }
    }
  `)
  .then(res => {
    const items = _.get(res, 'data.boards[0].items', []);

    return items.map(item => ({
      id: item.id,
      name: item.name,
      updatedAt: item.updated_at,
      columns: createColumnsMap(EMAIL_TEMPLATES_COLUMN_IDS_TO_KEYS, item.column_values),
    }));
  });
}
