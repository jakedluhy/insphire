import _ from 'lodash';

export const MONDAY_API_URL = 'https://api.monday.com/v2/';

export const getUserRoleKey = userId => `USER_${userId}_ROLE`;
export const BOARD_IDS_STORAGE_KEY = 'BOARD_IDS_KEY';
export const API_TOKEN_STORAGE_KEY = 'API_TOKEN_STORAGE_KEY';

export const CANDIDATE_PIPELINE_BOARD_NAME = 'Candidate Pipeline';
export const HIRING_POSITIONS_BOARD_NAME = 'Hiring Positions';
export const EMAIL_TEMPLATES_BOARD_NAME = 'Email Templates';

export const CANDIDATE_IN_PROGRESS_GROUP_ID = 'new_group86435';
export const CANDIDATE_PROSPECTS_GROUP_ID = 'group_title';
export const CANDIDATE_TO_REVISIT_GROUP_ID = 'new_group67436';
export const CANDIDATE_CLOSED_GROUP_ID = 'new_group72556';
export const CANDIDATE_NOT_A_FIT_GROUP_ID = 'new_group';

export const STATUS_ITEMS = [
  { label: 'In Progress', value: CANDIDATE_IN_PROGRESS_GROUP_ID },
  { label: 'Closed/Hired', value: CANDIDATE_CLOSED_GROUP_ID },
  { label: 'Prospect', value: CANDIDATE_PROSPECTS_GROUP_ID },
  { label: 'To Revisit', value: CANDIDATE_TO_REVISIT_GROUP_ID },
  { label: 'Not a Fit', value: CANDIDATE_NOT_A_FIT_GROUP_ID },
];

export const HIRING_POSITIONS_DEFAULT_GROUP_ID = 'topics';
export const EMAIL_TEMPLATES_DEFAULT_GROUP_ID = 'topics';

export const CANDIDATE_COLUMN_IDS_TO_KEYS = {
  connect_boards: 'position',
  date: 'lastContactedDate',
  date4: 'interviewContactDate',
  files: 'resume',
  linkedin: 'linkedin',
  people: 'hiringManager',
  people2: 'recruiter',
  person: 'upcomingInterviewer',
  phone: 'phone',
  text51: 'videoMeetingLink',
  stage: 'stage',
  status1: 'fromUnderrepresentedGroup',
  status2: 'source',
  text0: 'email',
  text3: 'expectedComp',
  text2: 'firstName',
  text5: 'lastName',
};
export const CANDIDATE_KEYS_TO_COLUMN_IDS = _.invert(CANDIDATE_COLUMN_IDS_TO_KEYS);

export const HIRING_POSITIONS_COLUMN_IDS_TO_KEYS = {
  link_to_candidate_pipeline: 'candidates',
  link_to_email_templates: 'emailTemplates',
  subitems9: 'stages',
};
export const HIRING_POSITIONS_KEYS_TO_COLUMN_IDS = _.invert(HIRING_POSITIONS_COLUMN_IDS_TO_KEYS);

export const HIRING_POSITION_STAGES_COLUMN_IDS_TO_KEYS = {
  long_text: 'description',
  numbers: 'duration',
  numbers8: 'orderIndex',
};
export const HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS = _.invert(HIRING_POSITION_STAGES_COLUMN_IDS_TO_KEYS);

export const EMAIL_TEMPLATES_COLUMN_IDS_TO_KEYS = {
  connect_boards: 'hiringPositions',
  status1: 'type',
  subject_line_template: 'subjectLineTemplate',
  long_text: 'template',
};
export const EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS = _.invert(EMAIL_TEMPLATES_COLUMN_IDS_TO_KEYS);
