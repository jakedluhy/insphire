import _ from 'lodash';

import monday from './sdk';
import { createColumnsMap } from '../helpers/monday';
import { HIRING_POSITIONS_COLUMN_IDS_TO_KEYS, HIRING_POSITION_STAGES_COLUMN_IDS_TO_KEYS } from './constants';

async function fetchStagesSubitems(subitemsValue) {
  if (!subitemsValue) return [];

  try {
    const stageIds = _.map(subitemsValue.linkedPulseIds, 'linkedPulseId');

    const res = await monday.api(`
      query {
        items(ids: [${stageIds.join(', ')}]) {
          id
          name
          updated_at
          column_values {
            id
            text
            title
            value
            additional_info
          }
        }
      }
    `);

    return _.get(res, 'data.items', []).map(item => ({
      id: item.id,
      name: item.name,
      updatedAt: item.updated_at,
      columns: createColumnsMap(HIRING_POSITION_STAGES_COLUMN_IDS_TO_KEYS, item.column_values),
    }));
  } catch(e) {
    // TODO log error
    return [];
  }
}

export async function getAllHiringPositions(boardId) {
  const res = await monday.api(`
    query {
      boards(ids: [${boardId}]) {
        items {
          id
          name
          updated_at
          column_values {
            id
            title
            type
            text
            value
            additional_info
          }
        }
      }
    }
  `);

  const items = _.get(res, 'data.boards[0].items', []);
  
  const hiringPositions = items.map(item => ({
    id: item.id,
    title: item.name,
    updatedAt: item.updated_at,
    columns: createColumnsMap(HIRING_POSITIONS_COLUMN_IDS_TO_KEYS, item.column_values),
  }));

  for (const hiringPosition of hiringPositions) {
    const newStages = await fetchStagesSubitems(hiringPosition.columns.stages.value);
    hiringPosition.columns.stages = newStages;
  }

  return hiringPositions;
}
