import * as React from "react";

function SvgImport(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.463 10.24a.496.496 0 01-.452.232.497.497 0 01-.407-.233L3.968 6.585a.503.503 0 010-.709.497.497 0 01.706 0l2.886 2.9V.5a.5.5 0 011 0v8.22l2.824-2.838a.498.498 0 01.707 0 .505.505 0 010 .71L8.463 10.24zM0 10.971a.5.5 0 011 0v4.023h14v-4.023a.5.5 0 011 0V15.5a.5.5 0 01-.5.5H.5a.5.5 0 01-.5-.5V10.972zM3 12.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgImport;
