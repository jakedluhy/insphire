import * as React from "react";

function SvgEmbed(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#Embed_svg__clip0)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M5.096 3.146a.5.5 0 11.707.708l-4.621 4.62 4.621 4.622a.5.5 0 11-.707.707l-4.95-4.95a.499.499 0 01-.145-.378.499.499 0 01.145-.379l4.95-4.95zm5.808 0a.5.5 0 10-.707.708l4.621 4.62-4.621 4.622a.5.5 0 10.707.707l4.95-4.95a.499.499 0 00.145-.378.499.499 0 00-.145-.379l-4.95-4.95z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="Embed_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgEmbed;
