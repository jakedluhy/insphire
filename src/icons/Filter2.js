import * as React from "react";

function SvgFilter2(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M4 7h8a1 1 0 110 2H4a1 1 0 010-2zM1 2h14a1 1 0 110 2H1a1 1 0 010-2zm6 10h2a1 1 0 110 2H7a1 1 0 110-2z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgFilter2;
