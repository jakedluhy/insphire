import * as React from "react";

function SvgDashboard(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 17 16" fill="none" {...props}>
      <rect
        x={0.5}
        y={0.5}
        width={15.071}
        height={15}
        rx={3.5}
        stroke="currentColor"
      />
      <mask id="Dashboard_svg__a" fill="#fff">
        <path d="M4 10.5a.5.5 0 011 0v2a.5.5 0 01-1 0v-2z" />
      </mask>
      <path
        d="M4 10.5v2h2v-2H4zm1 2v-2H3v2h2zm-.5-.5a.5.5 0 01.5.5H3A1.5 1.5 0 004.5 14v-2zm-.5.5a.5.5 0 01.5-.5v2A1.5 1.5 0 006 12.5H4zm.5-1.5a.5.5 0 01-.5-.5h2A1.5 1.5 0 004.5 9v2zm0-2A1.5 1.5 0 003 10.5h2a.5.5 0 01-.5.5V9z"
        fill="currentColor"
        mask="url(#Dashboard_svg__a)"
      />
      <mask id="Dashboard_svg__b" fill="#fff">
        <path d="M7.5 7.5a.5.5 0 011 0v5a.5.5 0 01-1 0v-5z" />
      </mask>
      <path
        d="M7.5 7.5v5h2v-5h-2zm1 5v-5h-2v5h2zM8 12a.5.5 0 01.5.5h-2A1.5 1.5 0 008 14v-2zm-.5.5A.5.5 0 018 12v2a1.5 1.5 0 001.5-1.5h-2zM8 8a.5.5 0 01-.5-.5h2A1.5 1.5 0 008 6v2zm0-2a1.5 1.5 0 00-1.5 1.5h2A.5.5 0 018 8V6z"
        fill="currentColor"
        mask="url(#Dashboard_svg__b)"
      />
      <mask id="Dashboard_svg__c" fill="#fff">
        <path d="M11 4.5a.5.5 0 011 0v8a.5.5 0 01-1 0v-8z" />
      </mask>
      <path
        d="M11 4.5v8h2v-8h-2zm1 8v-8h-2v8h2zm-.5-.5a.5.5 0 01.5.5h-2a1.5 1.5 0 001.5 1.5v-2zm-.5.5a.5.5 0 01.5-.5v2a1.5 1.5 0 001.5-1.5h-2zm.5-7.5a.5.5 0 01-.5-.5h2A1.5 1.5 0 0011.5 3v2zm0-2A1.5 1.5 0 0010 4.5h2a.5.5 0 01-.5.5V3z"
        fill="currentColor"
        mask="url(#Dashboard_svg__c)"
      />
    </svg>
  );
}

export default SvgDashboard;
