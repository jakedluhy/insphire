import * as React from "react";

function SvgBookmark(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.09 0h9.82C13.514 0 14 .47 14 1.058v13.884c0 .587-.485 1.058-1.09 1.058-.28 0-.555-.11-.771-.311l-4.171-4.047-4.107 4.047a1.11 1.11 0 01-1.54 0A1.017 1.017 0 012 14.942V1.058C2 .471 2.485 0 3.09 0zm.001.996c-.039 0-.064.024-.064.062v13.884c0 .022.006.036 0 .062.038.005.073.005.064 0l4.171-4.047a1.07 1.07 0 011.476 0l4.171 4.047c-.02-.006-.006 0 0 0 .04 0 .065-.024.065-.062V1.058c0-.038-.025-.062-.065-.062H3.091z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgBookmark;
