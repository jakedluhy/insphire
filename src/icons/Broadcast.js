import * as React from "react";

function SvgBroadcast(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#Broadcast_svg__clip0)">
        <mask id="Broadcast_svg__a" fill="#fff">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M15 7.5A5.5 5.5 0 009.5 2a.5.5 0 010-1A6.5 6.5 0 0116 7.5a.5.5 0 11-1 0zm-2 0A3.5 3.5 0 009.5 4a.5.5 0 010-1A4.5 4.5 0 0114 7.5a.5.5 0 01-1 0zm-2 0A1.5 1.5 0 009.5 6a.5.5 0 010-1A2.5 2.5 0 0112 7.5a.5.5 0 11-1 0zM7 4H2a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-2h-1v2a1 1 0 01-1 1H4V5h3V4zM2 5h1v8H2a1 1 0 01-1-1V6a1 1 0 011-1z"
          />
        </mask>
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M15 7.5A5.5 5.5 0 009.5 2a.5.5 0 010-1A6.5 6.5 0 0116 7.5a.5.5 0 11-1 0zm-2 0A3.5 3.5 0 009.5 4a.5.5 0 010-1A4.5 4.5 0 0114 7.5a.5.5 0 01-1 0zm-2 0A1.5 1.5 0 009.5 6a.5.5 0 010-1A2.5 2.5 0 0112 7.5a.5.5 0 11-1 0zM7 4H2a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-2h-1v2a1 1 0 01-1 1H4V5h3V4zM2 5h1v8H2a1 1 0 01-1-1V6a1 1 0 011-1z"
          fill="currentColor"
        />
        <path
          d="M9.5 2v1-1zm0 2V3v1zm4 4v1-1zm-4-3v1-1zm2 3V7v1zM7 4h1V3H7v1zm7 6h1V9h-1v1zm-1 0V9h-1v1h1zm-9 3H3v1h1v-1zm0-8V4H3v1h1zm3 0v1h1V5H7zM3 5h1V4H3v1zm0 8v1h1v-1H3zM9.5 3A4.5 4.5 0 0114 7.5h2A6.5 6.5 0 009.5 1v2zM8 1.5A1.5 1.5 0 009.5 3V1a.5.5 0 01.5.5H8zM9.5 0A1.5 1.5 0 008 1.5h2a.5.5 0 01-.5.5V0zM17 7.5A7.5 7.5 0 009.5 0v2A5.5 5.5 0 0115 7.5h2zM15.5 9A1.5 1.5 0 0017 7.5h-2a.5.5 0 01.5-.5v2zM14 7.5A1.5 1.5 0 0015.5 9V7a.5.5 0 01.5.5h-2zM9.5 5A2.5 2.5 0 0112 7.5h2A4.5 4.5 0 009.5 3v2zM8 3.5A1.5 1.5 0 009.5 5V3a.5.5 0 01.5.5H8zM9.5 2A1.5 1.5 0 008 3.5h2a.5.5 0 01-.5.5V2zM15 7.5A5.5 5.5 0 009.5 2v2A3.5 3.5 0 0113 7.5h2zM13.5 9A1.5 1.5 0 0015 7.5h-2a.5.5 0 01.5-.5v2zM12 7.5A1.5 1.5 0 0013.5 9V7a.5.5 0 01.5.5h-2zM9.5 7a.5.5 0 01.5.5h2A2.5 2.5 0 009.5 5v2zM8 5.5A1.5 1.5 0 009.5 7V5a.5.5 0 01.5.5H8zM9.5 4A1.5 1.5 0 008 5.5h2a.5.5 0 01-.5.5V4zM13 7.5A3.5 3.5 0 009.5 4v2A1.5 1.5 0 0111 7.5h2zM11.5 9A1.5 1.5 0 0013 7.5h-2a.5.5 0 01.5-.5v2zM10 7.5A1.5 1.5 0 0011.5 9V7a.5.5 0 01.5.5h-2zM4 5h3V3H4v2zM3 5h1V3H3v2zM2 5h1V3H2v2zM1 6a1 1 0 011-1V3a3 3 0 00-3 3h2zm0 6V6h-2v6h2zm1 1a1 1 0 01-1-1h-2a3 3 0 003 3v-2zm1 0H2v2h1v-2zm1 0H3v2h1v-2zm8 0H4v2h8v-2zm1-1a1 1 0 01-1 1v2a3 3 0 003-3h-2zm0-2v2h2v-2h-2zm0 1h1V9h-1v2zm1 1v-2h-2v2h2zm-2 2a2 2 0 002-2h-2v2zm-8 0h8v-2H4v2zM3 5v8h2V5H3zm4-1H4v2h3V4zM6 4v1h2V4H6zM3 4H2v2h1V4zm1 9V5H2v8h2zm-2 1h1v-2H2v2zm-2-2a2 2 0 002 2v-2H0zm0-6v6h2V6H0zm2-2a2 2 0 00-2 2h2V4z"
          fill="currentColor"
          mask="url(#Broadcast_svg__a)"
        />
      </g>
      <defs>
        <clipPath id="Broadcast_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgBroadcast;
