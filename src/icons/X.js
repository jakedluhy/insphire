import * as React from "react";
import PropTypes from 'prop-types';

function SvgX({ size, ...props }) {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8 7.279L14.765.515c.267-.268.642-.326.837-.131.195.195.136.57-.131.837L8.706 7.985l7.007 7.006c.27.27.33.647.135.843-.195.195-.572.134-.842-.135L7.999 8.692.97 15.72c-.267.267-.642.326-.837.13-.195-.194-.136-.569.131-.836l7.029-7.028L.427 1.119C.157.85.097.472.292.277c.195-.195.572-.135.842.135L8 7.28z"
        fill="currentColor"
      />
    </svg>
  );
}

SvgX.propTypes = {
  size: PropTypes.number,
};

SvgX.defaultProps = {
  size: 24,
};

export default SvgX;
