import * as React from "react";

function SvgSubItem(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.5 0a.5.5 0 000 1h.958a.505.505 0 00-.004.06V13.5a.5.5 0 00.5.5H7v1.1a.9.9 0 00.9.9h7.2a.9.9 0 00.9-.9v-3.2a.9.9 0 00-.9-.9H7.9a.9.9 0 00-.9.9V13H2.454V5H7v1.1a.9.9 0 00.9.9h7.2a.9.9 0 00.9-.9V2.9a.9.9 0 00-.9-.9H7.9a.9.9 0 00-.9.9V4H2.454V1.06L2.451 1H3.5a.5.5 0 000-1h-3zM8 4.5V6h7V3H8v1.5zM8 15v-3h7v3H8z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgSubItem;
