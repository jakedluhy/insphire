import * as React from "react";

function SvgRegenerate(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 17 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.71 14.933a7.5 7.5 0 01-3.634-3.183l.803-.463A6.573 6.573 0 102.9 4.68l1.478.632c.38.163.44.674.11.922L1.327 8.6a.536.536 0 01-.854-.366l-.469-3.92a.536.536 0 01.743-.557l1.294.554a7.5 7.5 0 113.669 10.62z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgRegenerate;
