import * as React from "react";

function SvgSort(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.476 6.912a.19.19 0 01-.164.088H3.688a.19.19 0 01-.164-.088.162.162 0 01.003-.175L7.84.147A.191.191 0 018 .062a.19.19 0 01.16.083l4.313 6.591c.034.054.036.12.003.175zm.063 2.363a.16.16 0 01-.003.172l-4.313 6.345a.191.191 0 01-.16.083.192.192 0 01-.161-.082L3.589 9.448a.16.16 0 01-.002-.173.19.19 0 01.163-.088h8.625c.068 0 .13.034.164.088z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgSort;
