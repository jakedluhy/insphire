import * as React from "react";

function SvgShare(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M13 5a2 2 0 100-4 2 2 0 000 4zm0 1a3 3 0 10-2.944-2.42l-4.62 2.668a3 3 0 10-.201 3.753l4.775 2.757a3 3 0 10.26-1.004l-4.5-2.598a2.99 2.99 0 00.107-2.008l4.54-2.62A2.999 2.999 0 0013 6zM3 10a2 2 0 100-4 2 2 0 000 4zm12 3a2 2 0 11-4 0 2 2 0 014 0z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgShare;
