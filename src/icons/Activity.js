import * as React from "react";

function SvgActivity(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#activity_svg__clip0)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8 .1a7.9 7.9 0 100 15.8A7.9 7.9 0 008 .1zM0 8a8 8 0 1116 0A8 8 0 010 8zm.745 0a7.255 7.255 0 1114.51 0A7.255 7.255 0 01.745 8zM8 .845a7.155 7.155 0 100 14.31A7.155 7.155 0 008 .845z"
          fill="currentColor"
        />
        <path
          d="M15.205 8A7.205 7.205 0 10.795 8a7.205 7.205 0 0014.41 0zm.745 0A7.95 7.95 0 11.05 8a7.95 7.95 0 0115.9 0z"
          fill="currentColor"
        />
        <path
          d="M.5 8.5h4.993a.01.01 0 01.01.006l1.45 3.87a.04.04 0 00.077-.004l1.938-8.235c.009-.039.064-.041.077-.003l1.453 4.36a.01.01 0 00.01.006H15.5"
          stroke="currentColor"
        />
      </g>
      <defs>
        <clipPath id="activity_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgActivity;
