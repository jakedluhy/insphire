import * as React from "react";

function SvgKeyboard(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1 4.889h14v7.222H1V4.89zm-1 0C0 4.398.448 4 1 4h14c.552 0 1 .398 1 .889v7.222c0 .491-.448.889-1 .889H1c-.552 0-1-.398-1-.889V4.89zM4.5 10a.5.5 0 000 1h7a.5.5 0 000-1h-7z"
        fill="currentColor"
      />
      <path
        d="M4 2a1 1 0 011-1v3H4V2zM9 0h1v1a1 1 0 01-1 1V0z"
        fill="currentColor"
      />
      <path d="M4 2a1 1 0 011-1h5a1 1 0 01-1 1H4z" fill="currentColor" />
    </svg>
  );
}

export default SvgKeyboard;
