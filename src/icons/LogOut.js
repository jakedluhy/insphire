import * as React from "react";

function SvgLogout(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.528.5a.5.5 0 00-.5-.5H.5a.5.5 0 00-.5.525V15.5a.5.5 0 00.5.5H5.028a.5.5 0 000-1H1.006V1h4.022a.5.5 0 00.5-.5zm8.711 7.274a.496.496 0 01.232.452.497.497 0 01-.232.408l-3.654 3.635a.503.503 0 01-.709 0 .497.497 0 010-.705l2.9-2.886H4.5a.5.5 0 010-1h8.22L9.883 4.854a.498.498 0 010-.708.504.504 0 01.71 0l3.647 3.628z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgLogout;
