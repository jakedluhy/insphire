import * as React from "react";

function SvgInvite(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        clipRule="evenodd"
        d="M7.067 7.533a3.267 3.267 0 100-6.533 3.267 3.267 0 000 6.533z"
        stroke="currentColor"
        strokeWidth={0.875}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.768 9.867A5.581 5.581 0 007.533 9.4H6.6A5.6 5.6 0 001 15h8.4M12.2 9.4V15M9.4 12.2H15"
        stroke="currentColor"
        strokeWidth={0.875}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgInvite;
