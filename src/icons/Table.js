import * as React from "react";

function SvgTable(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 1h7a2 2 0 012 2v2H6V1zm10 5V3a3 3 0 00-3-3H3a3 3 0 00-3 3v10a3 3 0 003 3h10a3 3 0 003-3V6zm-1 4V6H6v4h9zM5 10V6H1v4h4zm-4 1h4v4H3a2 2 0 01-2-2v-2zm5 0h9v2a2 2 0 01-2 2H6v-4zM1 3v2h4V1H3a2 2 0 00-2 2z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgTable;
