import * as React from "react";

function SvgMoveto(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16 8A8 8 0 110 8a8 8 0 0116 0zM1.004 8a6.996 6.996 0 1013.992 0A6.996 6.996 0 001.004 8zm12.069-.366a.502.502 0 01.025.733l-3.727 3.727a.502.502 0 11-.71-.71l2.87-2.87H3.244a.502.502 0 010-1.003h8.286L8.66 4.64a.502.502 0 11.71-.71l3.703 3.703z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgMoveto;
