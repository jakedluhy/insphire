import * as React from "react";

function SvgReply(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.442 5.679s9.473-1.13 9.473 8.283c-3.031-5.648-9.473-3.765-9.473-3.765v3.012L0 7.938l6.442-5.271v3.012z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgReply;
