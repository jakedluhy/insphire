import * as React from "react";

function SvgPrint(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#print_svg__clip0)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.145 1.146A.5.5 0 014.5 1h7a.5.5 0 01.5.5V4h-8V1.5a.5.5 0 01.146-.354zM3 4V1.5a1.5 1.5 0 011.5-1.5h7a1.5 1.5 0 011.5 1.5V4h1.5a1.5 1.5 0 011.5 1.5v5a1.5 1.5 0 01-1.5 1.5h-1.5v3.5a.5.5 0 01-.5.5h-9a.5.5 0 01-.5-.5V12h-1.5a1.5 1.5 0 01-1.5-1.5v-5a1.5 1.5 0 011.5-1.5h1.5zm.5 1h-2a.5.5 0 00-.5.5v5a.5.5 0 00.5.5h1.5V8.5a.5.5 0 01.5-.5h9a.5.5 0 01.5.5V11h1.5a.5.5 0 00.5-.5v-5a.5.5 0 00-.5-.5h-11zm8.5 4v6h-8V9h8zm-9.5-3a.5.5 0 000 1h1a.5.5 0 100-1h-1zm2.5 4.5a.5.5 0 01.5-.5h5a.5.5 0 110 1h-5a.5.5 0 01-.5-.5zm.5 1.5a.5.5 0 000 1h3.5a.5.5 0 000-1h-3.5z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="print_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgPrint;
