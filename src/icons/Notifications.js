import * as React from "react";

function SvgNotifications(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 17 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.002 0c3.73 0 6.753 2.93 6.753 6.542 0 3.618.399 5.748 1.053 6.256.38.296.164.89-.325.89h-4.454C10.724 15.011 9.467 16 7.97 16c-1.499 0-2.756-.988-3.061-2.313H.52c-.431 0-.674-.479-.408-.807.738-.913 1.137-3.042 1.137-6.338C1.25 2.929 4.272 0 8.002 0zM5.985 13.688C6.265 14.448 7.045 15 7.97 15c.923 0 1.702-.55 1.983-1.313H5.985zm8.515-1c-.542-1.166-.783-3.196-.783-6.146C13.717 3.48 11.157 1 8.002 1 4.845 1 2.286 3.48 2.286 6.542c0 2.82-.279 4.86-.869 6.146H14.5z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgNotifications;
