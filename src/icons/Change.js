import * as React from "react";

function SvgChange(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.354 2.146a.5.5 0 010 .708L2.207 6H13a.5.5 0 110 1H1a.5.5 0 01-.354-.854l4-4a.5.5 0 01.708 0zM10.646 13.854a.5.5 0 010-.708L13.793 10H3a.5.5 0 110-1h12a.5.5 0 01.354.854l-4 4a.5.5 0 01-.708 0z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgChange;
