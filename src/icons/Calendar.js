import * as React from "react";

function SvgCalendar(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M13 .5a.5.5 0 00-1 0V2H4V.5a.5.5 0 00-1 0V2H2a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V4a2 2 0 00-2-2h-1V.5zM3 3H2a1 1 0 00-1 1v1h14V4a1 1 0 00-1-1h-1v.5a.5.5 0 01-1 0V3H4v.5a.5.5 0 01-1 0V3zm12 3v8a1 1 0 01-1 1H2a1 1 0 01-1-1V6h14z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgCalendar;
