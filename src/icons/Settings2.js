import * as React from "react";

function SvgSettings2(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#Settings2_svg__clip0)" fill="currentColor">
        <path d="M.356 3.305h2.591a1.97 1.97 0 003.89 0h8.829a.33.33 0 100-.656H6.867a1.97 1.97 0 00-3.889 0H.356a.33.33 0 100 .66v-.004zm4.536-1.65a1.32 1.32 0 110 2.64 1.32 1.32 0 010-2.64zM15.634 7.603h-2.692a1.97 1.97 0 00-3.889 0H.343a.33.33 0 100 .66h8.723a1.97 1.97 0 003.89 0h2.692a.33.33 0 000-.66h-.014zm-4.636 1.65a1.32 1.32 0 110-2.64 1.32 1.32 0 010 2.64zM15.666 12.557H6.867a1.971 1.971 0 00-3.889 0H.356a.33.33 0 000 .66h2.591a1.971 1.971 0 003.89 0h8.829a.33.33 0 000-.66zm-10.774 1.65a1.32 1.32 0 110-2.64 1.32 1.32 0 010 2.64z" />
      </g>
      <defs>
        <clipPath id="Settings2_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgSettings2;
