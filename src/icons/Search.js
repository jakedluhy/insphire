import * as React from "react";
import PropTypes from 'prop-types';

function SvgSearch({ size, ...props }) {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.875 0a6.844 6.844 0 015.11 11.397l3.833 3.745a.5.5 0 11-.699.716l-3.853-3.765A6.844 6.844 0 116.875 0zm0 1a5.844 5.844 0 100 11.687A5.844 5.844 0 006.875 1z"
        fill="currentColor"
      />
    </svg>
  );
}

SvgSearch.propTypes = {
  size: PropTypes.number,
};

SvgSearch.defaultProps = {
  size: 24,
};

export default SvgSearch;
