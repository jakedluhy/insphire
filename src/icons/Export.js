import * as React from "react";

function SvgExport(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.597.233A.503.503 0 018.049 0a.497.497 0 01.407.232l3.635 3.654a.503.503 0 010 .709.497.497 0 01-.705 0L8.5 1.695v8.277a.5.5 0 01-1 0v-8.22L4.676 4.59a.498.498 0 01-.707 0 .504.504 0 010-.71L7.597.232zM0 10.972a.5.5 0 111 0v4.023h14v-4.023a.5.5 0 111 0V15.5a.5.5 0 01-.5.5H.5a.5.5 0 01-.5-.5V10.972zm3.5 2.013c0-.278.225-.503.503-.503h7.994a.503.503 0 110 1.005H4.003a.503.503 0 01-.503-.502z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgExport;
