import * as React from "react";

function SvgUnlockfull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M2.692 6h10.616C14.242 6 15 6.682 15 7.524v6.952c0 .842-.758 1.524-1.692 1.524H2.692C1.758 16 1 15.318 1 14.476V7.524C1 6.682 1.758 6 2.692 6z"
        fill="currentColor"
      />
      <path
        d="M8.5 11.556V9.444C8.5 9.2 8.276 9 8 9s-.5.199-.5.444v2.112c0 .245.224.444.5.444s.5-.199.5-.444z"
        fill="#fff"
      />
      <path
        d="M5 6.534V3.786C5 2.21 6.377.932 8.077.932c.854 0 1.65.323 2.228.885.272.265.539.768.717 1.31.08.247.36.385.624.31.264-.076.413-.336.332-.582-.22-.67-.55-1.292-.95-1.683A4.222 4.222 0 008.077 0C5.824 0 4 1.694 4 3.786v2.748c0 .257.224.466.5.466s.5-.209.5-.466z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgUnlockfull;
