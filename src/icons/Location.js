import * as React from "react";

function SvgLocation(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.667 6.696c0 2.147-1.148 4.327-3.046 6.448a21.797 21.797 0 01-2.996 2.761.496.496 0 01-.583 0c-.05-.036-.14-.104-.265-.201a21.793 21.793 0 01-2.731-2.56C3.147 11.024 2 8.844 2 6.696 2 2.939 4.71 0 8.333 0c3.624 0 6.334 2.94 6.334 6.696zM8.922 14.39a20.8 20.8 0 001.958-1.912c1.748-1.955 2.788-3.933 2.788-5.781C13.668 3.476 11.388 1 8.333 1 5.28 1 2.998 3.476 2.998 6.697c0 1.848 1.04 3.826 2.789 5.781a20.802 20.802 0 002.546 2.397c.18-.142.377-.304.589-.486zm-.589-4.765c1.775 0 3.214-1.455 3.214-3.25s-1.439-3.25-3.214-3.25c-1.774 0-3.213 1.455-3.213 3.25s1.439 3.25 3.213 3.25zm0-1c-1.223 0-2.215-1.007-2.215-2.25s.992-2.25 2.215-2.25c1.224 0 2.216 1.007 2.216 2.25s-.992 2.25-2.216 2.25z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgLocation;
