import * as React from "react";
import PropTypes from 'prop-types';

function SvgPlus({ size, ...props }) {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.5 8L7.502.5a.5.5 0 111 0V8h6.997a.501.501 0 110 1H8.5L8.5 15.5a.5.5 0 11-1 0V9H.503a.501.501 0 110-1h6.999z"
        fill="currentColor"
      />
    </svg>
  );
}

SvgPlus.propTypes = {
  size: PropTypes.number,
};

SvgPlus.defaultProps = {
  size: 24,
};


export default SvgPlus;
