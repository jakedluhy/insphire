import * as React from "react";

function SvgMegaphone(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4 8.924l11 2.539V1.977L4 4.515v4.409zM3 3.72H1a1 1 0 00-1 1v4a1 1 0 001 1h2l2.743.633 1.01 3.712a1.623 1.623 0 003.137-.835l-.531-2.043 5.416 1.25A1 1 0 0016 11.463V1.977a1 1 0 00-1.225-.974L3 3.72zm5.26 7.214l-1.411-.326.868 3.194a.623.623 0 001.205-.32l-.662-2.548zM3 4.72v4H1v-4h2z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgMegaphone;
