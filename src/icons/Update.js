import * as React from "react";
import PropTypes from 'prop-types';

function SvgUpdate({ size, ...props }) {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.775 12.91a.987.987 0 01.714-.052 8.48 8.48 0 002.51.374c4.067 0 7.001-2.694 7.001-5.608 0-2.916-2.934-5.61-7-5.61-4.067 0-7 2.694-7 5.61 0 1.492.737 2.893 2.025 3.937.368.299.48.819.27 1.246l-.516 1.045 1.996-.943zM.6 16l1.8-3.647C.918 11.151 0 9.476 0 7.623 0 3.966 3.58 1 8 1c4.419 0 8 2.965 8 6.624 0 3.657-3.581 6.622-8 6.622-.987 0-1.93-.147-2.803-.417L.6 16z"
        fill="currentColor"
      />
    </svg>
  );
}

SvgUpdate.propTypes = {
  size: PropTypes.number,
};

SvgUpdate.defaultProps = {
  size: 24,
};

export default SvgUpdate;
