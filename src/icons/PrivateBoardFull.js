import * as React from "react";

function SvgPrivateBoardFull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M12.018 6.414v-2.09A4.337 4.337 0 007.694 0a4.337 4.337 0 00-4.325 4.324V6.56A5.653 5.653 0 002 10.234C2 13.405 4.595 16 7.766 16c3.17 0 5.765-2.595 5.765-5.766-.072-1.441-.576-2.739-1.513-3.82z"
        fill="#fff"
      />
      <path
        d="M12.018 6.414v-2.09A4.337 4.337 0 007.694 0a4.337 4.337 0 00-4.325 4.324V6.56A5.653 5.653 0 002 10.234C2 13.405 4.595 16 7.766 16c3.17 0 5.765-2.595 5.765-5.766-.072-1.441-.576-2.739-1.513-3.82zm-7.207-1.08v-1.01a2.891 2.891 0 012.883-2.883 2.891 2.891 0 012.883 2.883v.937c-.793-.504-1.802-.72-2.811-.72-1.081 0-2.09.288-2.955.792z"
        fill="#F65F7C"
      />
      <path
        d="M7.658 9c-.647 0-1.173.525-1.173 1.172 0 .41.213.766.526.976v1.557a.641.641 0 101.282 0v-1.557c.343-.21.53-.57.53-.976C8.824 9.525 8.306 9 7.659 9z"
        fill="#fff"
      />
    </svg>
  );
}

export default SvgPrivateBoardFull;
