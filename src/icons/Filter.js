import * as React from "react";

function SvgFilter(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.05 8.495v6.288c0 .343.349.58.674.457l2.904-1.094a.49.49 0 00.32-.457V8.495l5.88-7.029a.488.488 0 00-.383-.8H.553c-.42 0-.65.481-.383.8l5.88 7.029zm8.343-6.85L9.068 8.007a.485.485 0 00-.113.311v5.034l-1.913.72V8.319a.485.485 0 00-.113-.31L1.604 1.643h12.789z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgFilter;
