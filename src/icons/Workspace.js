import * as React from "react";

function SvgWorkspace(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M1 .5h5.334a.5.5 0 01.5.5v5.333a.5.5 0 01-.5.5H1a.5.5 0 01-.5-.5V1a.5.5 0 01.5-.5zM9.667.5h5.332a.5.5 0 01.5.5v5.333a.5.5 0 01-.5.5H9.667a.5.5 0 01-.5-.5V1a.5.5 0 01.5-.5zM1 9.167h5.333a.5.5 0 01.5.5V15a.5.5 0 01-.5.5H1a.5.5 0 01-.5-.5V9.667a.5.5 0 01.5-.5zM9.667 9.167h5.332a.5.5 0 01.5.5V15a.5.5 0 01-.5.5H9.667a.5.5 0 01-.5-.5V9.667a.5.5 0 01.5-.5z"
        stroke="currentColor"
      />
    </svg>
  );
}

export default SvgWorkspace;
