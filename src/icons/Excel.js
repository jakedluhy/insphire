import * as React from "react";

function SvgExcel(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.051 13.137H9.284l-.01-.717h1.384v-1.23h-1.4l-.006-.726h1.406V9.235H9.241l-.007-.727h1.424V7.28H9.23v-.727h1.428V5.323H9.23l.005-.893h1.429V3.201h-1.43V1.912h5.817v11.225zM9.303 1.334h5.659c.376 0 .681.274.681.612v11.089c0 .338-.305.612-.681.612h-5.66v1.976L0 14.021V1.436L9.303 0v1.334zm-6.8 3.689l1.25 2.79L2.4 10.417l1.165.102.856-2.162.856 2.248 1.268.086-1.456-2.95L6.545 4.8l-1.2.069-.89 2.157-.753-2.072-1.2.069z"
        fill="currentColor"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.2 4.43V3.2h2.415v1.23H11.2zm0 1.6V4.8h2.415v1.23H11.2zm0 1.6V6.4h2.415v1.23H11.2zm0 1.6V8h2.415v1.23H11.2zm0 1.6V9.6h2.415v1.23H11.2zm0 1.6V11.2h2.415v1.23H11.2z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgExcel;
