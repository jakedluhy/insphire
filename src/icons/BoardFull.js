import * as React from "react";

function SvgBoardFull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <rect y={2} width={16} height={12} rx={1.6} fill="currentColor" />
      <path stroke="#fff" d="M3.7 2v12" />
    </svg>
  );
}

export default SvgBoardFull;
