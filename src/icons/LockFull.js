import * as React from "react";

function SvgLockfull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M2.692 6h10.616C14.242 6 15 6.682 15 7.524v6.952c0 .842-.758 1.524-1.692 1.524H2.692C1.758 16 1 15.318 1 14.476V7.524C1 6.682 1.758 6 2.692 6z"
        fill="currentColor"
      />
      <path
        d="M8.5 11.556V9.444C8.5 9.2 8.276 9 8 9s-.5.199-.5.444v2.112c0 .245.224.444.5.444s.5-.199.5-.444z"
        fill="#fff"
      />
      <path
        d="M4.962 6.53V3.823C4.962 2.234 6.32.94 8 .94c1.68 0 3.038 1.293 3.038 2.883v2.705c0 .26.215.471.48.471.267 0 .482-.21.482-.47V3.823C12 1.709 10.206 0 8 0 5.793 0 4 1.708 4 3.824v2.705c0 .26.215.471.481.471s.481-.21.481-.47z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgLockfull;
