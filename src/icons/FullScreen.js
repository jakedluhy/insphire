import * as React from "react";

function SvgFullScreen(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#full_screen_svg__clip0)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.25 1H1.658l4.884 4.884a.5.5 0 01-.707.707L1 1.756V4.25a.5.5 0 01-1 0V.7A.7.7 0 01.7 0h3.55a.5.5 0 010 1zm7-.5a.5.5 0 01.5-.5h3.55a.7.7 0 01.7.7v3.55a.5.5 0 01-1 0V1.655L10.064 6.59a.5.5 0 11-.707-.707L14.24 1H11.75a.5.5 0 01-.5-.5zM.5 11.25a.5.5 0 01.5.5v2.49l4.832-4.832a.5.5 0 01.707.707L1.655 15H4.25a.5.5 0 010 1H.7a.7.7 0 01-.7-.7v-3.55a.5.5 0 01.5-.5zm15 0a.5.5 0 01.5.5v3.55a.7.7 0 01-.7.7h-3.55a.5.5 0 010-1h2.493L9.36 10.116a.5.5 0 11.707-.708L15 14.342V11.75a.5.5 0 01.5-.5z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="full_screen_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgFullScreen;
