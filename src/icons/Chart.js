import * as React from "react";

function SvgChart(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#chart_svg__clip0)" fill="currentColor">
        <path d="M7.17 8.851h5.736c.264 0 .478.214.478.478a6.692 6.692 0 11-6.692-6.692c.264 0 .478.214.478.478v5.736zM.956 9.33a5.736 5.736 0 0011.452.478H6.692a.478.478 0 01-.478-.478V3.613A5.737 5.737 0 00.956 9.329z" />
        <path d="M9.394 7.153a.478.478 0 01-.478-.478V.46c0-.265.214-.479.478-.479a6.692 6.692 0 016.692 6.693.478.478 0 01-.478.478H9.394zm.478-.957h5.238A5.738 5.738 0 009.872.958v5.238z" />
      </g>
      <defs>
        <clipPath id="chart_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgChart;
