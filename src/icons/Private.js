import * as React from "react";

function SvgPrivate(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 7s-.352-.47-1-.954C10.311 5.531 9.288 5 8 5s-2.311.531-3 1.046C4.352 6.53 4 7 4 7a5 5 0 108 0zm2 3a5.985 5.985 0 00-2-4.472V4a4 4 0 00-8 0v1.528A6 6 0 1014 10zm-3-6v.803A5.972 5.972 0 008 4a5.972 5.972 0 00-3 .803V4a3 3 0 016 0zM9 9a1 1 0 01-.5.866V11.5a.5.5 0 01-1 0V9.866A1 1 0 119 9z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgPrivate;
