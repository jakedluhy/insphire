import * as React from "react";

function SvgDelete(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <mask id="Delete_svg__a" fill="#fff">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6 1h4v1H6V1zM5 2V1a1 1 0 011-1h4a1 1 0 011 1v1h4.5a.5.5 0 010 1H14v12a1 1 0 01-1 1H3a1 1 0 01-1-1V3H.5a.5.5 0 010-1H5zm6 1H3v12h10V3h-2zM6 6.5a.5.5 0 011 0v5a.5.5 0 01-1 0v-5zM9.5 6a.5.5 0 00-.5.5v5a.5.5 0 001 0v-5a.5.5 0 00-.5-.5z"
        />
      </mask>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 1h4v1H6V1zM5 2V1a1 1 0 011-1h4a1 1 0 011 1v1h4.5a.5.5 0 010 1H14v12a1 1 0 01-1 1H3a1 1 0 01-1-1V3H.5a.5.5 0 010-1H5zm6 1H3v12h10V3h-2zM6 6.5a.5.5 0 011 0v5a.5.5 0 01-1 0v-5zM9.5 6a.5.5 0 00-.5.5v5a.5.5 0 001 0v-5a.5.5 0 00-.5-.5z"
        fill="currentColor"
      />
      <path
        d="M10 1h1V0h-1v1zM6 1V0H5v1h1zm4 1v1h1V2h-1zM6 2H5v1h1V2zM5 2v1h1V2H5zm6 0h-1v1h1V2zm3 1V2h-1v1h1zM2 3h1V2H2v1zm1 0V2H2v1h1zm0 12H2v1h1v-1zm10 0v1h1v-1h-1zm0-12h1V2h-1v1zm-3-3H6v2h4V0zm1 2V1H9v1h2zM6 3h4V1H6v2zM5 1v1h2V1H5zM4 1v1h2V1H4zm2-2a2 2 0 00-2 2h2v-2zm4 0H6v2h4v-2zm2 2a2 2 0 00-2-2v2h2zm0 1V1h-2v1h2zm2-1h-3v2h3V1zm1.5 0H14v2h1.5V1zM17 2.5A1.5 1.5 0 0015.5 1v2a.5.5 0 01-.5-.5h2zM15.5 4A1.5 1.5 0 0017 2.5h-2a.5.5 0 01.5-.5v2zM14 4h1.5V2H14v2zm1 11V3h-2v12h2zm-2 2a2 2 0 002-2h-2v2zM3 17h10v-2H3v2zm-2-2a2 2 0 002 2v-2H1zM1 3v12h2V3H1zM.5 4H2V2H.5v2zM-1 2.5A1.5 1.5 0 00.5 4V2a.5.5 0 01.5.5h-2zM.5 1A1.5 1.5 0 00-1 2.5h2a.5.5 0 01-.5.5V1zM2 1H.5v2H2V1zm3 0H2v2h3V1zm0 3h6V2H5v2zM3 4h2V2H3v2zm1 11V3H2v12h2zm9-1H3v2h10v-2zM12 3v12h2V3h-2zm-1 1h2V2h-2v2zM6.5 5A1.5 1.5 0 005 6.5h2a.5.5 0 01-.5.5V5zM8 6.5A1.5 1.5 0 006.5 5v2a.5.5 0 01-.5-.5h2zm0 5v-5H6v5h2zM6.5 13A1.5 1.5 0 008 11.5H6a.5.5 0 01.5-.5v2zM5 11.5A1.5 1.5 0 006.5 13v-2a.5.5 0 01.5.5H5zm0-5v5h2v-5H5zm5 0a.5.5 0 01-.5.5V5A1.5 1.5 0 008 6.5h2zm0 5v-5H8v5h2zm-.5-.5a.5.5 0 01.5.5H8A1.5 1.5 0 009.5 13v-2zm-.5.5a.5.5 0 01.5-.5v2a1.5 1.5 0 001.5-1.5H9zm0-5v5h2v-5H9zm.5.5a.5.5 0 01-.5-.5h2A1.5 1.5 0 009.5 5v2z"
        fill="currentColor"
        mask="url(#Delete_svg__a)"
      />
    </svg>
  );
}

export default SvgDelete;
