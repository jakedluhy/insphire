import * as React from "react";

function SvgWorkspaceFull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M6.334 0H1a1 1 0 00-1 1v5.333a1 1 0 001 1h5.333a1 1 0 001-1V1a1 1 0 00-1-1zM15 0H9.666a1 1 0 00-1 1v5.333a1 1 0 001 1h5.332a1 1 0 001-1V1a1 1 0 00-1-1zM6.333 8.667H1a1 1 0 00-1 1V15a1 1 0 001 1h5.333a1 1 0 001-1V9.667a1 1 0 00-1-1zM15 8.667H9.666a1 1 0 00-1 1V15a1 1 0 001 1h5.332a1 1 0 001-1V9.667a1 1 0 00-1-1z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgWorkspaceFull;
