import * as React from "react";

function SvgSortCircle(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" {...props}>
      <circle cx={12} cy={12} r={12} fill="#0085FF" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.916 11.048a.167.167 0 01-.143.077H8.227a.166.166 0 01-.143-.077.142.142 0 01.002-.153l3.774-5.767a.167.167 0 01.14-.073c.057 0 .11.027.14.073l3.774 5.767c.03.047.031.106.002.153zm.055 2.067a.14.14 0 01-.002.151l-3.774 5.552a.167.167 0 01-.14.073.168.168 0 01-.14-.073L8.14 13.267a.14.14 0 01-.002-.152.166.166 0 01.142-.076h7.547c.06 0 .114.03.143.076z"
        fill="#fff"
      />
    </svg>
  );
}

export default SvgSortCircle;
