import * as React from "react";

function SvgBolt(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.829 5.886H8.542L9.628.553c.15-.468-.482-.765-.743-.367L2.77 9.379c-.253.301-.015.734.4.736h4.286L6.37 15.447c-.15.468.482.765.743.367l6.116-9.193c.252-.301.015-.734-.4-.735zm-5.2.367c-.138.314.09.61.4.552h4L7.687 13.3l.686-3.554c.132-.3-.076-.614-.343-.552H3.97l4.344-6.497-.686 3.555z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgBolt;
