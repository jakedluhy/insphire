import * as React from "react";

function SvgClock(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 118 0a8 8 0 010 16zm4.264-4.788a.5.5 0 11-.676.737l-3.906-3.58a.5.5 0 01-.162-.37V4.42a.5.5 0 011 0v3.36l3.744 3.433z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgClock;
