import * as React from "react";

function SvgBoard(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.316 14c.93 0 1.684-.768 1.684-1.714V3.714C16 2.768 15.246 2 14.316 2H1.684C.754 2 0 2.768 0 3.714v8.572C0 13.232.755 14 1.684 14h12.632zM15 3.918v8.164a.91.91 0 01-.902.918H1.902A.91.91 0 011 12.082V3.918c0-.51.407-.918.902-.918h12.196a.91.91 0 01.902.918z"
        fill="currentColor"
      />
      <path
        d="M3.375 2.625c0-.207.224-.375.5-.375s.5.168.5.375v10.75c0 .207-.224.375-.5.375s-.5-.168-.5-.375V2.625z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgBoard;
