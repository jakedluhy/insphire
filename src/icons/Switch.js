import * as React from "react";

function SvgSwitch(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.646 9.854a.5.5 0 01.707-.708l2.122 2.122a.5.5 0 010 .707l-2.122 2.121a.5.5 0 11-.707-.707L13.036 12H1.5a.5.5 0 010-1h11.293l-1.147-1.146zM3.975 2.854a.5.5 0 10-.707-.708L1.146 4.268a.5.5 0 000 .707l2.122 2.121a.5.5 0 10.707-.707L2.585 5H14.5a.5.5 0 000-1H2.828l1.147-1.146z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgSwitch;
