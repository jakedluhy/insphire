import * as React from "react";

function SvgWorkingstatus(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path fill="#fff" d="M0 0h16v16H0z" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.633 4.8a2.4 2.4 0 014.734 0H5.633zm-.808 0a3.2 3.2 0 016.35 0H15.6c.22 0 .4.18.4.4v10.4a.4.4 0 01-.4.4H.4a.4.4 0 01-.4-.4V5.2c0-.22.18-.4.4-.4h4.425zm10.375 4H9.6v-.4a.4.4 0 00-.4-.4H6.8a.4.4 0 00-.4.4v.4H.8V5.6h14.4v3.2zm-6.4 0v2H7.2v-2h1.6zm-2.4 2.4V9.6H.8v5.6h14.4V9.6H9.6v1.6a.4.4 0 01-.4.4H6.8a.4.4 0 01-.4-.4z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgWorkingstatus;
