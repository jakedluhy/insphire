import * as React from "react";

function SvgPage(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.588 16A1.594 1.594 0 012 14.4V1.6C2 .717 2.712 0 3.588 0H9.54c.45 0 .88.194 1.182.534l2.873 3.232c.261.292.405.673.405 1.069v9.564c0 .885-.711 1.601-1.588 1.601H3.588zM13.01 5.5v8.9c0 .331-.267.6-.598.6H3.588a.599.599 0 01-.598-.6V1.6a.6.6 0 01.598-.6H9.36l.001 4c0 .276.222.5.495.5h3.153zm-2.66-3.888l2.51 2.823c.018.02.035.042.05.065h-2.558l-.001-2.888z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgPage;
