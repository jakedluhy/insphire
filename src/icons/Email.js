import * as React from "react";

function SvgEmail(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16 12.32c0 .918-.693 1.68-1.571 1.68H1.57C.693 14 0 13.238 0 12.32V3.68C0 2.763.693 2 1.571 2H14.43C15.307 2 16 2.762 16 3.68v8.64zm-1.724-9.315L8.002 6.908 1.84 3.005h12.437zm.724.732v8.582c0 .383-.267.676-.571.676H1.57c-.304 0-.571-.293-.571-.676V3.681v-.019l6.733 4.264a.498.498 0 00.53.002L15 3.737z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgEmail;
