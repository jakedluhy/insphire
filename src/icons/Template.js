import * as React from "react";

function SvgTemplate(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M14.321 2.374a.4.4 0 00-.507-.507l-2.492.845a.4.4 0 01-.368-.058L8.845 1.08a.4.4 0 00-.64.326l.034 2.631a.4.4 0 01-.169.332l-2.148 1.52a.4.4 0 00.112.708l2.513.781a.4.4 0 01.263.263l.781 2.513a.4.4 0 00.709.113l1.52-2.149a.4.4 0 01.331-.17l2.632.035a.4.4 0 00.325-.64l-1.574-2.109a.4.4 0 01-.058-.367l.845-2.493z"
        fill="currentColor"
      />
      <path
        d="M8.218 7.97a.5.5 0 00-.708 0l-6.364 6.365a.5.5 0 10.708.707l6.364-6.364a.5.5 0 000-.707z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgTemplate;
