import * as React from "react";

function SvgExternallink(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        d="M15.471 0a.518.518 0 00-.113.013l-5.33.013a.524.524 0 00-.52.524c-.001.288.23.521.518.52l4.149-.01L8.152 7.11a.522.522 0 000 .737c.202.203.53.203.732 0l6.064-6.094-.01 4.26c-.001.288.23.52.516.52a.523.523 0 00.52-.522l.014-5.367a.525.525 0 00-.129-.47.513.513 0 00-.388-.175z"
        fill="currentColor"
      />
      <rect y={2} width={1} height={14} rx={0.5} fill="currentColor" />
      <rect y={15} width={14} height={1} rx={0.5} fill="currentColor" />
      <rect y={2} width={7} height={1} rx={0.5} fill="currentColor" />
      <rect x={13} y={9} width={1} height={7} rx={0.5} fill="currentColor" />
    </svg>
  );
}

export default SvgExternallink;
