import * as React from "react";

function SvgArchive(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15 2H1v2h14V2zM0 5h1v9a1 1 0 001 1h12a1 1 0 001-1V5h1V2a1 1 0 00-1-1H1a1 1 0 00-1 1v3zm2 9V5h12v9H2zm4.5-6a.5.5 0 000 1h3a.5.5 0 000-1h-3z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgArchive;
