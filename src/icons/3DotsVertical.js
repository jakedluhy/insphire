import * as React from "react";

function Svg3DotsVertical(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.5 3.5a1.5 1.5 0 100-3 1.5 1.5 0 000 3zM9 8a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zm0 6a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0z"
        fill="currentColor"
      />
    </svg>
  );
}

export default Svg3DotsVertical;
