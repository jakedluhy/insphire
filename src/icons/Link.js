import * as React from "react";

function SvgLink(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <g clipPath="url(#Link_svg__clip0)" fill="currentColor">
        <path d="M5.775 3.536a.533.533 0 00.754 0l1.13-1.13a4.262 4.262 0 116.027 6.027l-1.13 1.13a.533.533 0 00.754.754l1.13-1.13a5.327 5.327 0 10-7.534-7.534l-1.13 1.13a.533.533 0 000 .753zM3.515 5.796a.533.533 0 010 .754l-1.13 1.13a4.262 4.262 0 106.027 6.027l1.13-1.13a.533.533 0 01.754.754l-1.13 1.13a5.327 5.327 0 11-7.534-7.534l1.13-1.13a.533.533 0 01.753 0z" />
        <path d="M11.05 5.043a.533.533 0 00-.754 0l-5.274 5.274a.533.533 0 10.753.753l5.274-5.274a.533.533 0 000-.753z" />
      </g>
      <defs>
        <clipPath id="Link_svg__clip0">
          <path fill="#fff" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgLink;
