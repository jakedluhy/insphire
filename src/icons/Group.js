import * as React from "react";

function SvgGroup(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.75 2.5H14.5a1 1 0 011 1v5H3.75v-6z"
        fill="#0085FF"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16 12.286c0 .946-.754 1.714-1.684 1.714H1.684C.754 14 0 13.232 0 12.286V3.714C0 2.768.755 2 1.684 2h12.632C15.246 2 16 2.768 16 3.714v8.572zm-1-.204V3.918A.91.91 0 0014.098 3H4.375v10h9.723a.91.91 0 00.902-.918zM3.375 13V3H1.902A.911.911 0 001 3.918v8.164c0 .51.407.918.902.918h1.473z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgGroup;
