import * as React from "react";

function SvgTimeline(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" {...props}>
      <rect
        x={0.5}
        y={1.5}
        width={11.521}
        height={2.478}
        rx={1.239}
        stroke="currentColor"
      />
      <rect
        x={3.978}
        y={6.37}
        width={11.521}
        height={2.478}
        rx={1.239}
        stroke="currentColor"
      />
      <rect
        x={0.5}
        y={11.239}
        width={11.521}
        height={2.478}
        rx={1.239}
        stroke="currentColor"
      />
    </svg>
  );
}

export default SvgTimeline;
