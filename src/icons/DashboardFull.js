import * as React from "react";

function SvgDashboardFull(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 17 16" fill="none" {...props}>
      <rect width={16.071} height={16} rx={4} fill="currentColor" />
      <path
        d="M4 10.5a.5.5 0 011 0v2a.5.5 0 01-1 0v-2zM7.5 7.5a.5.5 0 011 0v5a.5.5 0 01-1 0v-5zM11 4.5a.5.5 0 011 0v8a.5.5 0 01-1 0v-8z"
        fill="#fff"
      />
    </svg>
  );
}

export default SvgDashboardFull;
