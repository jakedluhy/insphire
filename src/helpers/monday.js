import _ from 'lodash';

const extractJsonValue = (jsonString) => {
  if (!jsonString) return null;

  try {
    return JSON.parse(jsonString);
  } catch (e) {
    return jsonString;
  }
}

export function createColumnsMap(columnIdsToKeysMap, columnValues) {
  const mappedValues = _.map(columnValues, (column) => ({
    id: column.id,
    title: column.title,
    type: column.type,
    text: column.text,
    value: extractJsonValue(column.value),
    additionalInfo: extractJsonValue(column.additional_info),
  }));

  return _.keyBy(mappedValues, ({ id }) => columnIdsToKeysMap[id] || id);
}

export function extractOptionsFromSelectColumn(column) {
  try {
    const { labels, labels_positions_v2, labels_colors } = JSON.parse(column.settings_str);

    const types = _.map(labels, (label, id) => {
      return {
        label,
        id,
        color: labels_colors[id].color,
        border: labels_colors[id].border,
      };
    });

    return _.sortBy(types, ({ id }) => labels_positions_v2[id]);
  } catch(e) {
    // TODO: Log error
    return [];
  }
}
