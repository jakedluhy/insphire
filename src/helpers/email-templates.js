import _ from 'lodash';
import format from 'date-fns/format';

import {
  getDatetime,
  getPerson,
  getPersonId,
  getHiringPosition,
  getHiringPositionId,
} from './candidates';
import { useUsers } from '../contexts/users';
import { useMondayData } from '../contexts/monday-data';
import { useHiringPositions } from '../contexts/hiring-positions';

const TEMPLATE_OPTIONS = [
  'firstName',
  'lastName',
  'interviewContactDate',
  'upcomingInterviewer',
  'videoMeetingLink',
  'phoneNumber',
  'position',
  'currentUserName',
];

export function useTemplateOptionsToDataMap(candidate) {
  const { currentUser } = useMondayData();
  const { users } = useUsers();
  const { hiringPositions } = useHiringPositions();

  const contactDate = getDatetime(candidate);
  const interviewer = getPerson(candidate, 'upcomingInterviewer', users);
  const hiringPosition = getHiringPosition(candidate, hiringPositions);

  return {
    firstName: _.get(candidate, 'columns.firstName.text'),
    lastName: _.get(candidate, 'columns.lastName.text'),
    interviewContactDate: contactDate ? format(contactDate, 'LLLL do') : null,
    upcomingInterviewer: _.get(interviewer, 'name'),
    videoMeetingLink: _.get(candidate, 'columns.videoMeetingLink.text'),
    phoneNumber: _.get(candidate, 'columns.phone.text'),
    position: _.get(hiringPosition, 'title'),
    currentUserName: _.get(currentUser, 'name'),
  };
}

// eslint-disable-next-line
const SUGGESTION_REGEX = /.*\{\{([^\}]*)$/g;
export function getTemplateSuggestions(template) {
  const result = SUGGESTION_REGEX.exec(template);

  if (!result) return { isSuggesting: false, options: [] };

  const matchingStr = _.trim(result[1]);
  const templateOptions = _.isEmpty(matchingStr)
    ? TEMPLATE_OPTIONS
    : TEMPLATE_OPTIONS.filter(opt => opt.toLowerCase().startsWith(matchingStr.toLowerCase()));

  return {
    isSuggesting: true,
    options: templateOptions,
  };
}

export function transformTemplateToText(template, optionsToDataMap) {
  const errorKeysArray = [];

  const text = _.reduce(optionsToDataMap, (value, attrValue, attrKey) => {
    const findingRegex = new RegExp(`{{\\s*${attrKey}\\s*}}`, 'g');

    if (value.match(findingRegex) && !attrValue) {
      errorKeysArray.push(attrKey);
      return value;
    } else {
      return value.replace(findingRegex, attrValue);
    }
  }, template);

  return { text, errorKeysArray };
}

export function orderByNumDataPointsDesc(candidates) {
  return _.orderBy(
    candidates,
    candidate => {
      let dataPoints = 0;
      if (_.get(candidate, 'columns.firstName.text')) dataPoints++;
      if (_.get(candidate, 'columns.lastName.text')) dataPoints++;
      if (_.get(candidate, 'columns.interviewContactDate.text')) dataPoints++;
      if (getPersonId(candidate, 'upcomingInterviewer')) dataPoints++;
      if (_.get(candidate, 'columns.videoMeetingLink.text')) dataPoints++;
      if (_.get(candidate, 'columns.phone.text')) dataPoints++;
      if (getHiringPositionId(candidate)) dataPoints++;
      return dataPoints;
    },
    'desc'
  );
}
