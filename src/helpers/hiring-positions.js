import _ from 'lodash';

import { useHiringPositions } from '../contexts/hiring-positions';

export function useStages() {
  const { hiringPositions } = useHiringPositions();

  const stages = _.flatten(_.map(hiringPositions, hp => getOrderedStages(hp)))

  const orderedStages = _.sortBy(stages, s => {
    const orderIndexStr = _.get(s, 'columns.orderIndex.text');
    return orderIndexStr ? Number(orderIndexStr) : 100;
  });

  return { orderedStages: _.map(orderedStages, 'name') };
}

export function getStage(hiringPosition, candidate) {
  const stageName = _.get(candidate, 'columns.stage.text');
  return _.find(_.get(hiringPosition, 'columns.stages'), { name: stageName });
}

export function getOrderedStages(hiringPosition) {
  return _.sortBy(_.get(hiringPosition, 'columns.stages'), s => {
    const orderIndexStr = _.get(s, 'columns.orderIndex.text');
    return orderIndexStr ? Number(orderIndexStr) : 100;
  });
}
