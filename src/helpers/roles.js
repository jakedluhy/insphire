import _ from 'lodash';

export const ROLE_INTERVIEWER = 'INTERVIEWER';
export const ROLE_HIRING_MANAGER = 'HIRING MANAGER';
export const ROLE_RECRUITER = 'RECRUITER';
export const ROLE_RECRUITING_MANAGER = 'RECRUITING MANAGER';
export const roleToText = (role) => _.startCase(role.toLowerCase());
