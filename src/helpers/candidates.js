import _ from 'lodash';

export function filterCandidatesByUserId(candidates, columnKey, userId) {
  return _.filter(candidates, candidate => {
    const userIds = _.get(candidate, `columns.${columnKey}.value.personsAndTeams`, []).map(v => v.id);
    return userIds.includes(userId);
  });
}

export function getDatetime(candidate, key = 'interviewContactDate') {
  const dateText = _.get(candidate, `columns.${key}.text`);
  return dateText ? new Date(dateText) : null;
}

export function getPersonId(candidate, key) {
  const personId = _.get(candidate, `columns.${key}.value.personsAndTeams[0].id`);
  return personId ? _.toString(personId) : null;
}

export function getPerson(candidate, key, people) {
  const personId = getPersonId(candidate, key);
  return personId ? _.find(people, { id: Number(personId) }) : null;
}

export function getHiringPositionId(candidate) {
  const hiringPositionId = _.get(candidate, 'columns.position.value.linkedPulseIds[0].linkedPulseId');
  return hiringPositionId ? _.toString(hiringPositionId) : null;
}

export function getHiringPosition(candidate, hiringPositions) {
  const hiringPositionId = _.get(candidate, 'columns.position.value.linkedPulseIds[0].linkedPulseId');
  return hiringPositionId ? _.find(hiringPositions, { id: _.toString(hiringPositionId) }) : null;
}

export function extractDatesFromCandidates(candidates, dateKey) {
  const filteredCandidates = _.filter(candidates, c => _.get(c, `columns.${dateKey}.text`));
  const mappedCandidates = _.map(filteredCandidates, c => ({ ...c, date: new Date(_.get(c, `columns.${dateKey}.text`)) }));
  const maxDate = _.maxBy(mappedCandidates, c => +c.date).date;
  const minDate = _.minBy(mappedCandidates, c => +c.date).date;

  return { mappedCandidates, minDate, maxDate };
}

export function orderByDate(candidates, key = 'createdAt', order = 'desc') {
  return _.orderBy(candidates, c => new Date(c[key]), [order]);
}
