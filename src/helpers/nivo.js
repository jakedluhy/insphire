import _ from 'lodash';

export function getYTickValues(data, keys = ['value']) {
  let max = 0;
  _.forEach(data, d => {
    const total = _.reduce(keys, (t, key) => t + d[key], 0);
    if (total > max) max = total;
  });

  const step = Math.ceil(max / 10);

  return _.range(0, (Math.ceil(max / step) * step) + 1, step);
}
