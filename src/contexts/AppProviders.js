import React from 'react';

import { MondayDataProvider } from './monday-data';
import { CandidatesProvider } from './candidates';
import { HiringPositionsProvider } from './hiring-positions';
import { EmailTemplatesProvider } from './email-templates';
import { UsersProvider } from './users';

export default function AppProviders({ children }) {
  return (
    <MondayDataProvider>
      <UsersProvider>
        <CandidatesProvider>
          <HiringPositionsProvider>
            <EmailTemplatesProvider>
              {children}
            </EmailTemplatesProvider>
          </HiringPositionsProvider>
        </CandidatesProvider>
      </UsersProvider>
    </MondayDataProvider>
  );
}
