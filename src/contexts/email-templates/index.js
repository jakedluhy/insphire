import React, { useContext, useEffect, useState, useCallback } from 'react';

import { monday } from '../../monday';
import { useMondayData } from '../monday-data';
import useLocalStorage from '../../hooks/use-local-storage';
import { getAllEmailTemplates } from '../../monday/email-templates';

const EmailTemplatesContext = React.createContext();

export function EmailTemplatesProvider({ children }) {
  const { data: { emailTemplatesBoardId } } = useMondayData();

  const [emailTemplates, setEmailTemplates] = useLocalStorage('monday-email-templates', []);
  const [isLoading, setIsLoading] = useState(true);

  const fetchTemplates = useCallback(() => {
    if (!emailTemplatesBoardId) return;

    getAllEmailTemplates(emailTemplatesBoardId)
    .then(emailTemplates => {
      setEmailTemplates(emailTemplates);
      setIsLoading(false);
    })
    .catch(() => {
      monday.execute('notice', { message: 'Failed to fetch email templates', type: 'error' });
      setIsLoading(false);
    });
  }, [emailTemplatesBoardId, setEmailTemplates, setIsLoading]);

  useEffect(() => {
    fetchTemplates();
  }, [fetchTemplates]);

  return (
    <EmailTemplatesContext.Provider
      value={{
        emailTemplates,
        isLoading,
        refetch: fetchTemplates,
      }}
    >
      {children}
    </EmailTemplatesContext.Provider>
  );
}

export function useEmailTemplates() {
  return useContext(EmailTemplatesContext);
}
