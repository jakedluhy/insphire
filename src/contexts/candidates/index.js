import React, { useContext, useCallback, useEffect, useState } from 'react';
import _ from 'lodash';

import { useMondayData } from '../monday-data';
import {
  fetchCandidates,
  updateCandidateGroup,
  updateCandidate as updateCandidateRequest,
} from '../../monday/candidates';
import {
  CANDIDATE_IN_PROGRESS_GROUP_ID,
  CANDIDATE_PROSPECTS_GROUP_ID,
  CANDIDATE_TO_REVISIT_GROUP_ID,
  CANDIDATE_NOT_A_FIT_GROUP_ID,
} from '../../monday/constants';

const PROSPECT_GROUP_IDS = [CANDIDATE_PROSPECTS_GROUP_ID, CANDIDATE_TO_REVISIT_GROUP_ID];

const CandidatesContext = React.createContext();

const useCandidateFetching = (boardId, groupIds) => {
  const [candidates, setCandidates] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchCandidates(boardId, groupIds)
    .then(candidates => {
      setIsLoading(false);
      setCandidates(candidates);
    });
  }, [boardId, groupIds, setCandidates, setIsLoading]);

  return { candidates, isLoading, setCandidates };
}

function addOrUpdateCandidate(stateSetter, candidate) {
  stateSetter(oldValues => {
    const mappedCandidates = _.keyBy(oldValues, 'id');
    mappedCandidates[candidate.id] = candidate;
    return _.values(mappedCandidates);
  });
}

function removeCandidateFrom(stateSetter, candidate) {
  stateSetter(oldValues => {
    const mappedCandidates = _.keyBy(oldValues, 'id');
    delete mappedCandidates[candidate.id]
    return _.values(mappedCandidates);
  });
}

export function CandidatesProvider({ children }) {
  const { data } = useMondayData();
  const { candidatePipelineBoardId } = data;

  const {
    candidates: inProgressCandidates,
    isLoading: inProgressCandidatesLoading,
    setCandidates: inProgressSetCandidates,
  } = useCandidateFetching(candidatePipelineBoardId, CANDIDATE_IN_PROGRESS_GROUP_ID);

  const {
    candidates: prospectCandidates,
    isLoading: prospectCandidatesLoading,
    setCandidates: prospectSetCandidates,
  } = useCandidateFetching(candidatePipelineBoardId, PROSPECT_GROUP_IDS);

  const {
    candidates: notFitCandidates,
    isLoading: notFitCandidatesLoading,
    setCandidates: notFitSetCandidates,
  } = useCandidateFetching(candidatePipelineBoardId, CANDIDATE_NOT_A_FIT_GROUP_ID);

  const onAddOrUpdateCandidate = useCallback((candidate) => {
    const groupId = _.get(candidate, 'group.id');

    if (groupId === CANDIDATE_IN_PROGRESS_GROUP_ID) {
      addOrUpdateCandidate(inProgressSetCandidates, candidate);
      removeCandidateFrom(prospectSetCandidates, candidate);
      removeCandidateFrom(notFitSetCandidates, candidate);
    } else if (PROSPECT_GROUP_IDS.includes(groupId)) {
      addOrUpdateCandidate(prospectSetCandidates, candidate);
      removeCandidateFrom(inProgressSetCandidates, candidate);
      removeCandidateFrom(notFitSetCandidates, candidate);
    } else if (groupId === CANDIDATE_NOT_A_FIT_GROUP_ID) {
      addOrUpdateCandidate(notFitSetCandidates, candidate);
      removeCandidateFrom(inProgressSetCandidates, candidate);
      removeCandidateFrom(prospectSetCandidates, candidate);
    }
  }, [inProgressSetCandidates, prospectSetCandidates, notFitSetCandidates]);

  const updateCandidateStatus = useCallback((candidate, groupId) => {
    return updateCandidateGroup(candidate.id, groupId)
    .then(() => {
      const newCandidate = _.cloneDeep(candidate);
      newCandidate.group.id = groupId;
      onAddOrUpdateCandidate(newCandidate);
    });
  }, [onAddOrUpdateCandidate]);

  const updateCandidate = useCallback((candidate, columnValues) => {
    return updateCandidateRequest(candidatePipelineBoardId, candidate.id, columnValues)
    .then(newCandidate => {
      onAddOrUpdateCandidate(newCandidate);
    });
  }, [candidatePipelineBoardId, onAddOrUpdateCandidate]);

  return (
    <CandidatesContext.Provider
      value={{
        inProgressCandidates,
        inProgressCandidatesLoading,
        prospectCandidates,
        prospectCandidatesLoading,
        notFitCandidates,
        notFitCandidatesLoading,
        onAddOrUpdateCandidate,
        updateCandidateStatus,
        updateCandidate,
      }}
    >
      {children}
    </CandidatesContext.Provider>
  );
}

export function useCandidates() {
  return useContext(CandidatesContext);
}
