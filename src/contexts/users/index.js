import React, { useContext, useEffect, useState } from 'react';

import { monday } from '../../monday';
import useLocalStorage from '../../hooks/use-local-storage';
import { getAllUsers } from '../../monday/users';

const UsersContext = React.createContext();

export function UsersProvider({ children }) {
  const [users, setUsers] = useLocalStorage('monday-users', []);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getAllUsers()
    .then(users => {
      setUsers(users);
      setIsLoading(false);
    })
    .catch(() => {
      monday.execute('notice', { message: 'Failed to fetch users', type: 'error' });
      setIsLoading(false);
    });
  }, [setUsers, setIsLoading]);

  return (
    <UsersContext.Provider
      value={{
        users,
        isLoading,
      }}
    >
      {children}
    </UsersContext.Provider>
  );
}

export function useUsers() {
  return useContext(UsersContext);
}
