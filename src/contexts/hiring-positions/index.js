import React, { useContext, useEffect, useState, useCallback } from 'react';

import { monday } from '../../monday';
import { useMondayData } from '../monday-data';
import useLocalStorage from '../../hooks/use-local-storage';
import { getAllHiringPositions } from '../../monday/hiring-positions';

const HiringPositionsContext = React.createContext();

export function HiringPositionsProvider({ children }) {
  const { data: { hiringPositionsBoardId } } = useMondayData();

  const [hiringPositions, setHiringPositions] = useLocalStorage('monday-hiring-positions', []);
  const [isLoading, setIsLoading] = useState(true);

  const fetchPositions = useCallback(() => {
    if (!hiringPositionsBoardId) return;

    getAllHiringPositions(hiringPositionsBoardId)
    .then(hiringPositions => {
      setHiringPositions(hiringPositions);
      setIsLoading(false);
    })
    .catch((e) => {
      monday.execute('notice', { message: 'Failed to fetch hiring positions', type: 'error' });
      setIsLoading(false);
    });
  }, [hiringPositionsBoardId, setHiringPositions, setIsLoading]);

  useEffect(() => {
    fetchPositions();
  }, [fetchPositions]);

  return (
    <HiringPositionsContext.Provider
      value={{
        hiringPositions,
        isLoading,
        refetch: fetchPositions,
      }}
    >
      {children}
    </HiringPositionsContext.Provider>
  );
}

export function useHiringPositions() {
  return useContext(HiringPositionsContext);
}
