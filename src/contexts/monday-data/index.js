import React, { useContext, useCallback, useEffect, useState } from 'react';
import _ from 'lodash';

import { monday } from '../../monday';
import { storage } from '../../monday';
import { API_TOKEN_STORAGE_KEY } from '../../monday/constants';
import { getAndSetBoardIds } from '../../monday/boards';
import { getCurrentUser, setUserRole } from '../../monday/users';
import useLocalStorage from '../../hooks/use-local-storage';

import PageSpinner from '../../components/PageSpinner';

const MondayDataContext = React.createContext();

export function MondayDataProvider({ children }) {
  const [data, setData] = useLocalStorage('monday-data', {});
  const [currentUser, setCurrentUser] = useState({});
  const [isBoardIdsLoading, setIsBoardIdsLoading] = useState(true);
  const [isCurrentUserLoading, setIsCurrentUserLoading] = useState(true);
  const [isErrored, setIsErrored] = useState(false);

  useEffect(() => {
    getCurrentUser()
    .then(user => {
      setCurrentUser(user);
      setIsCurrentUserLoading(false);
    })
    .catch(() => {
      monday.execute('notice', { message: 'Failed to fetch current user', type: 'error' });
      setIsCurrentUserLoading(false);
      setIsErrored(true);
    });
  }, [setCurrentUser, setIsCurrentUserLoading, setIsErrored])

  useEffect(() => {
    if (data.candidatePipelineBoardId) {
      setIsBoardIdsLoading(false);
      return;
    }

    getAndSetBoardIds()
    .then(boardIds => {
      setData(oldData => ({ ...oldData, ...boardIds }));
      setIsBoardIdsLoading(false);
    })
    .catch(() => {
      monday.execute('notice', { message: 'Failed to fetch board information', type: 'error' });
      setIsBoardIdsLoading(false);
      setIsErrored(true);
    });
  }, [data, setIsBoardIdsLoading, setData, setIsErrored]);

  const chooseRole = useCallback(role => {
    setUserRole(currentUser.id, role);
    setCurrentUser(prevUser => ({ ...prevUser, role }));
  }, [currentUser, setCurrentUser]);


  const [apiToken, setApiToken] = useState(null);
  const [isApiTokenLoading, setIsApiTokenLoading] = useState(true);

  useEffect(() => {
    storage.getItem(API_TOKEN_STORAGE_KEY)
    .then(res => _.get(res, 'data.value'))
    .then(token => {
      if (token) setApiToken(token);
      setIsApiTokenLoading(false)
    })
    .catch(() => {
      monday.execute('notice', { message: 'Failed to retrieve API token', type: 'error' });
    });
  }, [setApiToken]);

  const isLoading = isBoardIdsLoading || isCurrentUserLoading || isApiTokenLoading;

  return (
    <MondayDataContext.Provider
      value={{
        currentUser,
        data,
        chooseRole,
        apiToken,
        setApiToken,
      }}
    >
      {isLoading ? (
        <PageSpinner />
      ) : isErrored ? (
        null // TODO render error if errored
      ) : data.candidatePipelineBoardId && currentUser.id ? (
        children
      ) : null}
    </MondayDataContext.Provider>
  );
}

export function useMondayData() {
  return useContext(MondayDataContext);
}
