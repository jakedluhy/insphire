import React from 'react';

import { useMondayData } from '../../contexts/monday-data';
import { useCandidates } from '../../contexts/candidates';
import { filterCandidatesByUserId } from '../../helpers/candidates';

import CandidatesToInterview from '../partials/CandidatesToInterview';
import CandidateActionItems from '../partials/CandidateActionItems';
import CandidatesGroupedByStage from '../partials/CandidatesGroupedByStage';
import CandidateFalloff from '../partials/CandidateFalloff';
import DashboardWrapper from '../partials/DashboardWrapper';

function RecruiterDashboard() {
  const { currentUser } = useMondayData();
  const { inProgressCandidates, prospectCandidates, notFitCandidates } = useCandidates();

  const myInterviewCandidates = filterCandidatesByUserId(
    inProgressCandidates,
    'upcomingInterviewer',
    currentUser.id
  );

  const mySourcedCandidates = filterCandidatesByUserId(
    inProgressCandidates,
    'recruiter',
    currentUser.id
  );

  const myProspectCandidates = filterCandidatesByUserId(
    prospectCandidates,
    'recruiter',
    currentUser.id,
  );

  const myNotFitCandidates = filterCandidatesByUserId(
    notFitCandidates,
    'recruiter',
    currentUser.id
  );

  return (
    <DashboardWrapper
      creationOptions={['candidate', 'template']}
    >
      <div className='px-lg pb-lg'>
        <CandidateActionItems
          className='pb-xl'
          candidates={mySourcedCandidates}
          prospectCandidates={myProspectCandidates}
        />

        <div className='flex pb-lg'>
          <CandidatesToInterview
            className='w-1/2 pr-md'
            candidates={myInterviewCandidates}
          />

          <CandidatesGroupedByStage
            className='w-1/2 pl-md'
            title='Your Sourced Candidates'
            candidates={mySourcedCandidates}
            candidateProps={{
              shouldShowInterviewer: true,
              shouldShowStage: false,
              shouldShowHiringManager: true,
            }}
          />
        </div>

        <CandidateFalloff
          candidates={myNotFitCandidates}
        />
      </div>
    </DashboardWrapper>
  );
}

export default RecruiterDashboard;
