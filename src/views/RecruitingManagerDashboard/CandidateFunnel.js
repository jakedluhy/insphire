import React from 'react';
import _ from 'lodash';

import { useStages } from '../../helpers/hiring-positions';
import { NIVO_COLOR_SCHEME } from '../../helpers/colors';
import { getYTickValues } from '../../helpers/nivo';

import { ResponsiveBar } from '@nivo/bar';
import Text from '../../components/Text';

function CandidateFunnel({
  className,
  candidates,
}) {
  const { orderedStages } = useStages();

  const groupedCandidates = _.groupBy(candidates, (c) => _.get(c, 'columns.stage.text'));
  const candidateCounts = _.mapValues(groupedCandidates, values => _.size(values));

  const data = _.map(orderedStages, (stage, index) => ({
    stageName: stage,
    value: candidateCounts[stage] || 0,
    color: NIVO_COLOR_SCHEME[index],
  }));

  return (
    <div className={className}>
      <div className='mb-md'>
        <Text className='block mb-xs' type='secondary-title'>Candidate Funnel</Text>
        <Text className='block'>This shows the total count of the current candidates in the pipeline at various stages.</Text>
      </div>

      <div className='pt-lg' style={{ height: 350 }}>
        <ResponsiveBar
          data={data}
          indexBy='stageName'
          margin={{ bottom: 105, left: 60, top: 10 }}
          padding={0.3}
          colors={({ data }) => data.color}
          enableGridY={false}
          isInteractive={false}
          enableLabel={false}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Current Stage',
            legendPosition: 'middle',
            legendOffset: 80,
            tickRotation: -30,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Number of Candidates',
            legendPosition: 'middle',
            legendOffset: -40,
            tickValues: getYTickValues(data),
          }}
        />
      </div>
    </div>
  );
}

export default CandidateFunnel;
