import React from 'react';
import _ from 'lodash';
import format from 'date-fns/format';
import eachDayOfInterval from 'date-fns/eachDayOfInterval';

import { extractDatesFromCandidates } from '../../helpers/candidates';
import { COLOR_MAP } from '../../helpers/colors';
import { getYTickValues } from '../../helpers/nivo';

import { ResponsiveBar } from '@nivo/bar';
import Text from '../../components/Text';

function CandidateInterviewsPerDay({
  className,
  candidates,
}) {
  const { mappedCandidates, maxDate } = extractDatesFromCandidates(candidates, 'interviewContactDate');

  const groupedCandidates = _.groupBy(mappedCandidates, c => c.date ? format(c.date, 'LLL do') : null);
  const candidateCounts = _.mapValues(groupedCandidates, values => _.size(values));

  const range = eachDayOfInterval({ start: new Date(), end: maxDate }).map(d => format(d, 'LLL do'));
  
  const data = _.map(range, dateStr => ({
    dateStr,
    value: candidateCounts[dateStr] || 0,
  }));

  return (
    <div className={className}>
      <div className='mb-md'>
        <Text className='block mb-xs' type='secondary-title'>Candidate Interviews Per Day</Text>
        <Text className='block'>This chart shows the number of interviews scheduled per day. In order to hire the best candidates, it's recommended to schedule interviews as soon as possible.</Text>
      </div>

      <div className='pt-lg' style={{ height: 320 }}>
        <ResponsiveBar
          data={data}
          indexBy='dateStr'
          margin={{ bottom: 65, left: 60, top: 10 }}
          padding={0.3}
          colors={COLOR_MAP['indigo']}
          enableGridY={false}
          isInteractive={false}
          enableLabel={false}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Date',
            legendPosition: 'middle',
            legendOffset: 45,
            tickRotation: -30,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Scheduled Interview Count',
            legendPosition: 'middle',
            legendOffset: -40,
            tickValues: getYTickValues(data),
          }}
        />
      </div>
    </div>
  );
}

export default CandidateInterviewsPerDay;
