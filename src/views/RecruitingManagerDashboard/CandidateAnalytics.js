import React, { useState } from 'react';
import _ from 'lodash';

import { useCandidates } from '../../contexts/candidates';
import { useHiringPositions } from '../../contexts/hiring-positions';
import { useUsers } from '../../contexts/users';
import { getPersonId, getHiringPositionId } from '../../helpers/candidates';

import Text from '../../components/Text';
import Select from '../../components/Select';
import CandidateFunnel from './CandidateFunnel';
import CandidateInterviewsPerDay from './CandidateInterviewsPerDay';
import LastContactedDayReport from './LastContactedDayReport';
import CandidateFalloff from '../partials/CandidateFalloff';
import UnderrepresentedCandidates from './UnderrepresentedCandidates';

function filterCandidatesByIds(candidates, recruiterFilterId, hiringManagerFilterId, positionFilterId) {
  return _.filter(candidates, candidate => {
    const recruiterIsMatch = recruiterFilterId === '' || getPersonId(candidate, 'recruiter') === recruiterFilterId;
    const hiringManagerIsMatch = hiringManagerFilterId === '' || getPersonId(candidate, 'hiringManager') === hiringManagerFilterId;
    const positionIsMatch = positionFilterId === '' || getHiringPositionId(candidate) === positionFilterId;

    return recruiterIsMatch && hiringManagerIsMatch && positionIsMatch;
  });
}

function CandidateAnalytics({
  className,
}) {
  const { inProgressCandidates, notFitCandidates } = useCandidates();
  const { users } = useUsers();
  const { hiringPositions } = useHiringPositions();

  const [recruiterFilterId, setRecruiterFilterId] = useState('');
  const [hiringManagerFilterId, setHiringManagerFilterId] = useState('');
  const [positionFilterId, setPositionFilterId] = useState('');

  const userOptions = _.map(users, u => ({ label: u.name, value: _.toString(u.id) }));
  const hiringPositionOptions = _.map(hiringPositions, hp => ({ label: hp.title, value: _.toString(hp.id) }));

  const filteredCandidates = filterCandidatesByIds(
    inProgressCandidates,
    recruiterFilterId,
    hiringManagerFilterId,
    positionFilterId
  );
  const filteredNotAFitCandidates = filterCandidatesByIds(
    notFitCandidates,
    recruiterFilterId,
    hiringManagerFilterId,
    positionFilterId
  );

  return (
    <div className={className}>
      <div className='mb-lg flex justify-between items-center'>
        <div className='mr-xl'>
          <Text type='title'>Candidate Analytics</Text>
        </div>

        <div className='flex'>
          <Select
            isSearchable
            className='mr-md'
            style={{ width: 200 }}
            selectedItemValue={recruiterFilterId}
            onChange={setRecruiterFilterId}
            items={[
              { label: 'All Recruiters', value: '' },
              ...userOptions,
            ]}
          />

          <Select
            isSearchable
            className='mr-md'
            style={{ width: 200 }}
            selectedItemValue={hiringManagerFilterId}
            onChange={setHiringManagerFilterId}
            items={[
              { label: 'All Hiring Managers', value: '' },
              ...userOptions,
            ]}
          />

          <Select
            isSearchable
            style={{ width: 200 }}
            selectedItemValue={positionFilterId}
            onChange={setPositionFilterId}
            items={[
              { label: 'All Positions', value: '' },
              ...hiringPositionOptions,
            ]}
          />
        </div>
      </div>

      {_.isEmpty(filteredCandidates) ? (
        <div className='p-xl flex justify-center items-center'>
          <div className='max-w-md'>
            No candidates in the pipeline match these filters. Try changing the filters to see your analytics.
          </div>
        </div>
      ) : _.isEmpty(filteredNotAFitCandidates) ? (
        <>
          <div className='mb-lg flex'>
            <CandidateFunnel className='w-1/2 pr-sm' candidates={filteredCandidates} />
            <LastContactedDayReport className='w-1/2 pl-sm' candidates={filteredCandidates} />
          </div>

          <div className='mb-lg'>
            <UnderrepresentedCandidates
              candidates={[...filteredCandidates, ...filteredNotAFitCandidates]}
            />
          </div>

          <div className='flex'>
            <CandidateInterviewsPerDay className='w-1/2 pr-sm' candidates={filteredCandidates} />
            <div className='w-1/2 pl-sm' />
          </div>
        </>
      ) : (
        <>
          <div className='mb-lg flex'>
            <CandidateFunnel className='w-1/2 pr-sm' candidates={filteredCandidates} />
            <LastContactedDayReport className='w-1/2 pl-sm' candidates={filteredCandidates} />
          </div>

          <div className='mb-lg'>
            <UnderrepresentedCandidates
              candidates={[...filteredCandidates, ...filteredNotAFitCandidates]}
            />
          </div>

          <div className='flex'>
            <CandidateInterviewsPerDay className='w-1/2 pr-sm' candidates={filteredCandidates} />
            <CandidateFalloff
              className='w-1/2 pl-sm'
              title='Candidate Falloff'
              titleType='secondary-title'
              candidates={filteredNotAFitCandidates}
              shouldShowPositionFilter={false}
            />
          </div>
        </>
      )}
    </div>
  );
}

export default CandidateAnalytics;
