import React from 'react';
import _ from 'lodash';
import differenceInDays from 'date-fns/differenceInDays';

import { extractDatesFromCandidates } from '../../helpers/candidates';
import { COLOR_MAP } from '../../helpers/colors';
import { getYTickValues } from '../../helpers/nivo';

import { ResponsiveBar } from '@nivo/bar';
import Text from '../../components/Text';

function LastContactedDayReport({
  className,
  candidates,
}) {
  const { mappedCandidates, minDate } = extractDatesFromCandidates(candidates, 'lastContactedDate');

  const maxDifference = differenceInDays(new Date(), minDate);

  const groupedCandidates = _.groupBy(mappedCandidates, c => c.date ? differenceInDays(new Date(), c.date) : null);
  const candidateCounts = _.mapValues(groupedCandidates, values => _.size(values));

  const range = _.range(1, maxDifference + 1);
  
  const data = _.map(range, dayDifference => ({
    dayDifference,
    value: candidateCounts[dayDifference] || 0,
  }));

  return (
    <div className={className}>
      <div className='mb-md'>
        <Text className='block mb-xs' type='secondary-title'>Last Contacted Days Ago</Text>
        <Text className='block'>To hire the best candidates, make sure you are communicating often and clearly. This graph tells you the last contacted time for your currently interviewing candidates.</Text>
      </div>

      <div className='pt-lg' style={{ height: 310 }}>
        <ResponsiveBar
          data={data}
          indexBy='dayDifference'
          margin={{ bottom: 55, left: 60, top: 10 }}
          padding={0.3}
          colors={COLOR_MAP['sunset-red']}
          enableGridY={false}
          isInteractive={false}
          enableLabel={false}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Days Since Last Contact',
            legendPosition: 'middle',
            legendOffset: 45,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Number of Candidates',
            legendPosition: 'middle',
            legendOffset: -40,
            tickValues: getYTickValues(data),
          }}
        />
      </div>
    </div>
  );
}

export default LastContactedDayReport;
