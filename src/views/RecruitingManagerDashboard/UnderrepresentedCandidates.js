import React, { useState } from 'react';
import _ from 'lodash';

import { COLOR_MAP } from '../../helpers/colors';
import { getYTickValues } from '../../helpers/nivo';
import { getHiringPosition, getPerson } from '../../helpers/candidates';
import { useHiringPositions } from '../../contexts/hiring-positions';
import { useUsers } from '../../contexts/users';

import { ResponsiveBar } from '@nivo/bar';
import Text from '../../components/Text';
import Select from '../../components/Select';

const GROUP_OPTIONS = [
  { label: 'Position', value: 'position' },
  { label: 'Recruiter', value: 'recruiter' },
];

function UnderrepresentedCandidates({
  className,
  candidates,
}) {
  const { hiringPositions } = useHiringPositions();
  const { users } = useUsers();

  const [groupBy, setGroupBy] = useState('position');

  const groupedCandidates = _.groupBy(candidates, c => {
    if (groupBy === 'position') {
      return _.get(getHiringPosition(c, hiringPositions), 'title');
    } else if (groupBy === 'recruiter') {
      return _.get(getPerson(c, 'recruiter', users), 'name');
    }
  });
  const groupedCandidatesWithUnderrepresentedData = _.mapValues(groupedCandidates, (candidateGroup) => {
    const underrepGroups = _.groupBy(candidateGroup, c => _.get(c, 'columns.fromUnderrepresentedGroup.text') || 'Unknown')
    return _.mapValues(underrepGroups, values => _.size(values));
  })

  const data = _.map(groupedCandidatesWithUnderrepresentedData, (group, groupedName) => ({
    groupedName,
    Yes: group['Yes'] || 0,
    YesColor: COLOR_MAP['light-blue'],
    No: group['No'] || 0,
    NoColor: COLOR_MAP['navy'],
    Unknown: group['Unknown'] || 0,
    UnknownColor: COLOR_MAP['blackish'],
  }));

  const filteredData = _.reject(data, d => d.groupedName === 'undefined' || d.groupedName === 'null');

  return (
    <div className={className}>
      <div className='mb-md'>
        <div className='mb-md flex justify-between'>
          <Text className='block mb-xs' type='secondary-title'>Underrepresented Candidates</Text>

          <Select
            style={{ width: 200 }}
            items={GROUP_OPTIONS}
            selectedItemValue={groupBy}
            onChange={setGroupBy}
          />
        </div>

        <Text className='block'>
          The first step towards a more diverse and inclusive workplace is making sure that underrepresented candidates even get a chance to interview.
          This graph will show you how well you're doing at sourcing underrepresented candidates. Select a 
        </Text>
      </div>

      <div className='pt-lg' style={{ height: 350 }}>
        <ResponsiveBar
          data={filteredData}
          indexBy='groupedName'
          keys={['Unknown', 'No', 'Yes']}
          margin={{ bottom: 105, left: 60, top: 10, right: 130 }}
          padding={0.3}
          colors={({ id, data }) => data[`${id}Color`]}
          enableGridY={false}
          isInteractive={false}
          enableLabel={false}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Role',
            legendPosition: 'middle',
            legendOffset: 80,
            tickRotation: -20,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Scheduled Interview Count',
            legendPosition: 'middle',
            legendOffset: -40,
            tickValues: getYTickValues(data, ['Yes', 'No', 'Unknown']),
          }}
          legends={[{
            dataFrom: 'keys',
            anchor: 'bottom-right',
            direction: 'column',
            justify: false,
            translateX: 120,
            translateY: 0,
            itemsSpacing: 2,
            itemWidth: 100,
            itemHeight: 20,
            itemDirection: 'left-to-right',
            itemOpacity: 0.85,
            symbolSize: 20,
          }]}
        />
      </div>
    </div>
  );
}

export default UnderrepresentedCandidates;
