import React from 'react';

import RoleSelect from '../partials/RoleSelect';
import Text from '../../components/Text';

function RoleSetup() {
  return (
    <div className='p-xl flex flex-col items-center'>
      <Text type='title' className='mb-md'>Welcome to Joi Recruiting</Text>

      <Text type='paragraph' className='mb-xl text-center'>
        Before you get started, you will need to select your role within your company.
        We tailor your dashboard and experience based on your role and what is important to you.
      </Text>

      <RoleSelect style={{ width: 200 }} />
    </div>
  );
}

export default RoleSetup;
