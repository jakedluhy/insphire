import React from 'react';

import { useMondayData } from '../../contexts/monday-data';
import { useCandidates } from '../../contexts/candidates';
import { filterCandidatesByUserId } from '../../helpers/candidates';

import CandidatesToInterview from '../partials/CandidatesToInterview';
import CandidatesGroupedByStage from '../partials/CandidatesGroupedByStage';
import DashboardWrapper from '../partials/DashboardWrapper';

function HiringManagerDashboard() {
  const { currentUser } = useMondayData();
  const { inProgressCandidates } = useCandidates();

  const myInterviewCandidates = filterCandidatesByUserId(
    inProgressCandidates,
    'upcomingInterviewer',
    currentUser.id
  );

  const myPositionCandidates = filterCandidatesByUserId(
    inProgressCandidates,
    'hiringManager',
    currentUser.id
  );

  return (
    <DashboardWrapper
      creationOptions={['candidate', 'position', 'template']}
    >
      <div className='px-lg pb-lg'>
        <div className='flex'>
          <CandidatesToInterview
            className='w-1/2 pr-md'
            candidates={myInterviewCandidates}
          />

          <CandidatesGroupedByStage
            className='w-1/2 pl-md'
            candidates={myPositionCandidates}
            candidateProps={{ shouldShowInterviewer: true, shouldShowStage: false }}
          />
        </div>
      </div>
    </DashboardWrapper>
  );
}

export default HiringManagerDashboard;
