import React from 'react';

import { useMondayData } from '../../contexts/monday-data';
import { useCandidates } from '../../contexts/candidates';
import { filterCandidatesByUserId } from '../../helpers/candidates';

import CandidatesToInterview from '../partials/CandidatesToInterview';
import DashboardWrapper from '../partials/DashboardWrapper';
// import CreatePosition from '../partials/CreatePosition';
// import CreateCandidate from '../partials/CreateCandidate';
// import EmailTemplateForm from '../partials/EmailTemplateForm';

function InterviewerDashboard() {
  const { currentUser } = useMondayData();
  const { inProgressCandidates } = useCandidates();

  const myInterviewCandidates = filterCandidatesByUserId(
    inProgressCandidates,
    'upcomingInterviewer',
    currentUser.id
  );

  return (
    <DashboardWrapper>
      <div className='px-lg'>
        <CandidatesToInterview
          candidates={myInterviewCandidates}
        />
      </div>
    </DashboardWrapper>
  );
}

export default InterviewerDashboard;
