import React from 'react';

import { useMondayData } from '../../contexts/monday-data';
import {
  ROLE_INTERVIEWER,
  ROLE_HIRING_MANAGER,
  ROLE_RECRUITER,
  ROLE_RECRUITING_MANAGER,
} from '../../helpers/roles';

import InterviewerDashboard from '../InterviewerDashboard';
import HiringManagerDashboard from '../HiringManagerDashboard';
import RecruiterDashboard from '../RecruiterDashboard';
import RecruitingManagerDashboard from '../RecruitingManagerDashboard';
import RoleSetup from '../RoleSetup';

function Dashboard() {
  const { currentUser } = useMondayData();

  if (!currentUser) {
    // Log error
    return null;
  }

  switch(currentUser.role) {
    case ROLE_INTERVIEWER:
      return <InterviewerDashboard />;
    case ROLE_HIRING_MANAGER:
      return <HiringManagerDashboard />;
    case ROLE_RECRUITER:
      return <RecruiterDashboard />;
    case ROLE_RECRUITING_MANAGER:
      return <RecruitingManagerDashboard />;
    default:
      return <RoleSetup />;
  }
}

export default Dashboard;
