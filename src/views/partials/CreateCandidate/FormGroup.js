import React from 'react';
import _ from 'lodash';
import cx from 'classnames';

function FormGroup({ className, label, elementsList }) {
  return (
    <div className={className}>
      <div className='text-sm font-semibold'>{label}</div>

      <div className='border-2 rounded-md p-md'>
        {_.chunk(_.compact(elementsList), 2).map(([element1, element2], index) => (
          <div key={index} className={cx('flex', { 'mt-md': index !== 0 })}>
            <div className='w-1/2 pr-sm'>{element1}</div>
            <div className='w-1/2 pl-sm'>{element2}</div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default FormGroup;
