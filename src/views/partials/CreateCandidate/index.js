import React from 'react';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import _ from 'lodash';
import format from 'date-fns/format';
import startOfDay from 'date-fns/startOfDay';

import { monday } from '../../../monday';
import { getPersonId, getHiringPositionId, orderByDate } from '../../../helpers/candidates';
import { getUTCDate } from '../../../helpers/dates';
import {
  ROLE_HIRING_MANAGER,
  ROLE_RECRUITER,
} from '../../../helpers/roles';
import { useMondayData } from '../../../contexts/monday-data';
import { useCandidates } from '../../../contexts/candidates';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { useUsers } from '../../../contexts/users';
import {
  STATUS_ITEMS,
  CANDIDATE_IN_PROGRESS_GROUP_ID,
  CANDIDATE_CLOSED_GROUP_ID,
  CANDIDATE_KEYS_TO_COLUMN_IDS,
} from '../../../monday/constants';
import { useCandidateColumnOptions, createCandidate } from '../../../monday/candidates';

import { Formik, Form } from 'formik';
import Button from '../../../components/Button';
import FormLabel from '../../../components/FormLabel';
import Modal from '../../../components/Modal';
import Select from '../../../components/Select';
import Text from '../../../components/Text';
import Tooltip from '../../../components/Tooltip';
import FormikDateTimePicker from '../../../components/formik/FormikDateTimePicker';
import FormikFileUpload from '../../../components/formik/FormikFileUpload';
import FormikTextField from '../../../components/formik/FormikTextField';
import FormikSelect from '../../../components/formik/FormikSelect';
import FormikSubmit from '../../../components/formik/FormikSubmit';
import FormGroup from './FormGroup';

const CREATE_STATUS_ITEMS = _.without(STATUS_ITEMS, { value: CANDIDATE_CLOSED_GROUP_ID });

function createCandidateRequest(candidatePipelineBoardId, values, apiToken) {
  return createCandidate(
    candidatePipelineBoardId,
    values.status,
    `${values.firstName} ${values.lastName}`,
    {
      [CANDIDATE_KEYS_TO_COLUMN_IDS.position]: values.position ? { item_ids: [Number(values.position)] } : null,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.stage]:  values.stage,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.interviewContactDate]: (
        values.interviewContactDate
          ? format(
            getUTCDate(values.interviewContactDate),
            'yyyy-MM-dd HH:mm'
          )
          : null
      ),
      [CANDIDATE_KEYS_TO_COLUMN_IDS.lastContactedDate]: (
        values.lastContactedDate
          ? format(values.lastContactedDate, 'yyyy-MM-dd')
          : null
      ),
      [CANDIDATE_KEYS_TO_COLUMN_IDS.resume]: values.resume,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.linkedin]: values.linkedinProfile,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.recruiter]: values.recruiter,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.hiringManager]: values.hiringManager,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.upcomingInterviewer]: values.upcomingInterviewer,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.phone]: values.phoneNumber,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.fromUnderrepresentedGroup]: values.fromUnderrepresentedGroup,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.source]: values.source,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.email]: values.email,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.expectedComp]: values.expectedCompensation,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.firstName]: values.firstName,
      [CANDIDATE_KEYS_TO_COLUMN_IDS.lastName]: values.lastName,
    },
    apiToken
  );
}

function CreateCandidate({
  isOpen,
  onRequestClose,
  onSuccess,
}) {
  const { currentUser, data: { candidatePipelineBoardId }, apiToken } = useMondayData();
  const { users } = useUsers();
  const { hiringPositions } = useHiringPositions();
  const { onAddOrUpdateCandidate, inProgressCandidates, prospectCandidates, notFitCandidates } = useCandidates();
  const { candidateColumnOptions } = useCandidateColumnOptions();
  const { underrepresentedGroupOptions = [], sourceOptions = [] } = candidateColumnOptions;

  const usersOptions = _.map(users, (u) => ({ label: u.name, value: _.toString(u.id) }));
  const hiringPositionOptions = _.map(hiringPositions, (hp) => ({ label: hp.title, value: _.toString(hp.id) }));
  const mappedUnderrepresentedGroupOptions = underrepresentedGroupOptions.map(o => ({ label: o.label, value: o.id }));
  const mappedSourceOptions = sourceOptions.map(o => ({ label: o.label, value: o.id }));

  const defaultValues = {};
  if (currentUser.role === ROLE_RECRUITER) {
    defaultValues.recruiter = _.toString(currentUser.id);
  } else if (currentUser.role === ROLE_HIRING_MANAGER) {
    defaultValues.hiringManager = _.toString(currentUser.id);
  }

  const orderedCandidates = orderByDate([...inProgressCandidates, ...prospectCandidates, ...notFitCandidates]);
  const candidateOptions = orderedCandidates.map(c => ({ label: c.name, value: c.id }));

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='min-w-xl'
    >
      <Text type='title' className='block mb-md'>Add a Candidate</Text>

      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          email: '',
          phoneNumber: '',
          resume: null,
          linkedinProfile: '',
          status: CANDIDATE_IN_PROGRESS_GROUP_ID,
          position: '',
          stage: '',
          interviewContactDate: null,
          lastContactedDate: startOfDay(new Date()),
          recruiter: '',
          upcomingInterviewer: '',
          hiringManager: '',
          fromUnderrepresentedGroup: '',
          source: '',
          expectedCompensation: '',
          ...defaultValues,
        }}
        onSubmit={(values, { setSubmitting }) => {
          createCandidateRequest(candidatePipelineBoardId, values, apiToken)
          .then(candidate => {
            setSubmitting(false);
            onAddOrUpdateCandidate(candidate);
            onSuccess(candidate);
          })
          .catch((e) => {
            monday.execute('notice', { message: 'Failed to create candidate', type: 'error' });
            setSubmitting(false);
          })
        }}
        validationSchema={yup.object().shape({
          firstName: yup.string().required('is required'),
          lastName: yup.string().required('is required'),
          email: yup.string().required('is required'),
          phoneNumber: yup.string(),
          resume: yup.mixed().nullable(),
          linkedinProfile: yup.string(),
          status: yup.string(),
          position: yup.string(),
          stage: yup.string(),
          interviewContactDate: yup.string().nullable(),
          upcomingInterviewer: yup.string(),
          recruiter: yup.string(),
          hiringManager: yup.string(),
          fromUnderrepresentedGroup: yup.string(),
          source: yup.string(),
          expectedCompensation: yup.string(),
        })}
      >
        {({ values, setFieldValue, setValues, status, setStatus }) => {
          const selectedHiringPosition = _.find(hiringPositions, { id: values.position });
          const stageOptions = _.map(_.get(selectedHiringPosition, 'columns.stages'), stage => ({
            label: stage.name,
            value: stage.name,
          }));

          const selectedStage = _.find(stageOptions, { value: values.stage });

          return (
            <>
              <FormLabel
                className='mb-xl'
                label='Candidate Template'
                helperText='Speed up candidate entry by using a recently created candidate to fill in some information'
              >
                <Select
                  isSearchable
                  items={candidateOptions}
                  selectedItemValue={_.get(status, 'candidateTemplateId')}
                  onChange={(candidateTemplateId) => {
                    const candidateTemplate = _.find(orderedCandidates, { id: candidateTemplateId });
                    setStatus({ candidateTemplateId });

                    const newValues = {
                      status: _.get(candidateTemplate, 'group.id', ''),
                      position: getHiringPositionId(candidateTemplate),
                      stage: _.get(candidateTemplate, 'columns.stage.text', ''),
                      upcomingInterviewer: getPersonId(candidateTemplate, 'upcomingInterviewer'),
                      recruiter: getPersonId(candidateTemplate, 'recruiter'),
                      hiringManager: getPersonId(candidateTemplate, 'hiringManager'),
                      source: _.get(candidateTemplate, 'columns.source.value.index', '').toString(),
                    };

                    setValues({ ...values, ..._.omitBy(newValues, v => !v) });
                  }}
                />
              </FormLabel>

              <Form>
                <FormGroup
                  className='mb-xl'
                  label='Contact / Credentials Information'
                  elementsList={[
                    <FormikTextField name='firstName' isRequired />,
                    <FormikTextField name='lastName' isRequired />,
                    <FormikTextField name='email' type='email' isRequired />,
                    <FormikTextField name='phoneNumber' placeholder='123-456-7890' />,
                    <FormikTextField name='videoMeetingLink' placeholder='https://meet.google.com/ykw-xmji-bzb' />,
                    <Tooltip
                      title='You need to add an API token to upload files. You can set it using the settings menu in the top-right of the main dashboard'
                      size='small'
                      disabled={!!apiToken}
                    >
                      <FormikFileUpload name='resume' disabled={!apiToken} />
                    </Tooltip>,
                    <FormikTextField name='linkedinProfile' placeholder='https://linkedin.com/john-doe' />,
                  ]}
                />

                <FormGroup
                  className='mb-xl'
                  label='Position Information'
                  elementsList={[
                    <FormikSelect name='status' items={CREATE_STATUS_ITEMS} />,
                    <FormikSelect
                      isSearchable
                      name='position'
                      items={hiringPositionOptions}
                      onChange={() => setFieldValue('stage', '')}
                    />,
                    selectedHiringPosition && !_.isEmpty(stageOptions) && (
                      <FormikSelect name='stage' items={stageOptions} />
                    ),
                    selectedStage && (
                      <FormikDateTimePicker
                        name='interviewContactDate'
                        label={`Date/Time of ${selectedStage.label}`}
                      />
                    ),
                  ]}
                />

                <FormGroup
                  className='mb-xl'
                  label='Hiring Information'
                  elementsList={[
                    <FormikSelect isSearchable name='upcomingInterviewer' items={usersOptions} />,
                    <FormikSelect isSearchable name='recruiter' items={usersOptions} />,
                    <FormikSelect isSearchable name='hiringManager' items={usersOptions} />,
                  ]}
                />

                <FormGroup
                  className='mb-xl'
                  label='Tracking Metrics'
                  elementsList={[
                    <FormikSelect name='fromUnderrepresentedGroup' items={mappedUnderrepresentedGroupOptions} />,
                    <FormikSelect name='source' items={mappedSourceOptions} />,
                    <FormikTextField name='expectedCompensation' placeholder='120k/year' />,
                    <FormikDateTimePicker name='lastContactedDate' showTimeInput={false} />,
                  ]}
                />

                <div className='mt-lg flex justify-end items-center'>
                  <Button
                    className='mr-sm'
                    label='Cancel'
                    type='button'
                    variant='tertiary'
                    onClick={() => onRequestClose()}
                  />

                  <FormikSubmit variant='primary' color='primary' label='Add Candidate' />
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </Modal>
  );
}

CreateCandidate.propTypes = {
  isOpen: PropTypes.bool,
};

export default CreateCandidate;
