import React from 'react';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import _ from 'lodash';

import { monday } from '../../../monday';
import { useMondayData } from '../../../contexts/monday-data';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import {
  HIRING_POSITIONS_DEFAULT_GROUP_ID,
  HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS,
} from '../../../monday/constants';
import { createItem, createSubitem } from '../../../monday/items';

import { Formik } from 'formik';
import Text from '../../../components/Text';
import Modal from '../../../components/Modal';
import FormLabel from '../../../components/FormLabel';
import Select from '../../../components/Select';
import PositionForm from './PositionForm';

async function createPositionRequest(hiringPositionsBoardId, values) {
  const itemResponse = await createItem(hiringPositionsBoardId, HIRING_POSITIONS_DEFAULT_GROUP_ID, values.jobTitle);
  const itemId = itemResponse.id;

  const stagesPromises = _.map(values.stagesJson, (stage, index) => {
    return createSubitem(
      itemId,
      stage.name,
      {
        [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.description]: stage.description,
        [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.duration]: stage.duration,
        [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.orderIndex]: index
      }
    );
  });

  return await Promise.all(stagesPromises);
}

function CreatePosition({
  isOpen,
  onRequestClose,
  onSuccess,
}) {
  const { data: { hiringPositionsBoardId } } = useMondayData();
  const { hiringPositions, refetch } = useHiringPositions();

  const hiringPositionOptions = _.map(hiringPositions, (hp) => ({ label: hp.title, value: _.toString(hp.id) }));

  return (
    <Modal
      className='min-w-xl'
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md'>Create a Hiring Position</Text>

      <Formik
        initialValues={{ jobTitle: '', stagesJson: [], }}
        onSubmit={(values, { setSubmitting }) => {
          createPositionRequest(hiringPositionsBoardId, values)
          .then(() => {
            setSubmitting(false);
            monday.execute('notice', { message: `Successfully created new position ${values.jobTitle}`, type: 'success' });
            onSuccess();
            refetch();
          })
          .catch((e) => {
            monday.execute('notice', { message: 'Failed to create new position', type: 'error' });
            setSubmitting(false);
          })
        }}
        validationSchema={yup.object().shape({
          jobTitle: yup.string().required('is required'),
          stagesJson: yup.array().of(
            yup.object().shape({
              name: yup.string(),
              duration: yup.string(),
              description: yup.string(),
            })
          ),
        })}
      >
        {({ values, errors, setValues, status, setStatus }) => (
          <>
            <FormLabel
              className='mb-xl'
              label='Hiring Position Template'
              helperText='Create a new position based off of an existing one'
            >
              <Select
                isSearchable
                items={hiringPositionOptions}
                selectedItemValue={_.get(status, 'hiringPositionTemplateId', '')}
                onChange={(hiringPositionTemplateId) => {
                  const hiringPositionTemplate = _.find(hiringPositions, { id: hiringPositionTemplateId });
                  setStatus({ hiringPositionTemplateId });

                  const templateStagesJson = _.map(_.get(hiringPositionTemplate, 'columns.stages'), stage => ({
                    name: stage.name || '',
                    duration: _.get(stage, 'columns.duration.text', ''),
                    description: _.get(stage, 'columns.description.text', ''),
                  }));

                  setValues({
                    stagesJson: templateStagesJson,
                  });
                }}
              />
            </FormLabel>

            <PositionForm values={values} onRequestClose={onRequestClose} />
          </>
        )}
      </Formik>
    </Modal>
  );
}

CreatePosition.propTypes = {
  isOpen: PropTypes.bool,
};

export default CreatePosition;
