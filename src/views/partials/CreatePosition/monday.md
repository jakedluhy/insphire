## Inspiration
I'm an Engineering Manager for a smallish startup in Los Angeles. A few months ago I was responsible for the entire hiring process while we were bringing on two more senior engineers. These were very important hires as they form the base of one of our product teams.

The problem was that the hiring process that I inherited was a complete mess! We were working with an external recruiter, and soon after I started we brought on another one to help source candidates. On top of that we used Vettery and Hired to find and source our own candidates. Everything was tracked in either a google sheet, email, or my memory. I had 6-8 interviews a day and I barely had time to send out all of the emails I needed to (interview prep, interview follow up, initial message, etc...). I was copying and pasting a lot of emails and every time I sent one out I'd stress out that I hadn't changed a name or date.

I knew that there were "Applicant Tracking Systems" out there (Greenhouse, Lever, etc...) that could help me, but from my research they were all very enterprise focused. As a scrappy startup we weren't really ready to shell out the big bucks, especially if we were only focusing on two hires at the time. I was already thinking about building my own "small scale" recruitment app when I heard about the Monday Apps Challenge - it seemed like a perfect fit!

## What it does
InspHire is was built to help startups and small business effectively track and manage their recruitment process. Hiring is one of the most important things you can do while building a business, and the best way to source top talent is to be communicative and prepared. Sending out interview prep and followup emails in a timely manner makes a **huge** difference in impressing a candidate. To that end, InspHire has the following features:

* **Action Items:** Alert the recruiter to various states (last contacted time, interview date past time and needs follow up)
* **Upcoming Interviews:** By assigning an interviewer, that interviewer can receive alerts and reminders about upcoming interviews, and easily add the appointment to their calendar.
* **Email Templates:** Create email templates that can be reused across different candidates and positions. Add dynamic candidate attributes in order to automatically insert names, dates, and positions to generate the right email text.
* **Track Discussion:** Easily centralize and track discussion about the candidate in one location (item updates). The monday updates functionality is robust and allows rich text formatting and document uploading which is perfect.
* **Track Candidate Stage:** Get an overview of the candidate pipeline, which stage they are at, and who should be interviewing them next. This also is a centralized, standardized place where any interviewer can go and see a description of how they should be conducting the interview.
* **Candidate Analytics:** As a recruiting manager, see where candidates are falling out of the pipeline, whether you have enough interviews upcoming in the pipeline, and whether you are sourcing enough candidates from underrepresented groups. Use these to gain insights with your hiring process and make positive tweaks.

## How I built it
React is my favorite framework to build with, so I was ecstatic to find out that I could easily build my app using the Monday SDK and React. I'm also very familiar with GraphQL, so it was really not an issue at all to get up and running with querying the data.

I'm a very visual person, so I started by taking a look at the Monday design guide and implementing that in my tailwind config file. It allowed me to easily set all the colors, spacing, and styles required to make my app look like a Monday app.

From there I spent a lot of time exploring the GraphQL API and figuring out the best way to pull data into my app. I wanted to have 3 connected boards to represent the different resources (candidates, hiring positions, and email templates) while allowing "Standard" tier companies to use the workspace.

It took a little time to figure out exactly how I wanted it to work, but ultimately I decided on using a Shared Workspace App with a single dashboard that represents the app itself.

From there it was just a matter continuing to build out all of the functionality that I had planned. I kept an ongoing list of things I wanted to add, and oftentimes I'd be continuously building out my list while I was working on the app itself!

## Challenges I ran into
I ran into a few challenges while building the app. The biggest one is that it was just way more functionality than I originally thought it would be! I kept having ideas that I wanted to build into the app, which just increased everything that I had to do. I really had to push myself at the end to get everything finished in time, but I'm super happy with the result.

From a more technical perspective, I ran into a frustrating issue where the Monday SDK doesn't support file uploads at the moment. Being able to upload resumes is very important for a recruitment app, so I had to build in a workaround where the user can insert their own API Key.

## Accomplishments that I'm proud of
I'm super proud of the whole app. While there's still a lot that I'd like to continue adding to it, I think it does a great job of serving as a simple hiring tool that can align teams and save people time. One part that I'm particularly happy with is the email templating functionality. This was a huge pain point for me before, and I've created a solution that does a great job in speeding up the process of sending out emails.

## What I learned
To be honest, I think the brunt of my learnings are yet to come. This app was built as a result of my experience with hiring in a small scale startup. This is the app that I wish I had when I was trying to hire. I'm excited to get it out in front of people that can give me feedback about what **they** are looking for. I see Monday as a great way to get started and build an initial app where I can gather these learnings, since I didn't have to worry about building out a backend server.

## What's next for InspHire
As I mentioned above, I'm excited to release the App and see what people think. I want to keep adding to it and improving it to make it dead simple to install and use.
