import React from 'react';

import { Form } from 'formik';
import Button from '../../../components/Button';
import FormikTextField from '../../../components/formik/FormikTextField';
import FormikSubmit from '../../../components/formik/FormikSubmit';
import FormikPositionStages from './FormikPositionStages';

function PositionForm({ values, onRequestClose, submissionText = 'Create Position' }) {
  return (
    <Form>
      <FormikTextField className='mb-lg' name='jobTitle' />

      <FormikPositionStages stagesJson={values.stagesJson} />

      <div className='mt-lg flex justify-end items-center'>
        <Button
          className='mr-sm'
          label='Cancel'
          type='button'
          variant='tertiary'
          onClick={() => onRequestClose()}
        />

        <FormikSubmit variant='primary' color='primary' label={submissionText} />
      </div>
    </Form>
  );
}

export default PositionForm;
