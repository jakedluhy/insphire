import React from 'react';
import _ from 'lodash';

import { FieldArray } from 'formik';
import FormikTextField from '../../../components/formik/FormikTextField';
import Button from '../../../components/Button';
import Text from '../../../components/Text';
import Plus from '../../../icons/Plus';
import X from '../../../icons/X';

function FormikPositionStages({ stagesJson }) {
  return (
    <>
      <Text type='paragraph'>Interview Stages</Text>
      <div className='text-sm text-helper-grey leading-tight mb-sm'>
        Add the interview stages that will be used for this position. In the description, note what the interviewer should be looking for.
      </div>

      <FieldArray name='stagesJson'>
        {({ remove, push }) => (
          <>
            {_.map(stagesJson, (stage, index) => {
              return (
                <div
                  key={index}
                  className='relative py-md pr-md pl-lg bg-riverstone-grey'
                  style={{ marginBottom: 1 }}
                >
                  <div
                    className='bg-wolf-grey'
                    style={{ width: 8, position: 'absolute', top: 0, left: 0, bottom: 0 }}
                  />

                  <div className='mb-sm flex'>
                    <FormikTextField
                      className='w-1/2 pr-md'
                      name={`stagesJson.${index}.name`}
                      label='Stage Name'
                    />

                    <FormikTextField
                      type='number'
                      step='0.1'
                      onWheel={e => e.preventDefault()}
                      className='w-1/2 pl-md'
                      name={`stagesJson.${index}.duration`}
                      label='Stage Duration (Hours)'
                    />
                  </div>

                  <FormikTextField
                    type='textarea'
                    name={`stagesJson.${index}.description`}
                    label='Stage Description'
                    rows={4}
                  />

                  <Button
                    type='button'
                    size='small'
                    variant='tertiary'
                    color='plain'
                    className='py-2'
                    style={{ position: 'absolute', top: 8, right: 8 }}
                    onClick={() => remove(index)}
                  >
                    <X height={8} width={8} />
                  </Button>
                </div>
              );
            })}

            <div className='pt-md'>
              <Button
                type='button'
                className='flex items-center'
                size='small'
                onClick={() => push({ name: '', description: '' })}
              >
                <Plus size={12} className='mr-sm' />
                Add Stage
              </Button>
            </div>
          </>
        )}
      </FieldArray>
    </>
  );
}

export default FormikPositionStages;
