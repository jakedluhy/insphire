import React from 'react';
import _ from 'lodash';
import isBefore from 'date-fns/isBefore';
import subMonths from 'date-fns/subMonths';

import { getDatetime, getPerson, getHiringPositionId } from '../../../helpers/candidates';
import { useUsers } from '../../../contexts/users';

import ActionItemCard from '../ActionItemCard';
import Text from '../../../components/Text';

function CandidateActionItems({
  className,
  title = 'Your Action Items',
  candidates,
  prospectCandidates,
}) {
  const { users } = useUsers();
  const actionItems = [];

  _.forEach(candidates, candidate => {
    const interviewer = getPerson(candidate, 'upcomingInterviewer', users);
    const recruiter = getPerson(candidate, 'recruiter', users);
    const interviewDate = getDatetime(candidate, 'interviewContactDate');
    const lastContactedDate = getDatetime(candidate, 'lastContactedDate');
    const positionId = getHiringPositionId(candidate);
    const stage = _.get(candidate, 'columns.stage.text');

    let type;
    const keys = [];

    if (!lastContactedDate) {
      type = 'info';
      keys.push('no_last_contacted_date');
    } else if (isBefore(lastContactedDate, subMonths(new Date(), 3))) {
      type = 'info';
      keys.push('last_contacted_time_ago');
    }

    if (!interviewDate) {
      type = 'warning';
      keys.push('no_interview_scheduled');
    }
    if (isBefore(interviewDate, new Date())) {
      type = 'warning';
      keys.push('interview_time_passed');
    }

    if (!interviewer) {
      type = 'error';
      keys.push('no_interviewer_assigned');
    }
    if (!recruiter) {
      type = 'error';
      keys.push('no_recruiter_assigned');
    }
    if (!positionId) {
      type = 'error';
      keys.push('no_position_assigned');
    }
    if (!stage) {
      type = 'error';
      keys.push('no_stage_assigned');
    }

    if (!_.isEmpty(keys)) {
      actionItems.push({ type, keys, candidate });
    }
  });

  _.forEach(prospectCandidates, candidate => {
    const lastContactedDate = getDatetime(candidate, 'lastContactedDate');

    let type;
    const keys = [];

    if (!lastContactedDate) {
      type = 'info';
      keys.push('no_last_contacted_date');
    } else if (isBefore(lastContactedDate, subMonths(new Date(), 3))) {
      type = 'info';
      keys.push('last_contacted_time_ago');
    }

    if (!_.isEmpty(keys)) {
      actionItems.push({ type, keys, candidate });
    }
  })

  return (
    <div className={className}>
      <div className='mb-sm'>
        <Text type='title'>{title}</Text>
      </div>

      <div className='flex flex-wrap'>
        {_.isEmpty(actionItems) ? (
          <div>No outstanding action items!</div>
        ) : (
          actionItems.map(actionItem => (
            <ActionItemCard
              key={actionItem.candidate.id}
              keys={actionItem.keys}
              type={actionItem.type}
              candidate={actionItem.candidate}
            />
          ))
        )}
      </div>
    </div>
  );
}

export default CandidateActionItems;
