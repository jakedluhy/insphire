import React from 'react';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import _ from 'lodash';

import { monday } from '../../../monday';
import { useMondayData } from '../../../contexts/monday-data';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS } from '../../../monday/constants';
import { updateItem, createSubitem, deleteItem } from '../../../monday/items';
import { getOrderedStages } from '../../../helpers/hiring-positions';

import { Formik } from 'formik';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import PositionForm from '../CreatePosition/PositionForm';

async function updatePositionRequest(hiringPositionsBoardId, hiringPositionId, values, existingStages) {
  const itemResponse = await updateItem(hiringPositionsBoardId, hiringPositionId, { name: values.jobTitle });
  const itemId = itemResponse.id;

  const existingStageIds = _.map(existingStages, 'id');
  const deleteRequests = existingStageIds.map(id => deleteItem(id));

  const createRequests = values.stagesJson.map((newStageData, index) => createSubitem(
    itemId,
    newStageData.name,
    {
      [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.description]: newStageData.description,
      [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.duration]: newStageData.duration,
      [HIRING_POSITION_STAGES_KEYS_TO_COLUMN_IDS.orderIndex]: index,
    }
  ));

  return Promise.all([...deleteRequests, ...createRequests]);
}

function EditPosition({
  isOpen,
  onRequestClose,
  hiringPosition,
}) {
  const { data: { hiringPositionsBoardId } } = useMondayData();
  const { refetch } = useHiringPositions();

  if (!hiringPosition) return null;

  const orderedStages = getOrderedStages(hiringPosition);
  const initialStagesJson = _.map(orderedStages, stage => ({
    name: stage.name,
    duration: _.get(stage, 'columns.duration.text'),
    description: _.get(stage, 'columns.description.text'),
  }));

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='min-w-xl'
    >
      <Text type='title' className='block mb-md'>Edit {hiringPosition.title}</Text>

      <Formik
        initialValues={{ jobTitle: hiringPosition.title, stagesJson: initialStagesJson, }}
        onSubmit={(values, { setSubmitting }) => {
          updatePositionRequest(
            hiringPositionsBoardId,
            hiringPosition.id,
            values,
            orderedStages
          )
          .then(() => {
            refetch();
            monday.execute('notice', { message: 'Successfully updated position!', type: 'success' });
            onRequestClose();
          })
          .catch(() => {
            monday.execute('notice', { message: 'Failed to update position', type: 'error' });
            setSubmitting(false);
          });
        }}
        validationSchema={yup.object().shape({
          jobTitle: yup.string().required('is required'),
          stagesJson: yup.array().of(
            yup.object().shape({
              name: yup.string(),
              duration: yup.string(),
              description: yup.string(),
            })
          ),
        })}
      >
        {({ values, errors }) => (
          <PositionForm values={values} onRequestClose={onRequestClose} submissionText='Edit Position' />
        )}
      </Formik>
    </Modal>
  );
}

EditPosition.propTypes = {
  isOpen: PropTypes.bool,
};

export default EditPosition;
