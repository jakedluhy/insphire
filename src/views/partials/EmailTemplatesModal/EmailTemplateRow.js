import React, { useState } from 'react';
import cx from 'classnames';
import _ from 'lodash';

import { monday } from '../../../monday';
import { useEmailTemplates } from '../../../contexts/email-templates';
import { deleteItem } from '../../../monday/items'

import Eye from 'react-feather/dist/icons/eye';
import Edit from 'react-feather/dist/icons/edit';
import Trash2 from 'react-feather/dist/icons/trash-2';
import Text from '../../../components/Text';
import IconButton from '../IconButton';

function EmailTemplateRow({ className, emailTemplate, onEdit }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const { refetch } = useEmailTemplates();

  return (
    <div className={cx('py-md flex justify-between', className)}>
      <div>
        <Text type='subtitle'>{emailTemplate.combinedName}</Text>

        {isExpanded ? (
          <div>
            <div className='mb-md'>
              <Text className='font-semibold'>Subject Line Template:</Text>

              <div className='mb-lg px-sm py-xs border rounded-sm bg-riverstone-grey whitespace-pre-wrap text-sm'>
                {_.get(emailTemplate, 'columns.subjectLineTemplate.text')}
              </div>
            </div>

            <div className='mb-md'>
              <Text className='font-semibold'>Body Template:</Text>

              <div className='mb-lg px-sm py-xs border rounded-sm bg-riverstone-grey whitespace-pre-wrap text-sm'>
                {_.get(emailTemplate, 'columns.template.text')}
              </div>
            </div>
          </div>
        ) : null}
      </div>

      <div className='flex'>
        <IconButton
          tooltipTitle='See Template Details'
          icon={Eye}
          onClick={() => setIsExpanded(v => !v)}
        />

        <IconButton
          tooltipTitle='Edit Template'
          icon={Edit}
          onClick={onEdit}
        />

        <IconButton
          tooltipTitle='Delete Template'
          icon={Trash2}
          onClick={() => {
            monday.execute('confirm', {
               message: `Delete ${emailTemplate.name}?`, 
               confirmButton: 'Delete Template', 
               cancelButton: 'Cancel', 
               excludeCancelButton: false,
            }).then((res) => {
              if (_.get(res, 'data.confirm')) {
                deleteItem(emailTemplate.id).then(() => refetch());
              }
            });
          }}
        />
      </div>
    </div>
  );
}

export default EmailTemplateRow;
