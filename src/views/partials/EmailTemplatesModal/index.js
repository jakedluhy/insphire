import React, { useState } from 'react';
import _ from 'lodash';

import { useEmailTemplates } from '../../../contexts/email-templates';

import Search from '../../../icons/Search';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import TextField from '../../../components/TextField';
import EmailTemplateRow from './EmailTemplateRow';

function EmailTemplatesModal({
  isOpen,
  onRequestClose,
  onEdit,
}) {
  const { emailTemplates } = useEmailTemplates();
  const mappedTemplates = _.map(emailTemplates, et => ({
    ...et,
    combinedName: `[${_.get(et, 'columns.type.text')}] ${et.name}`,
  }));

  const [searchValue, setSearchValue] = useState('');

  const filteredTemplates = searchValue
    ? _.filter(mappedTemplates, et => _.toLower(et.combinedName).includes(_.toLower(searchValue)))
    : mappedTemplates;

  return (
    <Modal
      style={{ minHeight: '90vh', width: '36rem' }}
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md pr-lg'>Hiring Positions</Text>

      <TextField
        value={searchValue}
        onChange={e => setSearchValue(e.target.value)}
        icon={<Search size={16} className='text-asphalt-grey' />}
        placeholder='Search Template Name'
      />

      <div className='overflow-y-auto'>
        {_.map(filteredTemplates, (emailTemplate, index) => (
          <EmailTemplateRow
            key={emailTemplate.id}
            className={index !== 0 ? 'border-t border-wolf-grey' : ''}
            emailTemplate={emailTemplate}
            onEdit={() => onEdit(emailTemplate)}
          />
        ))}
      </div>
    </Modal>
  );
}

export default EmailTemplatesModal;
