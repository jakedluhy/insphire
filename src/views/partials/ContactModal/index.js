import React, { useState, useCallback } from 'react';
import _ from 'lodash';
import format from 'date-fns/format';

import { useEmailTemplates } from '../../../contexts/email-templates';
import { useTemplateOptionsToDataMap, transformTemplateToText } from '../../../helpers/email-templates';
import { CANDIDATE_KEYS_TO_COLUMN_IDS } from '../../../monday/constants';
import { useCandidates } from '../../../contexts/candidates';

import Button from '../../../components/Button';
import FormLabel from '../../../components/FormLabel';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import Tooltip from '../../../components/Tooltip';
import Select from '../../../components/Select';
import TextField from '../../../components/TextField';

function getErrorMessage(keys) {
  return _.isEmpty(keys)
    ? null
    : `Missing data to fill in template keys: ${_.join(keys, ', ')}`;
}

function ContactModal({
  isOpen,
  onRequestClose,
  candidate,
}) {
  const { updateCandidate } = useCandidates();
  const templateDataMap = useTemplateOptionsToDataMap(candidate);

  const { emailTemplates } = useEmailTemplates();
  const emailTemplateOptions = _.map(emailTemplates, et => ({
    label: `[${_.get(et, 'columns.type.text')}] ${et.name}`,
    value: _.toString(et.id),
  }));

  const [selectedTemplateId, setSelectedTemplateId] = useState('');
  const [subjectLine, setSubjectLine] = useState('');
  const [subjectLineErrorKeys, setSubjectLineErrorKeys] = useState([]);
  const [body, setBody] = useState('');
  const [bodyErrorKeys, setBodyErrorKeys] = useState([]);

  const bodyLines = _.size(body.split('\n'));

  const mailtoLink = [
    'mailto:',
    _.get(candidate, 'columns.email.text'),
    `?subject=${subjectLine}`,
    `&body=${body}`,
  ].join('');

  let hasSubjectTemplateIssues = false;
  _.forEach(subjectLineErrorKeys, key => {
    if (subjectLine.match(new RegExp(`{{\\s*${key}\\s*}}`))) {
      hasSubjectTemplateIssues = true;
    }
  });
  let hasBodyTemplateIssues = false;
  _.forEach(bodyErrorKeys, key => {
    if (body.match(new RegExp(`{{\\s*${key}\\s*}}`))) {
      hasBodyTemplateIssues = true;
    }
  });

  const hasTemplateIssues = hasSubjectTemplateIssues || hasBodyTemplateIssues;

  const updateLastMessagedTime = useCallback(() => {
    updateCandidate(candidate, {
      [CANDIDATE_KEYS_TO_COLUMN_IDS.lastContactedDate]: format(new Date(), 'yyyy-MM-dd'),
    });
  }, [candidate, updateCandidate]);

  return (
    <Modal
      className='min-w-xl'
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md pr-lg'>Contact {candidate.name}</Text>

      <div className='text-sm'>
        Either select an email template to use or simply type your message into the boxes.
        When you have finished click the messaging link which will open a prefilled email in your email client of choice.&nbsp;
        <a href='https://www.timeatlas.com/email-links-chrome-gmail/' className='text-blue-500'>
          If it's not working this link might help you figure solve the problem.
        </a>
      </div>

      <FormLabel
        className='my-md'
        label='Email Template'
        helperText='Choosing this will override the subject line and email body'
      >
        <Select
          isSearchable
          placeholder='Choose a template'
          items={_.sortBy(emailTemplateOptions, 'label')}
          selectedItemValue={selectedTemplateId}
          onChange={(newTemplateId) => {
            setSelectedTemplateId(newTemplateId);

            const template = _.find(emailTemplates, { id: newTemplateId });
            if (template) {
              const subjectLineObj = transformTemplateToText(
                _.get(template, 'columns.subjectLineTemplate.text', ''),
                templateDataMap
              );

              const bodyObj = transformTemplateToText(
                _.get(template, 'columns.template.text', ''),
                templateDataMap
              );

              setSubjectLine(subjectLineObj.text);
              setSubjectLineErrorKeys(subjectLineObj.errorKeysArray)
              setBody(bodyObj.text);
              setBodyErrorKeys(bodyObj.errorKeysArray)
            }
          }}
        />
      </FormLabel>

      <FormLabel
        className='mb-md'
        label='Subject Line'
        error={hasSubjectTemplateIssues ? getErrorMessage(subjectLineErrorKeys) : null}
        hasErrorLabel={false}
      >
        <TextField
          value={subjectLine}
          onChange={e => setSubjectLine(e.target.value)}
        />
      </FormLabel>

      <FormLabel
        className='w-full'
        label='Email Body'
        error={hasBodyTemplateIssues ? getErrorMessage(bodyErrorKeys) : null}
        hasErrorLabel={false}
      >
        <TextField
          type='textarea'
          value={body}
          onChange={e => setBody(e.target.value)}
          rows={bodyLines < 4 ? 5 : bodyLines + 2}
        />
      </FormLabel>

      <div className='mt-xl mb-lg flex justify-center'>
        <Tooltip title='Fix template errors before sending' disabled={!hasTemplateIssues}>
          {
            // eslint-disable-next-line
          }<a href={hasTemplateIssues ? null : encodeURI(mailtoLink)} target='_blank' rel='noopener noreferrer'>
            <Button
              label='Open Mail Client'
              variant='primary'
              color='primary'
              disabled={hasTemplateIssues}
              onClick={updateLastMessagedTime}
            />
          </a>
        </Tooltip>
      </div>

      <div className='flex justify-end'>
        <Button
          label='Close Modal'
          variant='tertiary'
          color='plain'
          onClick={() => onRequestClose()}
        />
      </div>

      {/* Button link to send email */}
      {/* Close modal */}

      {/* Save as new template? */}
    </Modal>
  );
}

export default ContactModal;
