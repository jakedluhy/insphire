import React from 'react';

import { useMondayData } from '../../../contexts/monday-data';
import {
  ROLE_INTERVIEWER,
  ROLE_HIRING_MANAGER,
  ROLE_RECRUITER,
  ROLE_RECRUITING_MANAGER,
} from '../../../helpers/roles';

import Select from '../../../components/Select';

const BASE_ITEMS = [
  { label: 'Interviewer', value: ROLE_INTERVIEWER },
  { label: 'Hiring Manager', value: ROLE_HIRING_MANAGER },
  { label: 'Recruiter', value: ROLE_RECRUITER },
];

function RoleSelect({ className, style }) {
  const { currentUser, chooseRole } = useMondayData();

  const items = currentUser.is_guest
    ? BASE_ITEMS
    : [...BASE_ITEMS, { label: 'Recruiting Manager', value: ROLE_RECRUITING_MANAGER }];

  return (
    <Select
      className={className}
      style={style}
      items={items}
      selectedItemValue={currentUser.role}
      onChange={chooseRole}
      placeholder='Choose Role'
    />
  );
}

export default RoleSelect;
