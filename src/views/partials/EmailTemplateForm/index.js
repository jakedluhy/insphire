import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import _ from 'lodash';

import { useCandidates } from '../../../contexts/candidates';
import { useEmailTemplateTypes } from '../../../monday/email-templates';
import {
  useTemplateOptionsToDataMap,
  getTemplateSuggestions,
  transformTemplateToText,
  orderByNumDataPointsDesc,
} from '../../../helpers/email-templates';
import { orderByDate } from '../../../helpers/candidates';

import { Formik, Form } from 'formik';
import Button from '../../../components/Button';
import Tooltip from '../../../components/Tooltip';
import FormikTextField from '../../../components/formik/FormikTextField';
import FormikSelect from '../../../components/formik/FormikSelect';
import FormikSubmit from '../../../components/formik/FormikSubmit';



function EmailTemplateForm({
  onSubmit,
  onCancel,
  emailTemplate,
  submitText = 'Create Email Template',
}) {
  const { emailTemplateTypes } = useEmailTemplateTypes();
  const emailTemplateTypeItems = emailTemplateTypes.map(obj => ({ label: obj.label, value: obj.id }));

  const { inProgressCandidates, prospectCandidates, notFitCandidates } = useCandidates();
  const candidates = [...inProgressCandidates, ...prospectCandidates, ...notFitCandidates];
  const candidate = orderByNumDataPointsDesc(orderByDate(candidates, 'createdAt', 'desc'))[0];
  const templateDataMap = useTemplateOptionsToDataMap(candidate);

  const [focusedFieldName, setFocusedFieldName] = useState('');

  return (
    <Formik
      initialValues={{
        templateName: _.get(emailTemplate, 'name', ''),
        templateType: _.get(emailTemplate, 'columns.type.value.index', '').toString(),
        templateSubjectLine: _.get(emailTemplate, 'columns.subjectLineTemplate.text', ''),
        templateBody: _.get(emailTemplate, 'columns.template.text', ''),
      }}
      onSubmit={onSubmit}
      validationSchema={yup.object().shape({
        templateName: yup.string().required('is required'),
        templateType: yup.string().required('is required'),
        templateSubjectLine: yup.string().required('is required'),
        templateBody: yup.string().required('is required'),
      })}
    >
      {({ values }) => {
        const subjectLineSuggestingObj = getTemplateSuggestions(values.templateSubjectLine);
        const bodySuggestingObj = getTemplateSuggestions(values.templateBody);
        const subjectLinePreview = transformTemplateToText(values.templateSubjectLine, templateDataMap);
        const bodyPreview = transformTemplateToText(values.templateBody, templateDataMap);

        const bodyNumLines = _.size(bodyPreview.text.split('\n'));

        return (
          <Form>
            <div className='flex mb-lg'>
              <FormikTextField className='w-full mr-md' name='templateName' size='medium' />

              <FormikSelect
                className='w-full ml-md'
                name='templateType'
                items={emailTemplateTypeItems}
                placeholder='Select a Template Type'
              />
            </div>

            <div className='mb-lg text-sm'>
              Email Templates allow you to dynamically insert candidate information into a templated email, saving valuable time when communicating with candidates.
              Use double brackets (e.g. <strong>{'{{firstName}}'}</strong>) in order to dynamically insert information.
              Start typing <strong>{'{{'}</strong> to see all the options.
            </div>

            <Tooltip
              size='small'
              open={focusedFieldName === 'templateSubjectLine' && subjectLineSuggestingObj.isSuggesting}
              html={_.isEmpty(subjectLineSuggestingObj.options) ? 'No Suggestions Available' : (
                _.map(subjectLineSuggestingObj.options, opt => <div>{opt}</div>)
              )}
            >
              <FormikTextField
                className='mb-sm w-full'
                name='templateSubjectLine'
                placeholder='Your Interview on {{interviewContactDate}}'
                onFocus={() => setFocusedFieldName('templateSubjectLine')}
                onBlur={() => setFocusedFieldName(v => v === 'templateSubjectLine' ? '' : v)}
              />
            </Tooltip>

            <div className='mb-lg px-sm py-xs border rounded-sm bg-riverstone-grey whitespace-pre-wrap'>
              {subjectLinePreview.text}
            </div>

            <Tooltip
              size='small'
              open={focusedFieldName === 'templateBody' && bodySuggestingObj.isSuggesting}
              html={_.isEmpty(bodySuggestingObj.options) ? 'No Suggestions Available' : (
                _.map(bodySuggestingObj.options, opt => <div>{opt}</div>)
              )}
            >
              <FormikTextField
                className='mb-sm'
                type='textarea'
                name='templateBody'
                rows={bodyNumLines < 4 ? 5 : bodyNumLines + 2}
                placeholder='Dear {{firstName}},'
                onFocus={() => setFocusedFieldName('templateBody')}
                onBlur={() => setFocusedFieldName(v => v === 'templateBody' ? '' : v)}
              />
            </Tooltip>

            <div className='mb-lg px-sm py-xs border rounded-sm bg-riverstone-grey whitespace-pre-wrap'>
              {bodyPreview.text}
            </div>

            <div className='flex justify-end items-center'>
              <Button
                className='mr-sm'
                label='Cancel'
                type='button'
                variant='tertiary'
                onClick={() => onCancel()}
              />

              <FormikSubmit variant='primary' color='primary' label={submitText} />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

EmailTemplateForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
};

EmailTemplateForm.defaultProps = {
  onSubmit: () => {},
  onCancel: () => {},
};

export default EmailTemplateForm;
