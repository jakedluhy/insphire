import React from 'react';
import PropTypes from 'prop-types';

import { monday } from '../../../monday';
import { useMondayData } from '../../../contexts/monday-data';
import { useEmailTemplates } from '../../../contexts/email-templates';
import { EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS } from '../../../monday/constants';
import { updateItem } from '../../../monday/items';

import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import EmailTemplateForm from '../EmailTemplateForm';

async function updateEmailTemplateRequest(emailTemplateBoardId, emailTemplateId, values) {
  return updateItem(emailTemplateBoardId, emailTemplateId, {
    name: values.templateName,
    [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.type]: values.templateType,
    [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.subjectLineTemplate]: values.templateSubjectLine,
    [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.template]: values.templateBody,
  });
}

function EditEmailTemplate({
  isOpen,
  onRequestClose,
  emailTemplate,
}) {
  const { data: { emailTemplatesBoardId } } = useMondayData();
  const { refetch } = useEmailTemplates();

  if (!emailTemplate) return null;

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='min-w-xl'
    >
      <Text type='title' className='block mb-md'>Edit {emailTemplate.name}</Text>

      <EmailTemplateForm
        onSubmit={(values, { setSubmitting }) => {
          updateEmailTemplateRequest(emailTemplatesBoardId, emailTemplate.id, values)
          .then(() => {
            refetch()
            monday.execute('notice', { message: 'Successfully updated template!', type: 'success' });
            onRequestClose();
          })
          .catch(() => {
            monday.execute('notice', { message: 'Failed to update template', type: 'error' });
            setSubmitting(false);
          });
        }}
        onCancel={onRequestClose}
        emailTemplate={emailTemplate}
        submitText='Update Email Template'
      />
    </Modal>
  );
}

EditEmailTemplate.propTypes = {
  isOpen: PropTypes.bool,
};

export default EditEmailTemplate;
