import React from 'react';

import Button from '../../../components/Button';
import Tooltip from '../../../components/Tooltip';

function IconButton({ icon: Icon, tooltipTitle, ...rest }) {
  return (
    <Tooltip title={tooltipTitle}>
      <Button
        size='small'
        variant='tertiary'
        color='plain'
        {...rest}
      >
        <Icon size={18} />
      </Button>
    </Tooltip>
  );
}

export default IconButton;
