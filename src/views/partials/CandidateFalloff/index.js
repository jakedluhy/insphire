import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { useHiringPositions } from '../../../contexts/hiring-positions';
import { useStages } from '../../../helpers/hiring-positions';
import { NIVO_COLOR_SCHEME } from '../../../helpers/colors';
import { getYTickValues } from '../../../helpers/nivo';

import { ResponsiveBar } from '@nivo/bar';
import Text from '../../../components/Text';
import Select from '../../../components/Select';

function CandidateFalloff({
  className,
  title = 'Your Candidate Falloff',
  titleType = 'title',
  candidates,
  shouldShowPositionFilter,
}) {
  const [positionFilterId, setPositionFilterId] = useState('');

  const { hiringPositions } = useHiringPositions();
  const { orderedStages } = useStages();

  const hiringPositionOptions = [
    { label: 'All Positions', value: '' },
    ..._.map(hiringPositions, hp => ({ label: hp.title, value: _.toString(hp.id) })),
  ];

  const filteredCandidates = _.filter(candidates, candidate => {
    if (positionFilterId === '') return true;

    const positionId = _.get(candidate, 'columns.position.value.linkedPulseIds[0].linkedPulseId');
    return _.toString(positionId) === positionFilterId;
  });
  const groupedCandidates = _.groupBy(filteredCandidates, (c) => _.get(c, 'columns.stage.text'));
  const candidateCounts = _.mapValues(groupedCandidates, values => _.size(values));

  const data = _.map(orderedStages, (stage, index) => ({
    stageName: stage,
    value: candidateCounts[stage] || 0,
    color: NIVO_COLOR_SCHEME[index],
  }));

  return (
    <div className={className}>
      <div className='mb-sm flex justify-between items-center'>
        <div className='mr-xl'>
          <Text className='block mb-xs' type={titleType}>{title}</Text>
          <Text>This graph shows a breakdown of where your candidates have fallen out of the hiring process. This can give insight into tweaks you can make at key points.</Text>
        </div>

        {shouldShowPositionFilter ? (
          <Select
            isSearchable
            style={{ width: 300 }}
            selectedItemValue={positionFilterId}
            onChange={setPositionFilterId}
            items={hiringPositionOptions}
          />
        ) : null}
      </div>

      {_.isEmpty(candidates) ? (
        <div className='p-xl flex justify-center items-center'>
          <div className='max-w-md'>
            No Candidates found in the "Not a Fit" category. This region will populate as you move candidates through the pipeline.
          </div>
        </div>
      ) : (
        <div className='pt-lg' style={{ height: 350 }}>
          <ResponsiveBar
            data={data}
            indexBy='stageName'
            margin={{ bottom: 105, left: 60, top: 10 }}
            padding={0.3}
            colors={({ data }) => data.color}
            colorBy='index'
            enableGridY={false}
            isInteractive={false}
            enableLabel={false}
            axisBottom={{
              tickSize: 5,
              tickPadding: 5,
              legend: 'Stage Reached',
              legendPosition: 'middle',
              legendOffset: 80,
              tickRotation: -30,
            }}
            axisLeft={{
              tickSize: 5,
              tickPadding: 5,
              legend: 'Number of Candidates',
              legendPosition: 'middle',
              legendOffset: -40,
              tickValues: getYTickValues(data),
            }}
          />
        </div>
      )}
    </div>
  );
}

CandidateFalloff.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  candidates: PropTypes.arrayOf(PropTypes.object),
  shouldShowPositionFilter: PropTypes.bool,
};

CandidateFalloff.defaultProps = {
  shouldShowPositionFilter: true,
};

export default CandidateFalloff;
