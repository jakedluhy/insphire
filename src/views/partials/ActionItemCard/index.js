import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import cx from 'classnames';

import { monday } from '../../../monday';
import { KEY_TO_TITLE_MAP, TYPE_TO_COLOR_MAP } from './helpers';

import Email from 'react-feather/dist/icons/mail';
import MessageSquare from 'react-feather/dist/icons/message-square';
import IconButton from '../IconButton';
import ContactModal from '../ContactModal';
import ActionModal from './ActionModal';

function ActionItemCard({
  keys,
  type,
  candidate,
}) {
  const [isContactModalOpen, setIsContactModalOpen] = useState(false);
  const [isActionModalOpen, setIsActionModalOpen] = useState(false);

  return (
    <>
      <div
        className={cx(
          'bg-riverstone-grey hover:bg-ui-grey cursor-pointer',
          'flex justify-between items-center py-md pr-md pl-lg relative',
        )}
        style={{ width: 'calc(50% - 1px)', marginRight: 1, marginBottom: 1 }}
        onClick={() => {
          if (_.includes(keys, 'last_contacted_time_ago')) {
            setIsContactModalOpen(true);
          } else {
            setIsActionModalOpen(true)
          }
        }}
      >
        <div className={`${TYPE_TO_COLOR_MAP[type]} absolute top-0 bottom-0 left-0`} style={{ width: 8 }} />

        <div>
          <div>{candidate.name}</div>

          {_.map(keys, key => (
            <div key={key} className='font-semibold text-sm'>
              {KEY_TO_TITLE_MAP[key]}
            </div>
          ))}
        </div>

        <div className='flex'>
          <IconButton
            tooltipTitle='Send Message'
            icon={Email}
            onClick={e => {
              e.stopPropagation();
              setIsContactModalOpen(true);
            }}
          />

          <IconButton
            tooltipTitle='View Discussion'
            icon={MessageSquare}
            onClick={e => {
              e.stopPropagation();
              monday.execute('openItemCard', { itemId: Number(candidate.id), kind: 'updates' });
            }}
          />
        </div>
      </div>

      <ContactModal
        isOpen={isContactModalOpen}
        onRequestClose={() => setIsContactModalOpen(false)}
        candidate={candidate}
      />

      <ActionModal
        isOpen={isActionModalOpen}
        onRequestClose={() => setIsActionModalOpen(false)}
        candidate={candidate}
        keys={keys}
      />
    </>
  );
}

ActionItemCard.propTypes = {
  keys: PropTypes.arrayOf(PropTypes.string).isRequired,
  type: PropTypes.oneOf(['error', 'warning', 'info']),
  candidate: PropTypes.object,
};

ActionItemCard.defaultProps = {
  candidate: {},
  type: 'info',
};

export default ActionItemCard;
