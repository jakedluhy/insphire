export const KEY_TO_TITLE_MAP = {
  no_interview_scheduled: 'No interview scheduled',
  interview_time_passed: 'Interview time has passed',
  no_interviewer_assigned: 'No interviewer assigned',
  no_recruiter_assigned: 'No recruiter assigned',
  no_stage_assigned: 'No interview stage indicated',
  no_position_assigned: 'No hiring position assigned',
  no_last_contacted_date: 'No last contacted date set',
  last_contacted_time_ago: 'Last contact was more than 3 months ago',
};

export const TYPE_TO_COLOR_MAP = {
  error: 'bg-content-sunset-red',
  warning: 'bg-content-egg-yolk',
  info: 'bg-content-light-blue',
};
