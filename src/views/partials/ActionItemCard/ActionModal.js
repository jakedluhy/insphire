import React from 'react';
import _ from 'lodash';
import format from 'date-fns/format';

import { monday } from '../../../monday';
import { CANDIDATE_KEYS_TO_COLUMN_IDS, STATUS_ITEMS } from '../../../monday/constants';
import { getDatetime, getHiringPositionId, getPersonId } from '../../../helpers/candidates';
import { getUTCDate } from '../../../helpers/dates';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { useUsers } from '../../../contexts/users';
import { useCandidates } from '../../../contexts/candidates';

import { Formik, Form } from 'formik';
import Button from '../../../components/Button';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import FormikDateTimePicker from '../../../components/formik/FormikDateTimePicker';
import FormikSelect from '../../../components/formik/FormikSelect';
import FormikSubmit from '../../../components/formik/FormikSubmit';

function getFormKeyComponent({ key, selectedHiringPosition, stageOptions, usersOptions, hiringPositionOptions }) {
  switch(key) {
    case 'no_interviewer_assigned':
      return <FormikSelect isSearchable name='upcomingInterviewer' items={usersOptions} />;
    case 'no_recruiter_assigned':
      return <FormikSelect isSearchable name='recruiter' items={usersOptions} />;
    case 'no_position_assigned':
      return (
        <FormikSelect
          isSearchable
          name='position'
          items={hiringPositionOptions}
        />
      )
    case 'no_stage_assigned':
      return _.isEmpty(stageOptions)
        ? 'No stages have been assigned for the assigned hiring position'
        : <FormikSelect name='stage' items={stageOptions} />;
    case 'no_interview_scheduled':
    case 'interview_time_passed':
      return (
        <>
          <FormikDateTimePicker
            className='mb-lg'
            name='interviewContactDate'
            label='Upcoming Interview Date/Time'
          />

          <FormikSelect className='mb-lg' name='status' items={STATUS_ITEMS} />

          <FormikSelect name='stage' items={stageOptions} />
        </>
      );
    case 'no_last_contacted_date':
      return (
        <FormikDateTimePicker name='lastContactedDate' label='Last Contacted Date' showTimeInput={false} />
      );
    default:
      return null;
  }
}

function ActionModal({
  isOpen,
  onRequestClose,
  candidate,
  keys,
}) {
  const { users } = useUsers();
  const { hiringPositions } = useHiringPositions();
  const { updateCandidate, updateCandidateStatus } = useCandidates();

  const usersOptions = _.map(users, (u) => ({ label: u.name, value: _.toString(u.id) }));
  const hiringPositionOptions = _.map(hiringPositions, (hp) => ({ label: hp.title, value: _.toString(hp.id) }));

  const candidateStatus = _.get(candidate, 'group.id', '');

  return (
    <Modal
      className='min-w-xl'
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md pr-lg'>Take Action for {candidate.name}</Text>

      <Formik
        initialValues={{
          position: getHiringPositionId(candidate) || '',
          stage: _.get(candidate, 'columns.stage.text', ''),
          interviewContactDate: getDatetime(candidate, 'interviewContactDate') || null,
          lastContactedDate: getDatetime(candidate, 'lastContactedDate') || null,
          recruiter: getPersonId(candidate, 'recruiter') || '',
          upcomingInterviewer: getPersonId(candidate, 'upcomingInterviewer') || '',
          status: candidateStatus,
        }}
        onSubmit={(values, { setSubmitting }) => {
          updateCandidate(candidate, {
            [CANDIDATE_KEYS_TO_COLUMN_IDS.position]: values.position ? { item_ids: [Number(values.position)] } : null,
            [CANDIDATE_KEYS_TO_COLUMN_IDS.stage]:  values.stage,
            [CANDIDATE_KEYS_TO_COLUMN_IDS.interviewContactDate]: (
              values.interviewContactDate
                ? format(
                  getUTCDate(values.interviewContactDate),
                  'yyyy-MM-dd HH:mm'
                )
                : null
            ),
            [CANDIDATE_KEYS_TO_COLUMN_IDS.lastContactedDate]: (
              values.lastContactedDate
                ? format(values.lastContactedDate, 'yyyy-MM-dd')
                : null
            ),
            [CANDIDATE_KEYS_TO_COLUMN_IDS.recruiter]: values.recruiter,
            [CANDIDATE_KEYS_TO_COLUMN_IDS.upcomingInterviewer]: values.upcomingInterviewer,
          })
          .then(() => {
            return values.status !== candidateStatus
              ? updateCandidateStatus(candidate, values.status)
              : null;
          })
          .then(() => {
            monday.execute('notice', { message: 'Updated Candidate Successfully!', type: 'success' });
            onRequestClose();
          });
        }}
      >
        {({ values }) => {
          const selectedHiringPosition = _.find(hiringPositions, { id: values.position });
          const stageOptions = _.map(_.get(selectedHiringPosition, 'columns.stages'), stage => ({
            label: stage.name,
            value: stage.name,
          }));

          return (
            <Form>
              {keys.map(key => {
                const formKeyComponent = getFormKeyComponent({
                  key,
                  selectedHiringPosition,
                  stageOptions,
                  usersOptions,
                  hiringPositionOptions,
                });

                return formKeyComponent ? (
                  <div key={key} className='mb-lg'>
                    {formKeyComponent}
                  </div>
                ) : null;
              })}

              <div className='mt-lg flex justify-end items-center'>
                <Button
                  className='mr-sm'
                  label='Cancel'
                  type='button'
                  variant='tertiary'
                  onClick={() => onRequestClose()}
                />

                <FormikSubmit label='Update Candidate' />
              </div>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
}

export default ActionModal;
