import React, { useState } from 'react';
import _ from 'lodash';
import cx from 'classnames';
import * as yup from 'yup';

import { monday } from '../../../monday';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { useMondayData } from '../../../contexts/monday-data';
import { useCandidates } from '../../../contexts/candidates';
import { getHiringPosition } from '../../../helpers/candidates';
import { createCandidateUpdate } from '../../../monday/candidates';
import { getStage } from '../../../helpers/hiring-positions';
import {
  CANDIDATE_PROSPECTS_GROUP_ID,
  STATUS_ITEMS,
} from '../../../monday/constants';
import {
  ROLE_HIRING_MANAGER,
  ROLE_RECRUITER,
  ROLE_RECRUITING_MANAGER,
} from '../../../helpers/roles';

import { Formik, Form } from 'formik';
import Button from '../../../components/Button';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import FormikTextField from '../../../components/formik/FormikTextField';
import FormikSelect from '../../../components/formik/FormikSelect';
import FormikSubmit from '../../../components/formik/FormikSubmit';

const INTERVIEW_STATUS_ITEMS = _.reject(STATUS_ITEMS, { value: CANDIDATE_PROSPECTS_GROUP_ID });

function CommunicationLink({
  className,
  label,
  link,
  linkText,
}) {
  if (!linkText) return null;

  return (
    <div className={cx(className, 'flex')}>
      <Text className='font-semibold mr-xs'>{label}:</Text>
      <a
        className='text-blue-500'
        href={link}
        target='_blank'
        rel='noopener noreferrer'
      >
        {linkText}
      </a>
    </div>
  );
}

function ConductInterviewButton({
  candidate,
  onSuccess,
}) {
  const [isOpen, setIsOpen] = useState(false);
  const { hiringPositions } = useHiringPositions();
  const { currentUser } = useMondayData();
  const { updateCandidateStatus } = useCandidates();

  const hiringPosition = getHiringPosition(candidate, hiringPositions);
  const stage = getStage(hiringPosition, candidate);

  const phoneNumber = _.get(candidate, 'columns.phone.text');
  const email = _.get(candidate, 'columns.email.text');
  const videoMeetingLink = _.get(candidate, 'columns.videoMeetingLink.text');
  const candidateGroupId = _.get(candidate, 'group.id', '');

  const isDecisionMakingRole = [
    ROLE_HIRING_MANAGER,
    ROLE_RECRUITER,
    ROLE_RECRUITING_MANAGER,
  ].includes(currentUser.role);

  return (
    <>
      <Button
        size='small'
        variant='secondary'
        onClick={() => setIsOpen(true)}
        label='Conduct Interview'
      />

      <Modal
        className='min-w-xl'
        isOpen={isOpen}
        onRequestClose={() => setIsOpen(false)}
      >
        <Text type='title' className='block mb-md pr-lg'>{candidate.name}</Text>

        <CommunicationLink label='Phone Number' link={`tel:${phoneNumber}`} linkText={phoneNumber} />
        <CommunicationLink label='Email' link={`mailto:${email}`} linkText={email} />
        <CommunicationLink label='Video Meeting Link' link={videoMeetingLink} linkText={videoMeetingLink} />

        <Text type='subtitle' className='block mt-lg'>Meeting Type: {_.get(stage, 'name')}</Text>
        <Text>{_.get(stage, 'columns.description.text')}</Text>

        <div
          className='mt-md text-blue-500 cursor-pointer'
          onClick={() => monday.execute('openItemCard', { itemId: Number(candidate.id), kind: 'updates' })}
        >
          View Previous Notes
        </div>

        <Formik
          initialValues={{ interviewNotes: '', candidateStatus: candidateGroupId }}
          validationSchema={yup.object().shape({
            interviewNotes: yup.string().required('is required'),
            candidateStatus: yup.string(),
          })}
          onSubmit={(values, { setSubmitting }) => {
            createCandidateUpdate(candidate.id, values.interviewNotes)
            .then(res => {
              if (values.candidateStatus !== candidateGroupId) {
                return updateCandidateStatus(candidate, values.candidateStatus);
              } else {
                return res;
              }
            })
            .then(() => {
              monday.execute('notice', { message: 'Feedback Successfully Submitted!', type: 'success' });
              setIsOpen(false);
              if (onSuccess) onSuccess();
            })
            .catch(() => {
              monday.execute('notice', { message: 'Failed to submit feedback', type: 'error' });
              setSubmitting(false);
            });
          }}
        >
          {({ values }) => {
            const notesLines = _.get(values, 'interviewNotes', '').split('\n').length;

            return (
              <Form>
                {isDecisionMakingRole ? (
                  <FormikSelect className='mt-lg' name='candidateStatus' items={INTERVIEW_STATUS_ITEMS} />
                ) : null}

                <FormikTextField
                  className='w-full my-lg'
                  type='textarea'
                  name='interviewNotes'
                  placeholder='Notes on the interview...'
                  rows={notesLines < 4 ? 5 : notesLines + 2}
                />

                <div className='flex justify-end'>
                  <div className='flex'>
                    <Button
                      className='mr-sm'
                      label='Cancel'
                      variant='tertiary'
                      onClick={() => setIsOpen(false)}
                    />

                    <FormikSubmit label='Submit Feedback' />
                  </div>
                </div> 
              </Form>
            );
          }}
        </Formik>
      </Modal>
    </>
  );
}

export default ConductInterviewButton;
