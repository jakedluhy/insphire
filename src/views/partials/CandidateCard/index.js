import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import format from 'date-fns/format';
import isToday from 'date-fns/isToday';
import isTomorrow from 'date-fns/isTomorrow';
import addMinutes from 'date-fns/addMinutes';
import _ from 'lodash';
// import ics from 'ics';

import { monday } from '../../../monday';
import { getDatetime, getHiringPosition, getPerson } from '../../../helpers/candidates';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { useMondayData } from '../../../contexts/monday-data';
import { useUsers } from '../../../contexts/users';
import { ROLE_INTERVIEWER } from '../../../helpers/roles';
import { getUTCDate } from '../../../helpers/dates';
import useLocalStorage from '../../../hooks/use-local-storage';

import Linkedin from 'react-feather/dist/icons/linkedin';
import Phone from 'react-feather/dist/icons/phone';
import Email from 'react-feather/dist/icons/mail';
import FileText from 'react-feather/dist/icons/file-text';
import MessageSquare from 'react-feather/dist/icons/message-square';
import Eye from 'react-feather/dist/icons/eye';
import Button from '../../../components/Button';
import Tooltip from '../../../components/Tooltip';
import IconButton from '../IconButton';
import ConductInterviewButton from './ConductInterviewButton';
import ContactModal from '../ContactModal';

const GCAL_FORMAT = "yyyyMMdd'T'HHmm00'Z'";

const formatInterviewDate = date => {
  if (!date) return 'No Interview Scheduled';

  const dateStr = isToday(date)
    ? 'Today'
    : isTomorrow(date)
      ? 'Tomorrow'
        : format(date, 'MMM do');

  return `${dateStr}, ${format(date, 'h:mm aa')}`
};

function ButtonLinkTo({ to, icon: Icon, tooltipTitle, ...rest }) {
  return (
    <Tooltip title={tooltipTitle}>
      <a href={to} target='_blank' rel='noopener noreferrer'>
        <Button
          size='small'
          variant='tertiary'
          color='plain'
          {...rest}
        >
          <Icon size={18} />
        </Button>
      </a>
    </Tooltip>
  );
}

function UserDisplay({ label, user, shouldShow }) {
  if (!shouldShow || !user)  return null;

  return (
    <div className='text-sm mb-sm'>
      <span className='font-semibold mr-xs'>
        {label}
      </span>

      <a href={user.url} className='text-blue-500' target='_blank' rel='noopener noreferrer'>
        {user.name}
      </a>
    </div>
  );
}

function CandidateCard({
  candidate,
  barColor,
  shouldShowAddToCalendar,
  shouldShowStage,
  shouldShowInterviewer,
  shouldShowHiringManager,
  shouldShowRecruiter,
  shouldShowConductInterviewButton,
}) {
  const { users } = useUsers();
  const { hiringPositions } = useHiringPositions();
  const { currentUser } = useMondayData();

  const [isContactModalOpen, setIsContactModalOpen] = useState(false);
  const [hasAddedToCalendar, setHasAddedToCalendar] = useLocalStorage(`candidate-${candidate.id}-calendar`, false);

  const interviewDate = getDatetime(candidate);
  const position = getHiringPosition(candidate, hiringPositions);
  const email = _.get(candidate, 'columns.email.text');
  const phoneNumber = _.get(candidate, 'columns.phone.text');
  const videoMeetingLink = _.get(candidate, 'columns.videoMeetingLink.text');
  const linkedIn = _.get(candidate, 'columns.linkedin.text');
  const resume = _.get(candidate, 'columns.resume.text');;
  const stageText = _.get(candidate, 'columns.stage.text');

  const interviewer = getPerson(candidate, 'upcomingInterviewer', users);
  const hiringManager = getPerson(candidate, 'hiringManager', users);
  const recruiter = getPerson(candidate, 'recruiter', users);

  const stage = _.find(_.get(position, 'columns.stages'), { name: stageText });
  const stageDuration = _.get(stage, 'columns.duration.text');
  const stageDurationNum = stageDuration ? Number(stageDuration) : null;

  const currentUserRole = _.get(currentUser, 'role');

  const calendarLocation = _.compact([
    phoneNumber ? `Phone: ${phoneNumber}` : null,
    videoMeetingLink ? `Video: ${videoMeetingLink}` : null,
  ]).join(' <> ');
  const interviewEndTime = interviewDate && stageDurationNum ? addMinutes(interviewDate, stageDurationNum * 60) : interviewDate;

  const googleCalendarLink = _.compact([
    'https://www.google.com/calendar/render?action=TEMPLATE',
    `&text=${_.get(stage, 'name')}`,
    `&details=${_.get(stage, 'columns.description.text')}`,
    `&location=${calendarLocation}`,
    interviewDate ? `&dates=${format(getUTCDate(interviewDate), GCAL_FORMAT)}` : null,
    interviewEndTime ? `/${format(getUTCDate(interviewEndTime), GCAL_FORMAT)}` : null,
  ]).join('');

  const downloadCalendarEvent = useCallback(() => {
    const cal = window.ics();
    cal.addEvent(
      _.get(stage, 'name'),
      _.get(stage, 'columns.description.text'),
      calendarLocation,
      interviewDate.toJSON(),
      interviewEndTime.toJSON(),
    );
    cal.download();
    setHasAddedToCalendar(true);
  }, [stage, interviewDate, calendarLocation, interviewEndTime, setHasAddedToCalendar]);

  return (
    <>
      <div
        className='bg-riverstone-grey py-md pr-md pl-lg relative'
        style={{ marginBottom: 1 }}
      >
        <div className={`${barColor} absolute top-0 bottom-0 left-0`} style={{ width: 8 }} />

        <div className='flex justify-between'>
          <div>{candidate.name}</div>

          <div className='text-sm font-semibold'>
            {formatInterviewDate(interviewDate)}
          </div>
        </div>

        <div className='mb-md flex justify-between items-center'>
          <div className='text-sm'>{_.get(position, 'title')}</div> 

          {shouldShowStage ? (
            <div className='text-sm'>
              {stageText}
            </div>
          ) : null}
        </div>

        <UserDisplay label='Upcoming Interviewer:' user={interviewer} shouldShow={shouldShowInterviewer} />
        <UserDisplay label='Hiring Manager:' user={hiringManager} shouldShow={shouldShowHiringManager} />
        <UserDisplay label='Recruiter:' user={recruiter} shouldShow={shouldShowRecruiter} />

        {shouldShowAddToCalendar && !hasAddedToCalendar && stage && interviewDate ? (
          <div className='text-sm mb-sm'>
            <span className='font-semibold mr-xs'>Add to Calendar:</span>

            <a
              href={encodeURI(googleCalendarLink)}
              className='text-blue-500'
              target='_blank'
              rel='noopener noreferrer'
              onClick={() => setHasAddedToCalendar(true)}
            >
              Google Calendar
            </a>

            {window.ics ? (
              <>
                <span className='mx-sm'>or</span>

                {
                  // eslint-disable-next-line
                }<a className='cursor-pointer text-blue-500' onClick={downloadCalendarEvent}>
                  iCal, Outlook, etc...
                </a>
              </>
            ) : null}
          </div>
        ) : null}

        <div className='mt-md flex justify-between items-end'>
          <div className='flex'>
            <IconButton
              tooltipTitle='View Discussion'
              icon={MessageSquare}
              onClick={() => monday.execute('openItemCard', { itemId: Number(candidate.id), kind: 'updates' })}
            />

            <IconButton
              tooltipTitle='View Candidate Profile'
              icon={Eye}
              onClick={() => monday.execute('openItemCard', { itemId: Number(candidate.id), kind: 'columns' })}
            />

            {linkedIn && (
              <ButtonLinkTo to={linkedIn} icon={Linkedin} tooltipTitle='View LinkedIn' />
            )}

            {resume && (
              <ButtonLinkTo to={resume} icon={FileText} tooltipTitle='View Resume' />
            )}

            {phoneNumber && (
              <ButtonLinkTo to={`tel:${phoneNumber}`} icon={Phone} tooltipTitle={`Call ${phoneNumber}`} />
            )}

            {email && (
              <IconButton
                tooltipTitle='Send Message'
                icon={Email}
                onClick={() => setIsContactModalOpen(true)}
              />
            )}
          </div>

          {shouldShowConductInterviewButton ? (
            <div>
              <ConductInterviewButton
                candidate={candidate}
                onSuccess={() => {
                  if (currentUserRole && currentUserRole !== ROLE_INTERVIEWER) {
                    setIsContactModalOpen(true);
                  }
                }}
              />
            </div>
          ) : null}
        </div>
      </div>

      <ContactModal
        isOpen={isContactModalOpen}
        onRequestClose={() => setIsContactModalOpen(false)}
        candidate={candidate}
      />
    </>
  );
}

CandidateCard.propTypes = {
  candidate: PropTypes.object,
  barColor: PropTypes.string.isRequired,
  shouldShowStage: PropTypes.bool,
  shouldShowInterviewer: PropTypes.bool,
  shouldShowHiringManager: PropTypes.bool,
  shouldShowRecruiter: PropTypes.bool,
};

CandidateCard.defaultProps = {
  shouldShowStage: true,
  shouldShowInterviewer: false,
  shouldShowHiringManager: false,
  shouldShowRecruiter: false,
};

export default CandidateCard;
