import React from 'react';
import PropTypes from 'prop-types';

import { monday } from '../../../monday';
import { useMondayData } from '../../../contexts/monday-data';
import { useEmailTemplates } from '../../../contexts/email-templates';
import {
  EMAIL_TEMPLATES_DEFAULT_GROUP_ID,
  EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS,
} from '../../../monday/constants';
import { createItem } from '../../../monday/items';

import EmailTemplateForm from '../EmailTemplateForm';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';

function createEmailTemplateRequest(emailTemplateBoardId, values) {
  return createItem(
    emailTemplateBoardId,
    EMAIL_TEMPLATES_DEFAULT_GROUP_ID,
    values.templateName,
    {
      [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.type]: values.templateType,
      [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.subjectLineTemplate]: values.templateSubjectLine,
      [EMAIL_TEMPLATES_KEYS_TO_COLUMN_IDS.template]: values.templateBody,
    }
  );
}

function CreateEmailTemplate({
  isOpen,
  onRequestClose,
  onSuccess,
}) {
  const { data: { emailTemplatesBoardId } } = useMondayData();
  const { refetch } = useEmailTemplates();

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='min-w-xl'
    >
      <Text type='title' className='block mb-md'>Create an Email Template</Text>

      <EmailTemplateForm
        onSubmit={(values, { setSubmitting }) => {
          createEmailTemplateRequest(emailTemplatesBoardId, values)
          .then((item) => {
            setSubmitting(false);
            monday.execute('notice', { message: `Created New Template ${values.templateName}`, type: 'success' });
            onSuccess(item);
            refetch();
          })
          .catch((e) => {
            monday.execute('notice', { message: 'Failed to create new email template', type: 'error' });
            setSubmitting(false);
          });
        }}
        onCancel={onRequestClose}
        submitText='Create Email Template'
      />
    </Modal>
  );
}

CreateEmailTemplate.propTypes = {
  isOpen: PropTypes.bool,
};

export default CreateEmailTemplate;
