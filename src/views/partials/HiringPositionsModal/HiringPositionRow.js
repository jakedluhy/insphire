import React, { useState } from 'react';
import _ from 'lodash';
import cx from 'classnames';
import pluralize from 'pluralize';

import { monday } from '../../../monday';
import { useHiringPositions } from '../../../contexts/hiring-positions';
import { getOrderedStages } from '../../../helpers/hiring-positions';
import { deleteItem } from '../../../monday/items'

import Eye from 'react-feather/dist/icons/eye';
import Edit from 'react-feather/dist/icons/edit';
import Trash2 from 'react-feather/dist/icons/trash-2';
import Text from '../../../components/Text';
import IconButton from '../IconButton';

function HiringPositionRow({ className, hiringPosition, onEdit }) {
  const [isExpanded, setIsExpanded] = useState(false);
  const { refetch } = useHiringPositions();

  const orderedStages = getOrderedStages(hiringPosition);
  const stageNames = _.map(orderedStages, 'name');

  return (
    <div className={cx('py-md flex justify-between', className)}>
      <div>
        <Text type='subtitle'>{hiringPosition.title}</Text>

        {isExpanded ? (
          <div>
            {_.map(orderedStages, stage => {
              const duration = _.get(stage, 'columns.duration.text');
              const formattedDuration = !duration
                ? null
                : Number(duration) < 1
                  ? `${Number(duration) * 60} minutes`
                  : pluralize('hour', Number(duration), true);

              return (
                <div key={stage.id} className='mb-md'>
                  <Text>
                    <span className='font-semibold'>{stage.name}</span>&nbsp;
                    {duration && `(${formattedDuration})`}
                  </Text>

                  <div className='text-sm whitespace-pre-wrap'>
                    {_.get(stage, 'columns.description.text')}
                  </div>
                </div>
              );
            })}
          </div>
        ) : (
          <Text>
            <strong>Stages:</strong>&nbsp;
            {_.isEmpty(stageNames) ? (
              'None'
            ) : (
              stageNames.join(', ')
            )}
          </Text>
        )}
      </div>

      <div className='flex'>
        {!_.isEmpty(stageNames) ? (
          <IconButton
            tooltipTitle='See Stage Details'
            icon={Eye}
            onClick={() => setIsExpanded(v => !v)}
          />
        ) : null}

        <IconButton
          tooltipTitle='Edit Position and Stages'
          icon={Edit}
          onClick={onEdit}
        />

        <IconButton
          tooltipTitle='Delete Position'
          icon={Trash2}
          onClick={() => {
            monday.execute('confirm', {
               message: `Delete ${hiringPosition.title} Position?`, 
               confirmButton: 'Delete Position', 
               cancelButton: 'Cancel', 
               excludeCancelButton: false,
            }).then((res) => {
              if (_.get(res, 'data.confirm')) {
                deleteItem(hiringPosition.id).then(() => refetch());
              }
            });
          }}
        />
      </div>
    </div>
  );
}

export default HiringPositionRow;
