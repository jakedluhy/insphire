import React, { useState } from 'react';
import _ from 'lodash';

import { useHiringPositions } from '../../../contexts/hiring-positions';

import Search from '../../../icons/Search';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import TextField from '../../../components/TextField';
import HiringPositionRow from './HiringPositionRow';

function HiringPositionsModal({
  isOpen,
  onRequestClose,
  onEdit,
}) {
  const { hiringPositions } = useHiringPositions();

  const [searchValue, setSearchValue] = useState('');

  const filteredPositions = searchValue
    ? _.filter(hiringPositions, hp => _.toLower(hp.title).includes(_.toLower(searchValue)))
    : hiringPositions;

  return (
    <Modal
      style={{ minHeight: '90vh', width: '36rem' }}
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md pr-lg'>Hiring Positions</Text>

      <TextField
        value={searchValue}
        onChange={e => setSearchValue(e.target.value)}
        icon={<Search size={16} className='text-asphalt-grey' />}
        placeholder='Search Position Name'
      />

      <div className='overflow-y-auto'>
        {_.map(filteredPositions, (hiringPosition, index) => (
          <HiringPositionRow
            key={hiringPosition.id}
            className={index !== 0 ? 'border-t border-wolf-grey' : ''}
            hiringPosition={hiringPosition}
            onEdit={() => onEdit(hiringPosition)}
          />
        ))}
      </div>
    </Modal>
  );
}

export default HiringPositionsModal;
