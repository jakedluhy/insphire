import React from 'react';
import _ from 'lodash';
import * as yup from 'yup';

import { monday } from '../../../monday';
import { useMondayData } from '../../../contexts/monday-data';
import { storage } from '../../../monday';
import { API_TOKEN_STORAGE_KEY } from '../../../monday/constants';

import { Formik, Form } from 'formik';
import Button from '../../../components/Button';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import FormikTextField from '../../../components/formik/FormikTextField';
import FormikSubmit from '../../../components/formik/FormikSubmit';

function ApiTokenInputModal({
  isOpen,
  onRequestClose,
}) {
  const { currentUser, apiToken, setApiToken } = useMondayData();
  const accountSlug = _.get(currentUser, 'account.slug');

  return (
    <Modal
      className='min-w-xl'
      isOpen={isOpen}
      onRequestClose={onRequestClose}
    >
      <Text type='title' className='block mb-md'>Set Your API Token</Text>

      <Text className='block mb-sm'>
        In order to upload resume files for candidates, you need to set your account API token. You will only need to do this once.&nbsp;
        {accountSlug ? (
          <>
            <a
              className='text-blue-500'
              href={`https://${accountSlug}.monday.com/admin/integrations/api`}
              target='_blank'
              rel='noopener noreferrer'
            >
              Click here to create/find it.
            </a>
            &nbsp;If that doesn't work follow the instructions below.
          </>
        ) : (
          'You can find it following the instructions below.'
        )}
      </Text>

      <ol className='pt-md pl-lg'>
        <li className='mb-xs'>1. Log into your monday.com account.</li>
        <li className='mb-xs'>2. Click on your avatar (picture icon) in the bottom left corner.</li>
        <li className='mb-xs'>3. Select Admin from the resulting menu (this requires you to have admin permissions).</li>
        <li className='mb-xs'>4. Go to the API section.</li>
        <li className='mb-xs'>5. Generate a “API v2 Token”</li>
        <li>6. Copy your token.</li>
      </ol>

      <Formik
        initialValues={{ apiToken: apiToken || '' }}
        validationSchema={yup.object().shape({
          apiToken: yup.string().required('is required'),
        })}
        onSubmit={(values, { setSubmitting }) => {
          storage.setItem(API_TOKEN_STORAGE_KEY, values.apiToken)
          .then(() => {
            setApiToken(values.apiToken);
            onRequestClose();
          })
          .catch(() => {
            monday.execute('notice', { message: 'Failed to set API token', type: 'error' });
            setSubmitting(false);
          });
        }}
      >
        <Form>
          <FormikTextField
            className='w-full my-lg'
            name='apiToken'
            hasLabel={false}
            placeholder='eyJhbGciOiJIUzI1NiJ9...'
          />

          <div className='flex justify-end'>
            <div className='flex'>
              <Button
                className='mr-sm'
                label='Cancel'
                variant='tertiary'
                onClick={() => onRequestClose()}
              />

              <FormikSubmit />
            </div>
          </div> 
        </Form>
      </Formik>
    </Modal>
  );
}

export default ApiTokenInputModal;
