import React from 'react';
import _ from 'lodash';

import { getDatetime } from '../../../helpers/candidates';

import Text from '../../../components/Text';
import CandidateCard from '../CandidateCard';

function CandidatesGroupedByStage({
  className,
  candidates,
  title = 'Candidates For Your Positions',
  candidateProps,
}) {
  const groupedByStageCandidates = _.groupBy(candidates, c => _.get(c, 'columns.stage.text'));

  return (
    <div className={className}>
      <div className='mb-sm'>
        <Text type='title'>{title}</Text>
      </div>

      <div>
        {_.isEmpty(candidates) ? (
          'No candidate matches'
        ) : (
          _.map(groupedByStageCandidates, (candidates, stage) => {
            const sortedCandidates = _.sortBy(candidates, getDatetime);

            return (
              <div key={stage} className='mt-lg'>
                <div className='mb-sm'>
                  <Text type='subtitle'>{stage || 'No Specified Stage'}</Text>
                </div>

                {_.map(sortedCandidates, candidate => (
                  <CandidateCard
                    key={candidate.id}
                    barColor='bg-content-river-blue'
                    candidate={candidate}
                    {...candidateProps}
                  />
                ))}
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}

export default CandidatesGroupedByStage;
