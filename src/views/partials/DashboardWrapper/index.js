import React, { useState } from 'react';
import _ from 'lodash';

import { useMondayData } from '../../../contexts/monday-data';
import { roleToText, ROLE_INTERVIEWER } from '../../../helpers/roles';

import Eye from 'react-feather/dist/icons/eye';
import Plus from 'react-feather/dist/icons/plus';
import RoleSelect from '../RoleSelect';
import ButtonMenu from '../../../components/ButtonMenu';
import Modal from '../../../components/Modal';
import Text from '../../../components/Text';
import Settings from '../../../icons/Settings';

import CreateCandidate from '../CreateCandidate';
import CreatePosition from '../CreatePosition';
import CreateEmailTemplate from '../CreateEmailTemplate';
import ApiTokenInputModal from '../ApiTokenInputModal';
import HiringPositionsModal from '../HiringPositionsModal';
import EmailTemplatesModal from '../EmailTemplatesModal';
import EditPosition from '../EditPosition';
import EditEmailTemplate from '../EditEmailTemplate';

import logo from './images/logo.svg';

function DashboardWrapper({ children, creationOptions, viewOptions }) {
  const { currentUser, data: { candidatePipelineBoardId } } = useMondayData();
  const accountSlug = _.get(currentUser, 'account.slug');

  const [isRoleModalOpen, setIsRoleModalOpen] = useState(false);
  const [isApiTokenModalOpen, setIsApiTokenModalOpen] = useState(false);
  const [isCandidateCreationOpen, setIsCandidateCreationOpen] = useState(false);
  const [isPositionCreationOpen, setIsPositionCreationOpen] = useState(false);
  const [isEmailTemplateCreationOpen, setIsEmailTemplateCreationOpen] = useState(false);
  const [isHiringPositionsModalOpen, setIsHiringPositionsModalOpen] = useState(false);
  const [isEmailTemplatesModalOpen, setIsEmailTemplatesModalOpen] = useState(false);
  const [editingHiringPosition, setEditingHiringPosition] = useState(null);
  const [editingEmailTemplate, setEditingEmailTemplate] = useState(null);

  const additionItems = _.map(creationOptions, opt => {
    switch(opt) {
      case 'candidate':
        return { label: 'Candidate', onClick: () => setIsCandidateCreationOpen(true) };
      case 'position':
        return { label: 'Position', onClick: () => setIsPositionCreationOpen(true) };
      case 'template':
        return { label: 'Email Template', onClick: () => setIsEmailTemplateCreationOpen(true) };
      default:
        return null;
    }
  });

  const viewItems = [{
    label: 'Candidates',
    onClick: () => window.open(`http://${accountSlug}.monday.com/boards/${candidatePipelineBoardId}`),
  }, {
    label: 'Hiring Positions',
    onClick: () => setIsHiringPositionsModalOpen(true),
  }, {
    label: 'Email Templates',
    onClick: () => setIsEmailTemplatesModalOpen(true),
  }];

  const compactAdditionItems = _.compact(additionItems);

  return (
    <div className='pb-lg'>
      <div className='p-lg mb-sm flex items-center justify-between'>
        <div>
          <div>
            <img src={logo} alt='InspHire' />
          </div>

          {currentUser.role && (
            <div>
              <Text className='mt-xs'>
                {roleToText(currentUser.role)}
              </Text>
            </div>
          )}
        </div>

        <div className='flex items-center'>
          {currentUser.role !== ROLE_INTERVIEWER ? (
            <ButtonMenu
              className='mr-sm'
              color='plain'
              variant='tertiary'
              items={viewItems}
            >
              <Eye className='mr-xs' size={18} />
              View
            </ButtonMenu>
          ) : null}

          {!_.isEmpty(compactAdditionItems) ? (
            <ButtonMenu
              color='plain'
              variant='tertiary'
              menuPlacement='right'
              items={compactAdditionItems}
            >
              <Plus className='mr-xs' size={18} />
              Add
            </ButtonMenu>
          ) : null}

          <ButtonMenu
            color='plain'
            variant='tertiary'
            menuPlacement='right'
            items={[
              { label: 'Set API Token', onClick: () => setIsApiTokenModalOpen(true) },
              { label: 'Change Role', onClick: () => setIsRoleModalOpen(true) },
            ]}
          >
            <Settings className='text-dark-grey' />
          </ButtonMenu>
        </div>
      </div>

      {children}

      <Modal
        className='min-w-md'
        isOpen={isRoleModalOpen}
        onRequestClose={() => setIsRoleModalOpen(false)}
      >
        <Text type='title' className='block mb-md'>Choose a New Role</Text>

        <div className='py-lg px-xl flex justify-center'>
          <RoleSelect style={{ width: 240 }} />
        </div>
      </Modal>

      <CreateCandidate
        isOpen={isCandidateCreationOpen}
        onRequestClose={() => setIsCandidateCreationOpen(false)}
        onSuccess={() => setIsCandidateCreationOpen(false)}
      />

      <CreatePosition
        isOpen={isPositionCreationOpen}
        onRequestClose={() => setIsPositionCreationOpen(false)}
        onSuccess={() => setIsPositionCreationOpen(false)}
      />

      <CreateEmailTemplate
        isOpen={isEmailTemplateCreationOpen}
        onRequestClose={() => setIsEmailTemplateCreationOpen(false)}
        onSuccess={() => setIsEmailTemplateCreationOpen(false)}
      />

      <ApiTokenInputModal
        isOpen={isApiTokenModalOpen}
        onRequestClose={() => setIsApiTokenModalOpen(false)}
      />

      <HiringPositionsModal
        isOpen={isHiringPositionsModalOpen}
        onRequestClose={() => setIsHiringPositionsModalOpen(false)}
        onEdit={hiringPosition => {
          setIsHiringPositionsModalOpen(false)
          setEditingHiringPosition(hiringPosition)
        }}
      />

      <EmailTemplatesModal
        isOpen={isEmailTemplatesModalOpen}
        onRequestClose={() => setIsEmailTemplatesModalOpen(false)}
        onEdit={emailTemplate => {
          setIsEmailTemplatesModalOpen(false)
          setEditingEmailTemplate(emailTemplate)
        }}
      />

      <EditPosition
        isOpen={!!editingHiringPosition}
        onRequestClose={() => {
          setEditingHiringPosition(null)
          setIsHiringPositionsModalOpen(true);
        }}
        hiringPosition={editingHiringPosition}
      />

      <EditEmailTemplate
        isOpen={!!editingEmailTemplate}
        onRequestClose={() => {
          setEditingEmailTemplate(null);
          setIsEmailTemplatesModalOpen(true);
        }}
        emailTemplate={editingEmailTemplate}
      />
    </div>
  );
}

export default DashboardWrapper;
