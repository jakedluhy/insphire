import React from 'react';
import _ from 'lodash';

import { getDatetime } from '../../../helpers/candidates';

import Text from '../../../components/Text';
import CandidateCard from '../CandidateCard';

function CandidatesToInterview({
  className,
  candidates,
  title = 'Your Upcoming Interviews',
}) {
  const sortedCandidates = _.sortBy(candidates, getDatetime);

  return (
    <div className={className}>
      <div className='mb-sm'>
        <Text type='title'>{title}</Text>
      </div>

      <div>
        {_.isEmpty(candidates) ? (
          'No upcoming interviews'
        ) : (
          _.map(sortedCandidates, candidate => (
            <CandidateCard
              key={candidate.id}
              barColor='bg-content-indigo'
              candidate={candidate}
              shouldShowConductInterviewButton
              shouldShowAddToCalendar
            />
          ))
        )}
      </div>
    </div>
  );
}

export default CandidatesToInterview;
