import { useState, useCallback } from 'react';

export default function useLocalStorage(key, initialValue) {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      console.log(error);
      return initialValue;
    }
  });

  const setValue = useCallback(value => {
    try {
      setStoredValue(prevStoredValue => {
        const valueToStore = value instanceof Function ? value(prevStoredValue) : value;

        window.localStorage.setItem(key, JSON.stringify(valueToStore));

        return valueToStore;
      });
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  }, [key, setStoredValue]);

  return [storedValue, setValue];
}
