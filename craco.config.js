const fastRefreshCracoPlugin = require('craco-fast-refresh');

module.exports = {
  plugins: [{ plugin: fastRefreshCracoPlugin }],
  style: {
    postcss: {
      plugins: [
        require("tailwindcss")("./tailwind.config.js"),
      ],
    },
  },
  jest: {
    configure: {
      moduleDirectories: [
        "node_modules",
        "testing"
      ]
    }
  }
};
