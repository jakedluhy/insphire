const path = require('path');
const _ = require('lodash');

const tailwindPlugin = require("tailwindcss")(path.resolve(__dirname, '..', 'tailwind.config.js'));

module.exports = {
  stories: [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-create-react-app"
  ],
  webpackFinal: async (config, { configType }) => {
    const oneOfLoadersArray = _.get(config, 'module.rules[6].oneOf');
    if (!oneOfLoadersArray) throw new Error('Unable to find oneOf loaders');

    const postcssLoaders = [];
    oneOfLoadersArray.forEach(loader => {
      const postcssLoader = _.find(loader.use, subLoader => {
        return _.includes(subLoader.loader, 'postcss-loader');
      });

      if(postcssLoader) postcssLoaders.push(postcssLoader);
    });
    if (_.isEmpty(oneOfLoadersArray)) throw new Error('Unable to find oneOfLoadersArray');
    
    postcssLoaders.forEach(postcssLoader => {
      const existingPlugins = postcssLoader.options.plugins();

      postcssLoader.options.plugins = () => [
        ...existingPlugins,
        tailwindPlugin,
      ];
    });

    return config;
  },
}
